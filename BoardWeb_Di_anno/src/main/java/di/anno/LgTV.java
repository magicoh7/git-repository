package di.anno;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("tv")
public class LgTV implements TV{

	@Autowired
//	@Qualifier("apple")
//	@Resource(name="apple")
	private Speaker speaker;
	
	public LgTV() {
		System.out.println("LgTV ��ü ����.");
	}

	@Override
	public void powerOn() {
		System.out.println("LgTV ���� �Ҵ�.");
		
	}

	@Override
	public void powerOff() {
		System.out.println("LgTV ���� ����.");
		
	}

	@Override
	public void volumnUp() {
		//System.out.println("LgTV ���� ���δ�.");
		this.speaker.volumeup();
		
	}

	@Override
	public void volumnDown() {
		//System.out.println("LgTV ���� �����.");
		this.speaker.volumeDown();
		
	}

}
