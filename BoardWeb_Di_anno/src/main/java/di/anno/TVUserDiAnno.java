package di.anno;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TVUserDiAnno {
	
	public static void main(String[] args){
		
		//1. Spring 컨테이너 구동
		AbstractApplicationContext factory = new GenericXmlApplicationContext("applicationContext.xml");
	
		//2. Spring 컨테이너로부터 필요한 객체 요청(lookup)
		System.out.println("============tv==================");
		TV tv = (TV)factory.getBean("tv");	//getBean 반환타입 Object
		tv.powerOn();
		tv.volumnUp();
		tv.volumnDown();
		tv.powerOff();

//		System.out.println("============samsungTV==================");
//		
//		TV tv2 = (TV)factory.getBean("samsungTV");	//getBean 반환타입 Object
//		tv2.powerOn();
//		tv2.volumnUp();
//		tv2.volumnDown();
//		tv2.powerOff();
		
		
		//3. Spring 컨테이너 종료
		factory.close();
	}
}
