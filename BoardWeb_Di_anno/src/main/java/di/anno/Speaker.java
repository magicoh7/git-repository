package di.anno;

public interface Speaker {

	void volumeup();

	void volumeDown();

}