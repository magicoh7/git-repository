package com.magicoh.biz.board.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.util.SqlSessionFactoryBean;

public class BoardDAO {

	private SqlSession mybatis;
	
	//get SqlSession
	public BoardDAO() {
		mybatis = SqlSessionFactoryBean.getSqlSessionInstance();
	}
	
	public void insertBoard(BoardVO vo) {
		mybatis.insert("BoardDAO.insertBoard", vo);
		mybatis.commit();
	}

	public void updateBoard(BoardVO vo) {
		mybatis.update("BoardDAO.updateBoard", vo);
		mybatis.commit();
	}// updateBoard() END

	// 글 삭제
	public void deleteBoard(BoardVO vo) {
		mybatis.delete("BoardDAO.deleteBoard", vo);
		mybatis.commit();
	}// deleteBoard() END

	// 글 상세 조회
	public BoardVO getBoard(BoardVO vo) {
		return (BoardVO) mybatis.selectOne("BoardDAO.getBoard", vo);	
	}// getBoard() END

	// 글목록 조회
	public List<BoardVO> getBoardList(BoardVO vo) {

		return mybatis.selectOne("BoardDAO.getBoardList", vo);
	}// getBoardList() END

}
