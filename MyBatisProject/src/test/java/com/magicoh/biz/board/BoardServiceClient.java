package com.magicoh.biz.board;

import java.sql.SQLException;
import java.util.List;

import com.magicoh.biz.board.impl.BoardDAO;

public class BoardServiceClient {

	public static void main(String[] args) throws SQLException{
		BoardDAO boardDAO = new BoardDAO();
		
		BoardVO vo = new BoardVO();
		vo.setTitle("myBatis");
		vo.setWriter("Hong Gil Dong");
		vo.setContent("Content");
		boardDAO.insertBoard(vo);
		
		vo.setSearchCondition("TITLE");
		vo.setSearchKeyword("");
		List<BoardVO> boardList = boardDAO.getBoardList(vo);
		for(BoardVO board : boardList){
			System.out.println("----> " + board.toString());
		}
	}

}
