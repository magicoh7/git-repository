package com.magicoh.biz.board.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magicoh.biz.board.BoardService;
import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.common.LogAdvice;

@Service("boardService")
public class BoardServiceImpl implements BoardService {

	@Autowired
	private BoardDAO boardDAO;
	
	public BoardServiceImpl() {
	}

	@Override	//포인트컷
	public void insetBoard(BoardVO vo) {
		//원래 공통관심사 코드가 와야될 위치
		//하지만 하드 코딩이 아닌 AOP를 통해서 공통로직을 런타임에 실행해줌.
		this.boardDAO.insertBoard(vo);
	}

	@Override
	public void updateBoard(BoardVO vo) {
		boardDAO.updateBoard(vo);
		
	}

	@Override
	public void deleteBoard(BoardVO vo) {
		boardDAO.deleteBoard(vo);
		
	}

	@Override
	public BoardVO getBoard(BoardVO vo) {
		return boardDAO.getBoard(vo);
	}

	@Override
	public List<BoardVO> getBoardList(BoardVO vo) {
		
		return boardDAO.getBoardList(vo);
	}

	
}
