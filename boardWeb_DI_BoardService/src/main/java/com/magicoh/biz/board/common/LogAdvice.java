package com.magicoh.biz.board.common;

import org.aspectj.lang.JoinPoint;

public class LogAdvice {

	public LogAdvice() {
	}

	//공통관심 메소드(Advice)
	public void printLog(){
		System.out.println("공통로그 - 비즈니스 로직 수행 ");
	}
}
