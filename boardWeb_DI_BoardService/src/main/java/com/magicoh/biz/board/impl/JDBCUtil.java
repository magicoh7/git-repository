package com.magicoh.biz.board.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import com.mysql.jdbc.PreparedStatement;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

public class JDBCUtil {

	public JDBCUtil() {
	}
	
	public static Connection getConnection(){
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			return DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "board_spring", "1234");
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static void close(PreparedStatement stmt, Connection conn){
		if(stmt != null){
			try{
				if(!stmt.isClosed()) stmt.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				stmt = null;
			}
		}
		
		if(conn != null){
			try{
				if(!conn.isClosed()) conn.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				conn = null;
			}
		}
	}
	
	public static void close(ResultSet rs, PreparedStatement stmt, Connection conn){
		if(rs != null){
			try{
				if(!rs.isClosed()) rs.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				rs = null;
			}
		}
		
		if(stmt != null){
			try{
				if(!stmt.isClosed()) stmt.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				stmt = null;
			}
		}
		
		if(conn != null){
			try{
				if(!conn.isClosed()) conn.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				conn = null;
			}
		}
	}
}
