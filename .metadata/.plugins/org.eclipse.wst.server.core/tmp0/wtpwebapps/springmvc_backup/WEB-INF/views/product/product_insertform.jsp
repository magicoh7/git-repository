<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/views/shared/header.jsp" %>

	<div class="container">
		<div class="row">
			<form method="post" action="product_insert">
				<table class="table table-bordered table-hover table-striped" style="text-align: center; border: 1px solid #dddddd">
					<thead>
						<tr>
							<th colspan="3" style="background-color: #eeeeee; text-align: center;">상품 정보 등록</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width: 20%">상품명</td>
							<td colspan="2"><input type="text" class="form-control" name="product_name" maxlength="50"></td>
						</tr>
						<tr>
							<td style="width: 20%">브랜드</td>
							<td colspan="2"><input type="text" class="form-control" name="brand" maxlength="50" ></td>
						</tr>
						<tr>
							<td>상품 설명</td>
							<td colspan="2"><input type="text" class="form-control"  name="description" maxlength="100" ></td>
						</tr>
						<tr>
							<td>상품 단가</td>
							<td colspan="2"><input type="text" class="form-control"  name="unit_price" maxlength="100" ></td>
						</tr>
						<tr>
							<td>카테고리ID</td>
							<td colspan="2">
								<select name="category_id" style="width:1000px;height:30px;" >
									<c:forEach var="category" items="${categoryList}">
										<option value="${category.category_id }">${category.category_name }</option>
									</c:forEach>	
								</select>
							</td>
						</tr>
						<tr>
							<td>사용유무</td>
							<td colspan="2"><input type="text" class="form-control"  name="is_active" maxlength="200" value='1'></td>
						</tr>
					</tbody>				
				</table>
				<div>
					<input type="submit" class="btn btn-primary pull-rigth" value="저장">
					<a href="category_list" class="btn btn-info">목록</a>
				</div>
			</form>
		</div>
	</div>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	
</body>
</html>