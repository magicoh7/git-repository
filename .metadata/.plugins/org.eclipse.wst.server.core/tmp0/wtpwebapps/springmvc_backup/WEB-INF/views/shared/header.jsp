<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width" initial-scale="1">
	
	<!--  Bootstrap CSS--> 
	<link href="css/bootstrap.css" rel="stylesheet"  />
	<!--  jQuery UI CSS--> 
	<link href="http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
	<!--  jQuery awesome font CSS-->
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
	<!--  Datatable UI CSS--> 
	<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/custom.css" />
	<title>Simple Invoice</title>

</head>
<body>
	<%
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
		}
		int pageNumber = 1;	//기본 페이지
		if(request.getParameter("pageNumber") != null){
			pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
		}
	%>

	<nav class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Simple Invoice</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="home">메인</a></li>
				<li><a href="bbsList">고객센터</a></li>
				<li><a href="invoice_list">인보이스 조회</a></li>
				<li><a href="invoice_insertform">인보이스 입력</a></li>
				<li><a href="category_insertform">카테고리 등록</a></li>
				<li><a href="category_list">카테고리 조회</a></li>
				<li><a href="product_insertform">상품 등록</a></li>
				<li><a href="product_list">상품 조회</a></li>
			</ul>
			<%
				if(userID == null) {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="login">회원가입</a></li>
				<li><a href="login">로그인</a></li>
			</ul>
			
			<%
				} else {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="logout">로그아웃</a></li>
				<li><a href="changeInfo">개인정보 변경</a></li>
			</ul>
			
			<%
				}
			%>
		</div>
	</nav>
	