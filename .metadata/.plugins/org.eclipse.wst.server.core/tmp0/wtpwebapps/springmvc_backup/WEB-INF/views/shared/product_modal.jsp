<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<!--  Bootstrap CSS--> 
	<!--  <link href="css/bootstrap.css" rel="stylesheet"  /> -->
	<!--  jQuery UI CSS--> 
	<!--  <link href="http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" /> -->
	<!--  jQuery awesome font CSS-->
	<!--  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" /> -->
	<!--  Datatable UI CSS--> 
	<!--  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" /> -->
	<!--  <link rel="stylesheet" href="css/custom.css" /> -->
	
	<div class="container" style="width:400px;">
	    <nav class="navbar navbar-default">
	        <form action="product_modal" method="get">
	                <div class="form-group">
		                <a href="#" class="navbar-brand">Search</a>
	                    <input type="text" id="product_name" name="product_name" id="product_name" class="form-group" ${product_name} />
	                    <button type="button" id="btnSearch" class="btn btn-success">search</button>
	                </div>
	         </form>       
	    </nav>
		<div class="row">
			<div>
				<table class="table table-bordered table-hover table-striped" id="datatable" style="width:50%">
					<thead>
						<tr>
							<th style="background-color: #eeeeee">product_id</th>
							<th style="background-color: #eeeeee">product_name</th>
							<th style="background-color: #eeeeee">brand</th>
							<th style="background-color: #eeeeee">unit_price</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="product" items="${productList}">
						<tr>
							<td align=left>${product.product_id }</td>
						<!--  	<td align=left><a href="#" onclick="clickProduct(this)">${product.product_name}</a></td> -->
							
							<td align=left><a href="#">${product.product_name}</a></td>
							<td align=left>${product.brand }</td>
							<td align=left>${product.unit_price }</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<input type="hidden" id="parentsLowNo" value="${parentsLowNo}" />
	
	<!--  <script type="text/javascript" src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script> -->
	<!--  <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js"></script> -->
		
	<script>
		$(document).ready(function(){
			$("#datatable").DataTable({
				"paging": true,
				"bFilter":false,
				columnDefs: [
                    { "targets": 0, 'width': '100px'},
                    { "targets": 1, 'width': '100px'},
                    { "targets": 2, 'width': '100px'},
                    { "targets": 3, 'width': '100px'},
               ],
				"columns": [
                    { "data": "product_id"},
                    { "data": "product_name"},
                    { "data": "brand"},
                    { "data": "unit_price"},
                ]
            });
			
			
			$('#datatable a').on("click", function(event){
				//event.preventDefault();
 				var row = $(this).closest("tr");
 				var productId = row.find("td").eq(0).html();    //product Id in modal dialog
 				var productName = $(this).text();				//row.find("td").eq(1).html(); <-- 주석처럼 하면 <a href="~~">값까지 갖고옴"
 		        var productPrice = row.find("td").eq(3).html(); //product Price in modal dialog
 		        var parentsLowNo = $('#parentsLowNo').val();    //Parents row number hidden field
 		        
 		       //Check Dubplicate purduct Id
 		        var isDup = Fn_IsDuplicate_Items(productId.trim());
 		        if (isDup) {
 		            if (confirm('Duplicate items exist. Would you like to proceed?')) {
 		                table.cell(parentsLowNo, 1).nodes().to$().find('input').val(productName);   
 		                table.cell(parentsLowNo, 2).nodes().to$().find('input').val(parseInt(productId));
 		                table.cell(parentsLowNo, 3).nodes().to$().find('input').val(productPrice);
 		                $('#myModal').modal('hide'); //$('#myModal').hide(); <- error
 		            }
 		            else {
 		                return false;
 		            }
 		        }
 		        
				table.cell(parentsLowNo, 1).nodes().to$().find('input').val(productName);
 		        table.cell(parentsLowNo, 2).nodes().to$().find('input').val(parseInt(productId));
 		        table.cell(parentsLowNo, 3).nodes().to$().find('input').val(productPrice);
 		        $('#myModal').modal('hide');
 		 
			});	//end of onClick()
		});	//end of ready()
		
		//Check duplicate productId from parents table
		function Fn_IsDuplicate_Items(productId) {
	        var dup = false;
	        $('#invoiceDetails tbody tr').each(function (index, ele) {
	            var pid = table.cell(this, 2).nodes().to$().find('input').val();       //productId
	            if (productId.trim() == pid.trim()) dup = true;
	        });
	        return dup;
	    }
	</script>	
</body>
</html>