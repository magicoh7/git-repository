package com.magicoh.biz.board;


import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.magicoh.biz.user.UserVo;

public class UserServiceClient {

	public static void main(String[] args) {
		// 1. Spring Container를 구동
		AbstractApplicationContext container = new GenericXmlApplicationContext("applicationContext.xml");

		// 2. Spring Container로부터 UserServiceImpl 객체를 요청(lookup)한다.
		//UserService userService = (UserService) container.getBean("userService");

		// 3. 회원 로크 체크를 위한 값 설정
		UserVo vo = new UserVo();
		vo.setId("test");
		vo.setPassword("test1234");
		
		// 4. 회원 로크 체크 케스트
//		UserVo user=userService.getUser(vo);
//		if(user != null){
//			System.out.println(user.getName() + "님 환영합니다.");
//		} else {
//			System.out.println("로그인 실패");
//		}

		// 5. Spring Container 종료
		container.close();

	}

}
