package com.magicoh.view.user;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.magicoh.biz.user.UserVo;
import com.magicoh.biz.user.impl.UserDao;


@Controller
public class LoginController  {
	
	@RequestMapping(value="/login.do", method=RequestMethod.GET)
	public String loginView(UserVo vo, Model model) {	//로그인 정보 입력 화면
		System.out.println("로그인 화면으로 이동하기 전 아이디/패스워드 만듦");
		//프로그램 테스트할 때마다 아이디/패스워드 입력하는 걸 피하기 위해서 로그인 아이디/패스워드 미리 세팅해둠.
		Map<String, String> user = new HashMap<String, String>();
		user.put("id", "magic");
		user.put("password", "1234");
		model.addAttribute("user", user);
		return "login.jsp";		//해당 로직 처리후 보여줄 정보가 없는 경우는 그냥 login.jsp(폼)를 보여주도록
	}

	@RequestMapping(value="/login.do", method=RequestMethod.POST)
	public String login(UserVo vo, UserDao userDAO, HttpSession session) {	//로그인 아이디/비번 인증 처리
		System.out.println("로그인 인증 처리");
		UserVo user = userDAO.getUser(vo); 
		if(user != null){	
			session.setAttribute("userName", user.getName());
			return "getBoardList.do";	//해당 로직 처리후 뭔가 보여줄 정보가 있는 경우
		}else{
			return "login.jsp";		//해당 로직 처리후 보여줄 정보가 없는 경우는 그냥 login.jsp(폼)를 보여주도록
		}
	}

	@RequestMapping(value="/logout.do")
	public String logout(HttpSession session) {
		System.out.println("로그아웃 처리");
		session.invalidate();
		return "login.jsp";
	}
}
