package com.magicoh.view.board;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashAttributeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.magicoh.biz.board.BoardService;
import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.impl.BoardDAO;

@Controller
@SessionAttributes("board")
public class BoardController {

	@Autowired
	private BoardService boardService;
	
	/**
	 * 조회 화면의 검색 조건을 세팅해주는 메소드로 화면에서 만들지 않고
	 * DB에서 불러서 세팅해줄 때 유용함. 이 메소드의 실행이 끝나면
	 * 아래에 있는 getBoardList()가 실행된다.
	 * **/
	@ModelAttribute("conditionMap")
	public Map<String, String> searchConditionMap(){
		Map<String, String> conditionMap = new HashMap<String, String>();
		conditionMap.put("제목", "TITLE");
		conditionMap.put("내용", "CONTENT");
		//보낸 내용(conditionMap)이 Model 객체에 실린다. 그 다음에  getBoardList()메소드 실행하러 간다.
		return conditionMap;	
	}

	@RequestMapping("/getBoardList.do")
	public String getBoardList(BoardVO vo, Model model){
		if(vo.getSearchCondition() == null){
			vo.setSearchCondition("TITLE");
		}
		if(vo.getSearchKeyword() ==  null){
			vo.setSearchKeyword("");
		}
		List<BoardVO> boardList = boardService.getBoardList(vo);
		model.addAttribute("boardList", boardList);	//boardList가 2차로  Model 객체에 실려서 JSP로 전달된다.
		
		return "getBoardList.jsp" ;
	}
	
	/**
	 * 앞의 뷰 화면에서 BoardVO 객체에 바인딩될 값 말고 추가적으로 조회 조건들(검색 타입, 검색어)을
	 * @RequestParam (value="searchCondition", defaultValue="TITLE", required=false) String condition
	 * @RequestParam(value="searchKeyworld", defaultValue="", required=false) String keyword
	 * 식으로 받고 있다.
	 * **/
//	@RequestMapping("/getBoardList.do")
//	public String getBoardList(
//			@RequestParam(value="searchCondition", defaultValue="TITLE", required=false) String condition,
//			@RequestParam(value="searchKeyworld", defaultValue="", required=false) String keyword,
//			BoardVO vo, 
//			BoardDAO boardDAO, Model model){
//		List<BoardVO> boardList = boardDAO.getBoardList(vo);
//		model.addAttribute("boardList", boardList);
//		return "getBoardList.jsp" ;
//	}
	
	@RequestMapping("/getBoard.do")
	public String getBoard(BoardVO vo, Model model){
		BoardVO board = boardService.getBoard(vo);
		model.addAttribute("board", board);
		return "getBoard.jsp";
	}
	
	@RequestMapping("/insertBoard.do")
	public String insertBoard(BoardVO vo) {
		boardService.insetBoard(vo);
		return "getBoardList.do";
	}
	
	//updateForm
	@RequestMapping(value="/updateBoard.do", method=RequestMethod.GET)
	public String updateBoardForm(BoardVO vo, Model model){
		BoardVO board = boardService.getBoard(vo);
		model.addAttribute("board", board);
		return "updateBoardForm.jsp";
	}
	
	//updateProcess
	@RequestMapping(value="/updateBoard.do", method=RequestMethod.POST)
	public String updateBoard(BoardVO vo){
		boardService.updateBoard(vo);
		return "getBoardList.do";
	}
	
	@RequestMapping("/deleteBoard.do")
	public String deleteBoard(BoardVO vo, BoardDAO boardDAO){
		boardDAO.deleteBoard(vo);
		return "getBoardList.do";

	}
}
