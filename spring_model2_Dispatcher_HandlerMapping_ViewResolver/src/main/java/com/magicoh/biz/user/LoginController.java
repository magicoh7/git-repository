package com.magicoh.biz.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.biz.board.UserVo;
import com.magicoh.biz.board.impl.UserDAO;
import com.magicoh.biz.controller.Controller;

public class LoginController implements Controller {

	public LoginController() {
	}

	@Override
	public String handlerRequest(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("로그인 처리");
		
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		
		UserVo vo = new UserVo();
		vo.setId(id);
		vo.setPassword(password);
		
		UserDAO userDAO = new UserDAO();
		UserVo user = userDAO.getUser(vo);
		
		//화면명 반환
		if(user != null){	
			return "getBoardList.do";	//해당 로직 처리후 뭔가 보여줄 정보가 있는 경우
		}else{
			return "login";		//해당 로직 처리후 보여줄 정보가 없는 경우는 그냥 login.jsp(폼)를 보여주도록
		}
	}

}
