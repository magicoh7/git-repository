package com.magicoh.biz.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Controller {

	//DispatcherServlet의 doGet() doPost() 메소드가 아래와 같은 형태의 매개변수를 받기 때문에 동일하게 설정함.
	String handlerRequest(HttpServletRequest request, HttpServletResponse response);
}
