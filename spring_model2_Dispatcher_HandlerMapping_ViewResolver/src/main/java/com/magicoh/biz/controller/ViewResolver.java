package com.magicoh.biz.controller;

public class ViewResolver {
	public String prefix;
	public String suffix;
	
	public void setPrefix(String prefix){
		this.prefix = prefix;
	}
	
	public void setSuffix(String suffix){
		this.suffix = suffix;
	}
	
	//DispatcherServlet에서 호출
	//전달된 prefix suffix를 혼합해서 정확한 경로 반환.
	public String getView(String viewName){
		System.out.println("ViewResolver : " + prefix + viewName + suffix);
		return prefix + viewName + suffix;
	}
}
