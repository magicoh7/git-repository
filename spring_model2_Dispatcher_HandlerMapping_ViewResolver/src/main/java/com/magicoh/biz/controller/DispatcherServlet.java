package com.magicoh.biz.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.UserVo;
import com.magicoh.biz.board.impl.BoardDAO;
import com.magicoh.biz.board.impl.UserDAO;

public class DispatcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private HandlerMapping handlerMapping ;
	private ViewResolver viewResolver;
       
    public DispatcherServlet() {}
    
    public void init() throws ServletException{
    	handlerMapping = new HandlerMapping();
    	viewResolver = new ViewResolver();
    	viewResolver.setPrefix("./");
    	viewResolver.setSuffix(".jsp");
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		process(request, response);
	}
	private void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		//1. 클라이언트의 요청 경로 추출
		String uri = request.getRequestURI();	
		String path = uri.substring(uri.lastIndexOf("/"));
		System.out.println("클라이언트의 요청 경로 : " + path);
		
		//2. 핸들러 맵핑에 찾은 경로를 주고 적합한 컨트롤러를 찾아온다.
		Controller ctrl = handlerMapping.getController(path);
		
		//3. 찾아진 컨트롤러가 클라이언트가 보내온 파라미터를 추출해서 작업을 할 수 있도록 request를 인자로 보낸다.
		String viewName = ctrl.handlerRequest(request, response);
		
		String view = null;
		if(!viewName.contains(".do")){	//Controller 처리후 보여줄 내용이 없는 경우  "login" + ".jsp"(그냥 폼 출력)
			view = viewResolver.getView(viewName);
		}else{	//Controller 처리후 뭔가 보여줄 내용이 있는 경우  "login" + ".do" 해서 다음 루틴 수행 
			view = viewName;
		}
		System.out.println("DispatcherServlet view : " + view );
		
		//5. 얻은 경로 정보를 클라이언트에 보내서 다시 요청하도록 지시
		//   1) 클라이언트는 "login.jsp"폼이 돌아오면 그냥 그 페이지를 요청해서 보게되고
		//   2) boardList.do 경로가 돌아오면 그 경로를 다시 서버에 보내서 목록을 보게된다. 
		response.sendRedirect(view);
		
	}
}
