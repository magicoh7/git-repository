package com.magicoh.springmvc.util;

import java.util.List;

import com.magicoh.springmvc.dto.InvoiceDetail;

/**
 * ���댁� �ㅻ�寃��댄�� �⑥��
 * @author stoneis.pe.kr
 * @since 2013.07.10
 */
public class PageNavigator {
	
	/**
	 * ���댁� �ㅻ�寃��댄�곕�� 留��ㅼ�댁＜�� �⑥��
	 * @param totalCount	- 珥���
	 * @param listCount		- �몄��� 紐⑸� 寃���臾� ��
	 * @param pagePerBlock	- �몄��� 釉�濡� �� [ 1 2 3 4 5 ] 5媛�
	 * @param pageNum		- ���댁� 踰���
	 * @param startPage		- ���� 寃���臾�
	 * @param endPage		- �� 寃���臾�
	 * @return
	 */
	
	 private int totalCount;
	 private int listCount = 10;
	 private int pagePerBlock = 5;
	 private int pageNum;
	 private int startPage;
	 private int endPage;

	 //private List<SearchCondition> searchCondition;
	 
	public int getTotalCount() {
		return totalCount;
	}


	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}


	public int getListCount() {
		return listCount;
	}


	public void setListCount(int listCount) {
		this.listCount = listCount;
	}


	public int getPagePerBlock() {
		return pagePerBlock;
	}


	public void setPagePerBlock(int pagePerBlock) {
		this.pagePerBlock = pagePerBlock;
	}


	public int getPageNum() {
		return pageNum;
	}


	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}


	public int getStartPage() {
		return startPage;
	}


	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}


	public int getEndPage() {
		return endPage;
	}


	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}


	public String getPageNavigator(int totalCount, int listCount, int pagePerBlock, int pageNum, String searchText) {
		StringBuffer sb = new StringBuffer();	
		
		if(totalCount > 0) {	//珥� ��肄��� ��媛� �����쇰�� ���댁�� ���댁�
			//珥� ���댁���
			int totalNumOfPage = (totalCount % listCount == 0) ? totalCount / listCount :	totalCount / listCount + 1;
			
			//���댁� 踰��� 洹몃９
			int totalNumOfBlock = (totalNumOfPage % pagePerBlock == 0) ? totalNumOfPage / pagePerBlock : totalNumOfPage / pagePerBlock + 1;
			
			//��泥��� ���댁�媛� 紐� 踰�吏� ���댁� 洹몃９�� ����吏� ����(������ 酉곕� 媛��� �������� �� 湲곗�듯��怨� ���댁��.)
			int currentBlock = (pageNum % pagePerBlock == 0) ? 
					pageNum / pagePerBlock :
					pageNum / pagePerBlock + 1;
			
			//�������댁��� �����댁�(�� ���댁� 釉��� �댁����)
			int startPage = (currentBlock - 1) * pagePerBlock + 1;
			int endPage = startPage + pagePerBlock - 1;
			
			if(endPage > totalNumOfPage)
				endPage = totalNumOfPage;
			
			boolean isNext = false;
			boolean isPrev = false;
			
			//���� ���댁�媛� ���� 釉��� 蹂대�� ���� ��留� >> 蹂댁�ъ�(珥� ���댁� 釉����� 3�몃�� �닿� ���� ���� ���댁� 釉����� 3�대㈃ >> ��������)
			if(currentBlock < totalNumOfBlock)
				isNext = true;
			
			if(currentBlock > 1)
				isPrev = true;
			if(totalNumOfBlock == 1){
				isNext = false;
				isPrev = false;
			}
			
			if(pageNum > 1){	// 荑쇰━�ㅽ�몃����� & = &amp; 
				sb.append("<a href=\"").append("productList.do?pageNum=1&amp;searchText="+searchText);
				sb.append("\" title=\"<<\"><<</a>&nbsp;");
			}
			if (isPrev) {	//<< 媛� ���쇰㈃
				int goPrevPage = startPage - pagePerBlock;			
				sb.append("&nbsp;&nbsp;<a href=\"").append("productList.do?pageNum="+goPrevPage+"&amp;searchText="+searchText);
				sb.append("\" title=\"<\"><</a>");
			} else {
				
			}
			for (int i = startPage; i <= endPage; i++) {
				if (i == pageNum) {
					sb.append("<a href=\"#\"><strong>").append(i).append("</strong></a>&nbsp;&nbsp;");
				} else {
					sb.append("<a href=\"").append("productList.do?pageNum="+i+"&amp;searchText="+searchText);
					sb.append("\" title=\""+i+"\">").append(i).append("</a>&nbsp;&nbsp;");
				}
			}
			if (isNext) {
				int goNextPage = startPage + pagePerBlock;
	
				sb.append("<a href=\"").append("productList.do?pageNum="+goNextPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">\">></a>");
			} else {
				
			}
			if(totalNumOfPage > pageNum){
				sb.append("&nbsp;&nbsp;<a href=\"").append("productList.do?pageNum="+totalNumOfPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">>\">>></a>");
			}
			
		}
		
		return sb.toString();
	}
}
