package com.magicoh.springmvc.dto;

public class User_role {
	private int user_role_id = 0;
	private String user_id = "";
	private String role_id = "";
	
	public User_role() {
	}

	public User_role(int user_role_id, String user_id, String role_id) {
		this.user_role_id = user_role_id;
		this.user_id = user_id;
		this.role_id = role_id;
	}

	public int getUser_role_id() {
		return user_role_id;
	}

	public void setUser_role_id(int user_role_id) {
		this.user_role_id = user_role_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}
	
	
}
