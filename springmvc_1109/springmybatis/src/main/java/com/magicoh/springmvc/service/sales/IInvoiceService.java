package com.magicoh.springmvc.service.sales;


import java.util.List;

import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.InvoiceCommonResult;

public interface IInvoiceService
{
	public List<InvoiceCommonResult> getInvoiceList();
	public List<InvoiceCommonResult> getInvoiceDetail(int invoice_id);
	public InvoiceCommonResult getInvoiceHeader(int invoice_no);
	public int getMaxInvoiceId();
	public void insertInvoiceDetail(InvoiceDetail invoiceDetail);
	public void insertInvoiceHeader(InvoiceHeader invoiceHeader);
//	public void updateProduct(Product product);
//	public void deleteProduct(int product_id);
}
