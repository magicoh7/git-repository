
	<div class="container" style="width:500px;">
		<div class="content-container">
        </div>
		<div class="row">
			<div>
				<table class="table table-bordered table-hover table-striped" id="datatable" style="width:50%">
					<thead>
						<tr>
							<th style="background-color: #eeeeee">product_id</th>
							<th style="background-color: #eeeeee">product_name</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="product" items="${productList}">
						<tr>
							<td align=left>${product.product_id }</td>
							<td align=left><a href="product_view?product_id=${product.product_id}">${product.product_name}</a></td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js"></script>
		
	<script>
		$(document).ready(function(){
			$("#datatable").DataTable({
				"paging": true,
				"columns": [
                    { "data": "product_id", "autoWidth": true },
                    { "data": "product_name", "autoWidth": true }
                ]
            });
		})
	</script>	
