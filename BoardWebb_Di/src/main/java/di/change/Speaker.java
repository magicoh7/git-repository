package di.change;

public interface Speaker {

	void volumeup();

	void volumeDown();

}