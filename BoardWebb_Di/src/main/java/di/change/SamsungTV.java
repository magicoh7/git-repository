package di.change;

public class SamsungTV implements TV {

	private Speaker speaker;
	private int price;
	
	public SamsungTV(){
		System.out.println("�Ｚ TV(1) ��ü����");
	}

	public SamsungTV(Speaker speaker){
		System.out.println("�ＺTV(2) ��ü ����");
		this.speaker = speaker;
	}

	public SamsungTV(Speaker speaker, int price){
		System.out.println("�ＺTV(3) ��ü ����");
		this.speaker = speaker;
		this.price = price;
	}
	
	@Override
	public void powerOn() {
		System.out.println("�ＺTV ���� �Ҵ�. ���� : " + this.price + ")");
		
	}

	@Override
	public void powerOff() {
		System.out.println("�ＺTV ��������.");
		
	}

	@Override
	public void volumnUp() {
		speaker = new SonySpeaker();
		speaker.volumeup();
	}

	@Override
	public void volumnDown() {
		speaker = new SonySpeaker();
		speaker.volumeDown();
	}
	
}
