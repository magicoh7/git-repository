package di.setter;

public class SonySpeaker implements Speaker {

	public SonySpeaker(){
		System.out.println("sony speaker 按眉 积己");
	}
	
	/* (non-Javadoc)
	 * @see di.change.Speaker#volumeup()
	 */
	@Override
	public void volumeup(){
		System.out.println("sonyspeaker 家府 棵覆");		
	}
	
	/* (non-Javadoc)
	 * @see di.change.Speaker#volumeDown()
	 */
	@Override
	public void volumeDown(){
		System.out.println("sony speaker 家府 郴覆");
	}
}
