package di.setter;

public interface Speaker {

	void volumeup();

	void volumeDown();

}