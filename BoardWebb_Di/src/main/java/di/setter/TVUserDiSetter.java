package di.setter;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TVUserDiSetter {
	
	public static void main(String[] args){
		
		//1. Spring 컨테이너 구동
		AbstractApplicationContext factory = new GenericXmlApplicationContext("applicationContext.xml");
		//2. Spring 컨테이너로부터 필요한 객체 요청(lookup)
		TV tv=(TV)factory.getBean("tv");	//getBean 반환타입 Object
		tv.powerOn();
		tv.volumnUp();
		tv.volumnDown();
		tv.powerOff();
		
		//3. Spring 컨테이너 종료
		factory.close();
	}
}
