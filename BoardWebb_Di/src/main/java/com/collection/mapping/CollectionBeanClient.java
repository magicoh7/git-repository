package com.collection.mapping;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class CollectionBeanClient {
	
	public static void main(String[] args){
		
		//1. Spring 컨테이너 구동(컨테이너 설정 정보는 applicationContext.xml 에 있다.)
		AbstractApplicationContext factory = new GenericXmlApplicationContext("applicationContext.xml");
		
		//2. Spring Container 에게  필요한 객체 요청한다.( 혹시 collectionBean 이라는 객체가 있나요?)
		CollectionBean bean = (CollectionBean) factory.getBean("collectionBean");
		
		Properties addressList = bean.getAddressList();
		
		Enumeration enu = addressList.propertyNames();
		
		while(enu.hasMoreElements()){
			
			String ele = (String)enu.nextElement();
			System.out.println(ele + " : " + addressList.getProperty(ele));
		}
		
		//3. Spring 컨테이너 종료
		factory.close();
	}
}
