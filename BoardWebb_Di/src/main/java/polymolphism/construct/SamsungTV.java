package polymolphism.construct;

public class SamsungTV implements TV {

	private SonySpeaker speaker;
	
	public SamsungTV(){
		System.out.println("�Ｚ TV ��ü����");
	}

	@Override
	public void powerOn() {
		System.out.println("�ＺTV ���� �Ҵ�");
		
	}

	@Override
	public void powerOff() {
		System.out.println("�ＺTV ��������.");
		
	}

	@Override
	public void volumnUp() {
		speaker = new SonySpeaker();
		speaker.volumeup();
	}

	@Override
	public void volumnDown() {
		speaker = new SonySpeaker();
		speaker.volumeDown();
	}
	
}
