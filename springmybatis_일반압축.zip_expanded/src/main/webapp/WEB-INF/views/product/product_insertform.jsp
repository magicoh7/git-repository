<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/views/shared/header.jsp" %>

	<div class="container">
		<div class="row">
			<form method="post" action="product_insert">
				<table class="table table-bordered table-hover table-striped" style="text-align: center; border: 1px solid #dddddd">
					<thead>
						<tr>
							<th colspan="3" style="background-color: #eeeeee; text-align: center;">상품 정보 등록</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width: 20%">상품명</td>
							<td colspan="2"><input type="text" class="form-control" name="product_name" maxlength="50"></td>
						</tr>
						<tr>
							<td style="width: 20%">브랜드</td>
							<td colspan="2"><input type="text" class="form-control" name="brand" maxlength="50" ></td>
						</tr>
						<tr>
							<td>상품 설명</td>
							<td colspan="2"><input type="text" class="form-control"  name="description" maxlength="100" ></td>
						</tr>
						<tr>
							<td>상품 단가</td>
							<td colspan="2"><input type="text" class="form-control"  name="unit_price" maxlength="100" ></td>
						</tr>
						<tr>
							<td>카테고리ID</td>
							<td colspan="2">
								<select name="category_id" style="width:1000px;height:30px;" >
									<c:forEach var="category" items="${categoryList}">
										<option value="${category.category_id }">${category.category_name }</option>
									</c:forEach>	
								</select>
							</td>
						</tr>
						<tr>
							<td>사용유무</td>
							<td colspan="2"><input type="text" class="form-control"  name="is_active" maxlength="200" value='1'></td>
						</tr>
						<tr>
							<td>입고일</td>
							<td colspan="2"><input type="text" class="form-control date-picker" id="receipt_date" name="receipt_date" maxlength="10" value="${receipt_date }"></td>
						</tr>
					</tbody>				
				</table>
				<div>
					<input type="submit" class="btn btn-primary pull-rigth" value="저장">
					<a href="category_list" class="btn btn-info">목록</a>
				</div>
			</form>
		</div>
	</div>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
	<script src="<c:url value="/resources/js/jquery.serialize-object.js" />"></script>

	<script>
		$(document).ready(function(){
			//calendar
			$('.date-picker').datepicker({ 
				dateFormat: 'yy-mm-dd', 
				changeMonth: 'true', 
				changeYear: 'true',
		        dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],
		        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], 
		        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		        monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				showButtonPanel: true, 
		        currentText: '오늘 날짜', 
		        closeText: '닫기',
			});
			
			//search 
            $('#btnSearch').on('click', function () {
            	//alert('btnSearch click event');
            	event.preventDefault(); 	
            	//var client_name = $('.client_name').val();
                Fn_Search_Product();
            });
		});
		
	</script>
	
</body>
</html>