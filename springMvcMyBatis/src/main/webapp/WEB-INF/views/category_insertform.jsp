<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width" initial-scale="1">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/custom.css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
	<title>Simple Invoice</title>
</head>
<body>
	<%
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
		}
	%>
	<nav class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Simple Invoice</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="home">메인</a></li>
				<li><a href="bbsList">고객센터</a></li>
				<li><a href="invoice_list">인보이스 조회</a></li>
				<li><a href="invoice_insertform">인보이스 입력</a></li>
				<li class="active"><a href="category_insertform">카테고리 등록</a></li>
				<li><a href="category_list">카테고리 조회</a></li>
				<li><a href="product_insertform">상품 등록</a></li>
				<li><a href="product_list">상품 조회</a></li>
			</ul>
			<%
				if(userID == null) {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="login">회원가입</a></li>
				<li><a href="login">로그인</a></li>
			</ul>
			
			<%
				} else {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="logout">로그아웃</a></li>
				<li><a href="changeInfo">개인정보 변경</a></li>
			</ul>
			
			<%
				}
			%>
		</div>
	</nav>
	
	<div class="container">
		<div class="row">
			<form method="post" action="category_insert">
				<table class="table table-bordered table-hover table-striped" style="text-align: center; border: 1px solid #dddddd">
					<thead>
						<tr>
							<th colspan="3" style="background-color: #eeeeee; text-align: center;">카테고리 등록</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width: 20%">카테고리명</td>
							<td colspan="2"><input type="text" class="form-control" name="category_name" maxlength="50" ></td>
						</tr>
						<tr>
							<td>카테고리 설명</td>
							<td colspan="2"><input type="text" class="form-control"  name="description" maxlength="100" ></td>
						</tr>
					</tbody>
				</table>
				<div>
					<input type="submit" class="btn btn-primary pull-rigth" value="저장">
					<a href="category_list" class="btn btn-info">목록</a>
				</div>
			</form>
		</div>
	</div>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	
</body>
</html>