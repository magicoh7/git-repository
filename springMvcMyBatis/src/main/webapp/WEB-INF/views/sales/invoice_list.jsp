<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/views/shared/header.jsp" %>
	
	<div class="container">
		<div class="content-container">
            <section>
            </section>
        </div>
		<div class="row">
			<div>
				<p>
				    <button type="button" class="btn btn-success" onclick="location.href='invoice_insertform'"><i class="fa fa-plus"></i> New Invoice</button>
				</p>
			</div>
		
			<div>
				<table class="table table-bordered table-hover table-striped" id="datatable" style="width:100%">
					<thead>
						<tr>
							<th style="background-color: #eeeeee">invoice_no</th>
							<th style="background-color: #eeeeee">client_id</th>
							<th style="background-color: #eeeeee">total_amt</th>
							<th style="background-color: #eeeeee">shipping_address</th>
							<th style="background-color: #eeeeee">created_date</th>
							<!-- <th style="background-color: #eeeeee">another</th> -->
						</tr>
					</thead>
					<tbody>
						<c:forEach var="invoice" items="${invoiceList}">
						<tr>
							<td align=left><a href="invoice_view?invoice_no=${invoice.invoice_no}">${invoice.invoice_no}</a></td>
							<td align=left>${invoice.client_id }</td>
							<td align=left>${invoice.total_amt }</td>
							<td align=left>${invoice.shipping_address }</td>
							<td align=left>${invoice.created_date }</td>
							<!-- td align=left>${invoice.client.client_name }</td>  -->
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
	</div>
	
	<script type="text/javascript" src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js"></script>
		
	<script>
		$(document).ready(function(){
			$("#datatable").DataTable();
		})
	</script>	
		
		
</body>
</html>