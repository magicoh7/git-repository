<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.FileInputStream"%>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.ArrayList" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css"/>
	<link rel="stylesheet" href="css/custom.css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
	
	<title>category list</title>
	
	<style type="text/css">
		a, a:hovor {
			color: #000000;
			text-decoration: none;		
		}
	</style>
</head>
<body>
	<%
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
		}
		int pageNumber = 1;	//기본 페이지
		if(request.getParameter("pageNumber") != null){
			pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
		}
	%>
	<nav class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="home">Simple Invoice</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="home">메인</a></li>
				<li><a href="bbsList">고객센터</a></li>
				<li><a href="invoice_list">인보이스 조회</a></li>
				<li><a href="bbsList_datatable">인보이스 입력</a></li>
				<li><a href="category_insertform">카테고리 등록</a></li>
				<li class="active"><a href="category_list">카테고리 조회</a></li>
				<li><a href="product_insertform">상품 등록</a></li>
				<li><a href="product_list">상품 조회</a></li>
			</ul>
			<%
				if(userID == null) {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="login">회원가입</a></li>
				<li><a href="login">로그인</a></li>
			</ul>
			
			<%
				} else {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="logout">로그아웃</a></li>
				<li><a href="changeInfo">개인정보 변경</a></li>
			</ul>
			
			<%
				}
			%>
		</div>
	</nav>
	
	<div class="container">
		<div class="content-container">
            <section>
            </section>
        </div>
		<div class="row">
			<div>
				<p>
				    <button type="button" class="btn btn-success" onclick="location.href='insert_form'"><i class="fa fa-plus"></i> 카테고리 등록</button>
				</p>
			</div>
		
			<div>
				<table class="table table-bordered table-hover table-striped" id="datatable" style="width:100%">
					<thead>
						<tr>
							<th style="background-color: #eeeeee">category_id</th>
							<th style="background-color: #eeeeee">category_name</th>
							<th style="background-color: #eeeeee">description</th>
							<th style="background-color: #eeeeee">is_active</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="category" items="${categoryList}">
						<tr>
							<td align=left>${category.category_id }</td>
							<td align=left><a href="category_view?category_id=${category.category_id}">${category.category_name}</a></td>
							<td align=left>${category.description }</td>
							<td align=left>${category.is_active }</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
	</div>
	
	<script type="text/javascript" src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js"></script>
		
	<script>
		$(document).ready(function(){
			$("#datatable").DataTable();
		})
	</script>	
		
		
</body>
</html>