package com.magicoh.springmvc.dao;

import java.util.List;

import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.Client;
import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.Product;

@MyMapper 
public interface IBbsMapper
{
	//移댄뀒怨좊━ 留ㅽ띁
	public List<Category> selectCategoryList();
	public Category selectCategory(int category_id);
	public void insertCategory(Category category);
	public void updateCategory(Category category);
	public void deleteCategory(int category_id);

	//�긽�뭹�젙蹂� 留ㅽ띁
	public List<Product> selectProductList();
	public List<Product> selectProductsByName(String product_name);
	public Product selectProduct(int product_id);
	public void insertProduct(Product product);
	public void updateProduct(Product product);
	public void deleteProduct(int product_id);
	
	//Client
	public List<Client> selectClientList();
	public Client selectClient(int client_id);	
	
	//�씤蹂댁씠�뒪 留ㅽ띁
	public List<InvoiceHeader> getInvoiceList();
	public List<InvoiceDetail> getInvoiceDetailList();
	public InvoiceHeader selectInvoiceHeader(String invoice_no);
	public void insertInvoice(InvoiceHeader invoice);
//	public void updateProduct(Product product);
//	public void deleteProduct(int product_id);
	
}
