package com.magicoh.springmvc.service;


import java.util.List;

import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.Client;

public interface IClientService
{
	public List<Client> getClientList();
	public Client getClient(int client_id);
//	public void insertCategory(Category category);
//	public void updateCategory(Category category);
//	public void deleteCategory(int category_id);
}
