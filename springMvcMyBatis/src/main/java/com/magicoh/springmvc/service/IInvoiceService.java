package com.magicoh.springmvc.service;


import java.util.List;

import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.Product;

public interface IInvoiceService
{
	public List<InvoiceHeader> getInvoiceList();
	public List<InvoiceDetail> getInvoiceDetailList();
	public InvoiceHeader getInvoiceHeader(String invoice_no);
	public void insertInvoice(InvoiceHeader invoiceHeader);
//	public void updateProduct(Product product);
//	public void deleteProduct(int product_id);
}
