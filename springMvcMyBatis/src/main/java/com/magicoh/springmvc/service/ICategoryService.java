package com.magicoh.springmvc.service;


import java.util.List;

import com.magicoh.springmvc.dto.Category;

public interface ICategoryService
{
	public List<Category> getCategoryList();
	public Category getCategory(int category_id);
	public void insertCategory(Category category);
	public void updateCategory(Category category);
	public void deleteCategory(int category_id);
}
