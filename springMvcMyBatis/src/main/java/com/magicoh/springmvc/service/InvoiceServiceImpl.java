package com.magicoh.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magicoh.springmvc.dao.IBbsMapper;
import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.Product;

@Service
//@Service("IProductService")
public class InvoiceServiceImpl implements IInvoiceService
{
	@Autowired
	private IBbsMapper bbsMapper;

	@Override
	public List<InvoiceHeader> getInvoiceList()
	{
		List<InvoiceHeader> invoiceHeaderList = bbsMapper.getInvoiceList();
		return invoiceHeaderList;
	}

	@Override
	public List<InvoiceDetail> getInvoiceDetailList()
	{
		List<InvoiceDetail> invoiceDetailList = bbsMapper.getInvoiceDetailList();
		return invoiceDetailList;
	}

	@Override
	public void insertInvoice(InvoiceHeader invoiceHeader)
	{
		bbsMapper.insertInvoice(invoiceHeader);
	}

	
	@Override
	public InvoiceHeader getInvoiceHeader(String invoice_no)
	{
		InvoiceHeader invoiceHeader = bbsMapper.selectInvoiceHeader(invoice_no);
		return invoiceHeader;
	}

	/**

	@Override
	public void updateProduct(Product product)
	{
		bbsMapper.updateProduct(product);
	}

	@Override
	public void deleteProduct(int product_id)
	{
		bbsMapper.deleteProduct(product_id);
	}
	**/
}
