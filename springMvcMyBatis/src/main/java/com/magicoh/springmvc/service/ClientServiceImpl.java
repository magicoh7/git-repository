package com.magicoh.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magicoh.springmvc.dao.IBbsMapper;
import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.Client;

@Service
public class ClientServiceImpl implements IClientService
{
	@Autowired
	private IBbsMapper bbsMapper;

	@Override
	public List<Client> getClientList()
	{
		List<Client> bbsList = bbsMapper.selectClientList();
		return bbsList;
	}

	@Override
	public Client getClient(int client_id)
	{
		Client client = bbsMapper.selectClient(client_id);
		return client;
	}	

}
