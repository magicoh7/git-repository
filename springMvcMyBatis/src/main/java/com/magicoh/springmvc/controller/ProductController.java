package com.magicoh.springmvc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.Product;
import com.magicoh.springmvc.service.ICategoryService;
import com.magicoh.springmvc.service.IProductService;

@Controller
public class ProductController
{
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private IProductService productService;

	@RequestMapping(value = {"/", "/home", "/index"}, method = RequestMethod.GET)
	public String home() 
	{
		
		return "home";
	}
	
	/***************************************
	 * 移댄뀒怨좊━ �벑濡�/議고쉶/�닔�젙/�궘�젣
	 **************************************/
	
	//�쟾泥� 移댄뀒怨좊━ 紐⑸줉 議고쉶
	@RequestMapping(value = "/category_list", method = RequestMethod.GET)
	public String getCategoryList(Model model) throws Exception
	{
		List<Category> categoryList = categoryService.getCategoryList();
		
		model.addAttribute("categoryList", categoryList);
		
		return "category_list";
	}
	
	//1媛� 移댄뀒怨좊━ 議고쉶
	@RequestMapping(value = "/category_view", method = RequestMethod.GET)
	public ModelAndView getCategory(@RequestParam int category_id) 
	{
		Category category = categoryService.getCategory(category_id);
		
		return new ModelAndView("category_view", "category", category);
	}
	
	//移댄뀒怨좊━ �엯�젰 �뤌
	@RequestMapping(value = "/category_insertform", method = RequestMethod.GET)
	public ModelAndView categoryInsertForm(Model model)
	{
		return new ModelAndView("category_insertform");
	}

	//移댄뀒怨좊━ ���옣 泥섎━
	@RequestMapping(value = "/category_insert", method = RequestMethod.POST)
	public String categoryInsert(@ModelAttribute Category category)
	{
		if(category != null)
		{
			categoryService.insertCategory(category);
		}
		
		return "redirect:category_list";
	}
	
	//移댄뀒怨좊━ �닔�젙
	@RequestMapping(value = "/category_updateform", method = RequestMethod.GET)
	public ModelAndView categoryUpdateForm(@RequestParam int category_id)
	{
		Category category = categoryService.getCategory(category_id);
				
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("category", category);
		
		return new ModelAndView("category_updateform", "map", map);
	}
	
	@RequestMapping(value = "/category_update", method = RequestMethod.POST)
	public String categoryUpdate(@ModelAttribute Category category)
	{
		categoryService.updateCategory(category);
		
		return "redirect:category_list";
	}
	
	@RequestMapping(value = "/category_delete", method = RequestMethod.GET)
	public String categoryDelete(@RequestParam int category_id)
	{
		categoryService.deleteCategory(category_id);
		
		return "redirect:category_list";
	}
	
	
	/***************************************
	 * �긽�뭹�젙蹂� �벑濡�/議고쉶/�닔�젙/�궘�젣
	 **************************************/
	
	//�쟾泥� �긽�뭹�젙蹂� 紐⑸줉 議고쉶
	@RequestMapping(value = "/product_list", method = RequestMethod.GET)
	public String getProductList(Model model) throws Exception
	{
		List<Product> productList = productService.getProductList();
		
		model.addAttribute("productList", productList);
		
		return "product_list";
	}
	
	//1媛� �긽�뭹 議고쉶
	@RequestMapping(value = "/product_view", method = RequestMethod.GET)
	public ModelAndView getProduct(@RequestParam int product_id) 
	{
		Product product = productService.getProduct(product_id);
		
		return new ModelAndView("product_view", "product", product);
	}
	
	//�긽�뭹 �젙蹂� �엯�젰�뤌
	@RequestMapping(value = "/product_insertform", method = RequestMethod.GET)
	public ModelAndView productInsertForm(Model model)
	{
		//�긽�뭹 �엯�젰�뤌�뿉 移댄뀒怨좊━ �꽭�똿
		List<Category> categoryList = categoryService.getCategoryList();
				
		return new ModelAndView("product_insertform", "categoryList", categoryList);
	}

	
	//�긽�뭹 ���옣 泥섎━
	@RequestMapping(value = "/product_insert", method = RequestMethod.POST)
	public String insertProduct(@ModelAttribute Product product)
	{
		if(product != null)
		{
			productService.insertProduct(product);
		}
		
		return "redirect:product_list";
	}
	
	
	//�긽�뭹�젙蹂� �닔�젙�뤌
	@RequestMapping(value = "/product_updateform", method = RequestMethod.GET)
	public ModelAndView updateForm(@RequestParam int product_id)
	{
		//get product info
		Product product = productService.getProduct(product_id);
				
		//get category info
		List<Category> categoryList = categoryService.getCategoryList();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("product", product);
		map.put("categoryList", categoryList);
		
		return new ModelAndView("product_updateform", "map", map);

	}

	//�긽�뭹�젙蹂� �닔�젙 泥섎━
	@RequestMapping(value = "/product_update", method = RequestMethod.POST)
	public String productUpdate(@ModelAttribute Product product)
	{
		productService.updateProduct(product);
		
		return "redirect:product_list";
	}

	
	
	
	
	
	
	
	
	
	public String handleExString(Exception e)
	{
		return "viewError";
	}
}
