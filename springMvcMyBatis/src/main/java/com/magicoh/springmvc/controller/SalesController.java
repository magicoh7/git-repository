package com.magicoh.springmvc.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.Client;
import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.Product;
import com.magicoh.springmvc.service.ICategoryService;
import com.magicoh.springmvc.service.IClientService;
import com.magicoh.springmvc.service.IInvoiceService;
import com.magicoh.springmvc.service.IProductService;

@Controller("/sales")
public class SalesController {
	@Autowired
	private IInvoiceService invoiceService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private IProductService productService;

	
	/***************************************
	 *카테고리 등록/조회/수정/삭제
	 **************************************/
	
	//전체 인보이스 목록 조회
	@RequestMapping(value = "/invoice_list", method = RequestMethod.GET)
	public String getInvoiceList(Model model) throws Exception
	{
		List<InvoiceHeader> invoiceList = invoiceService.getInvoiceList();
		
		model.addAttribute("invoiceList", invoiceList);
		
		return "/sales/invoice_list";
	}
	
	//Invoice insert form
	@RequestMapping(value = "/invoice_insertform", method = RequestMethod.GET)
	public ModelAndView invoiceInsertForm(Model model)
	{
		//initialize insert from
		List<Category> categoryList = categoryService.getCategoryList(); 	//Category
		List<Client> clientList = clientService.getClientList();			//Client
		List<Product> productList = productService.getProductList(); 		//product
		
		String invoice_no = "0";
		InvoiceHeader invoiceHeader = invoiceService.getInvoiceHeader(invoice_no);
		List<InvoiceDetail> invoiceDetailList = invoiceService.getInvoiceDetailList();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("categoryList", categoryList);
		map.put("clientList", clientList);
		map.put("productList", productList);
		//map.put("invoiceHeader", invoiceHeader);
		map.put("invoiceHeader", new InvoiceHeader());
		map.put("invoiceDetailList", invoiceDetailList);
		
		return new ModelAndView("/sales/invoice_insertform", "map", map);
	}
	
	//상품 저장 처리
	@RequestMapping(value = "/invoice_insert", method = RequestMethod.POST)
	public String insertInvoice(@ModelAttribute InvoiceHeader invoice)
	{
		if(invoice != null)
		{
			invoiceService.insertInvoice(invoice);
		}
		
		return "redirect:invoice_list";
	}

	@ResponseBody
	@RequestMapping(value = "/sales/product_list_modal", method = RequestMethod.GET)
	public String getProductList(ModelMap model, @RequestParam("product_name") String product_name, @RequestParam("parentsLowNo") String parentsLowNo) throws Exception
	{
		//json으로 넘길 때 사용
		//String json = null;        
        //ObjectMapper objectMapper = new ObjectMapper();  


        List<Product> productList = productService.getProductsByName(product_name);
        model.addAttribute("productList", productList);

        //json = objectMapper.writeValueAsString(productList);            
        
        return "/sales/invoice_insertform :: modalContents";
	        
		
		
		
	}
	
}
