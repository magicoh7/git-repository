package com.magicoh.springmvc.dto;

public class Product {

	private int product_id = 0;			//�긽�뭹ID   
	private String product_name = ""; 	//�긽�뭹紐�
	private String brand = "";        	//釉뚮옖�뱶紐�
	private String description = "";  	//�꽕紐�
	private double unit_price = 0.0;   	//�떒媛�
	private int category_id = 0; 		//移댄뀒怨좊━ID 
	private int is_active = 1;	   		//�궗�슜�쑀臾�
	
	public Product() {
	}

	public Product(int product_id, String product_name, String brand, String description, double unit_price,
			int category_id, int is_active) {
		this.product_id = product_id;
		this.product_name = product_name;
		this.brand = brand;
		this.description = description;
		this.unit_price = unit_price;
		this.category_id = category_id;
		this.is_active = is_active;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(double unit_price) {
		this.unit_price = unit_price;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getIs_active() {
		return is_active;
	}

	public void setIs_active(int is_active) {
		this.is_active = is_active;
	}
	
	
	
}
