package com.magicoh.springmvc.dto;

import java.util.Date;
import java.util.List;

public class InvoiceHeader {

	private String invoice_no;      	
	private int client_id;       		
	private String shipping_address;	
	private double total_amt;       	
	private String description;    
	private Date created_date;   
	private Date modified_date;	
	private List<InvoiceDetail> invoiceDetails;
	private Client client;			
	
	public InvoiceHeader() {
	}
	
	public InvoiceHeader(String invoice_no, int client_id, String shipping_address, double total_amt,
			String description, Date created_date, Date modified_date) {
		this.invoice_no = invoice_no;
		this.client_id = client_id;
		this.shipping_address = shipping_address;
		this.total_amt = total_amt;
		this.description = description;
		this.created_date = created_date;
		this.modified_date = modified_date;
	}
	
	public List<InvoiceDetail> getInvoiceDetail() {
		return invoiceDetails;
	}

	public void setInvoiceDetail(List<InvoiceDetail> invoiceDetails) {
		this.invoiceDetails = invoiceDetails;
	}
	
		
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getInvoice_no() {
		return invoice_no;
	}
	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}
	public int getClient_id() {
		return client_id;
	}
	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}
	public String getShipping_address() {
		return shipping_address;
	}
	public void setShipping_address(String shipping_address) {
		this.shipping_address = shipping_address;
	}
	public double getTotal_amt() {
		return total_amt;
	}
	public void setTotal_amt(double total_amt) {
		this.total_amt = total_amt;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getModified_date() {
		return modified_date;
	}
	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}   
		
	
	
	
}
