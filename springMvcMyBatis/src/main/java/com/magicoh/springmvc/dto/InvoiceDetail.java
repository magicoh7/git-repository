package com.magicoh.springmvc.dto;

import java.util.Date;

public class InvoiceDetail {

	private String invoice_no;   	
	private int seq;          		
	private int product_id;   		
	private String product_name; 	
	private double unit_price;   	
	private int quantity;     		
	private double total_amt;    	
	private Date created_date; 		
	private Date modified_date;		
	
	public InvoiceDetail() {
	}

	public InvoiceDetail(String invoice_no, int seq, int product_id, String product_name, double unit_price,
			int quantity, double total_amt, Date created_date, Date modified_date) {
		this.invoice_no = invoice_no;
		this.seq = seq;
		this.product_id = product_id;
		this.product_name = product_name;
		this.unit_price = unit_price;
		this.quantity = quantity;
		this.total_amt = total_amt;
		this.created_date = created_date;
		this.modified_date = modified_date;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public double getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(double unit_price) {
		this.unit_price = unit_price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getTotal_amt() {
		return total_amt;
	}

	public void setTotal_amt(double total_amt) {
		this.total_amt = total_amt;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getModified_date() {
		return modified_date;
	}

	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}
	
	
	
	
}
