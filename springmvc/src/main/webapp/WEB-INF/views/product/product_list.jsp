<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/views/shared/header.jsp" %>
	
	<div class="container">
		<div class="content-container">
            <section>
            </section>
        </div>
		<div class="row">
			<div>
				<p>
				    <button type="button" class="btn btn-success" onclick="location.href='product_insertform'"><i class="fa fa-plus"></i> New Product</button>
				</p>
			</div>
			<div id="wrap" align="center" class="table-responsive" >
			    <form name="productForm" method="get">
			    	<input type="hidden" name="category_id" type="number"/>
			        <div class="col-sm-12 pt-3">
			            <div class="card">
			                <div class="card-header card-header-primary">
			                    <h4 class="card-title"><i class="fa fa-plus"></i>상품 정보 조회</h4>
			                </div>
			                <div class="card-body">
			                    <div class="table-responsive">
			                        <table class="table">
			                            <tbody>
			                            <tr style="line-height:32px;">
			                                <td>상품 ID</td>
			                                <td >
			                                    <input type="number" name="product_id" class="form-control" value="">
			                                </td>
			                                <td>상품 이름</td>
			                                <td>
			                                    <input type="text" name="product_name" class="form-control" value="">
			                                </td>                        
			                            </tr>
			                            <tr>
			                                <td>브랜드명</td>
			                                <td>
			                                    <input type="text" name="brand" class="form-control" maxlength="10" value="">                                    
			                                </td>
			                                <td>카테고리명</td>
			                                <td>
			                                    <input type="text" name="category_name" class="form-control" maxlength="10" value="">                                    
			                                </td>
			                            </tr>  
			                            <tr>
			                                <td>단가</td>
			                                <td>
			                                    <input type="text" type="number" name="unit_price" class="form-control" maxlength="10" value="">                                    
			                                </td>
			                                <td>사용유무</td>
			                                <td>
			                                    <input type="text" name="is_active" class="form-control" maxlength="10" value="">                                    
			                                </td>
			                            </tr>  
			                            <tr>
			                            	<td colspan="2" >
												<button type="submit" id="btnSearch" class="btn btn-success">조회</button>
												<input type="button" class="btn btn-info" onclick="javascript:location.href='productList.do'" value="전체보기" />
											</td>
			                            </tr>  
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </form>		
			    	
				<table class="table table-bordered table-hover table-striped" id="datatable" style="width:100%">
					<thead>
						<tr>
							<th style="background-color: #eeeeee">product_id</th>
							<th style="background-color: #eeeeee">product_name</th>
							<th style="background-color: #eeeeee">brand</th>
							<th style="background-color: #eeeeee">unit_price</th>
							<th style="background-color: #eeeeee">category_id</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="product" items="${productList}">
						<tr>
							<td align=left>${product.product_id }</td>
							<td align=left><a href="product_view?product_id=${product.product_id}">${product.product_name}</a></td>
							<td align=left>${product.brand }</td>
							<td align=left>${product.unit_price }</td>
							<td align=left>${product.category_id }</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
	</div>
	
	<script type="text/javascript" src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js"></script>
	<script src="<c:url value="/resources/js/jquery.serialize-object.js" />"></script>
		
	<script>
		$(document).ready(function(){
// 			$("#datatable").DataTable({
// 				"lengthChange": false,
// 				"searching": false
// 			});
			
			//search 
            $('#btnSearch').on('click', function () {
            	//alert('btnSearch click event');
            	event.preventDefault(); 	
            	//var client_name = $('.client_name').val();
                Fn_Search_Product();
            });
			
			//[serializeObject]
// 			jQuery.fn.serializeObject = function() {
// 			    var obj = null;
// 			    alert('serializeObject!!!!!!');
// 			    try {
// 			        if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
// 			            var arr = this.serializeArray();
// 			            if (arr) {
// 			                obj = {};
// 			                jQuery.each(arr, function() {
// 			                    obj[this.name] = this.value;
// 			                });
// 			            }//if ( arr ) {
// 			        }
// 			    } catch (e) {
// 			        alert(e.message);
// 			    } finally {
// 			    }
// 			    return obj;
// 			};
			
			
		});
		

		
		
		//search client (actually use)
		function Fn_Search_Product() {
			var url = "product_list_search";
			var data = $("form[name=productForm]").serializeObject();
			//alert(JSON.stringify(data));
			
			$.ajax({
				type : "POST",
				url : url,
				data : JSON.stringify(data), 
				contentType : "application/json; charset=utf-8",
				dataType : "html",
				success : function(response) {
					//alert(response);
					
					var jasonData = JSON.parse(response);
					$('#datatable tr:not()').remove();
	
					var text = "";
					text += "<tr>\n";
					text += "<th style='background-color: #eeeeee; text-align:center;'>product_id</th> \n";
					text += "<th style='background-color: #eeeeee; text-align:center;'>product_name</th> \n";
					text += "<th style='background-color: #eeeeee; text-align:center;'>brand</th> \n";
					text += "<th style='background-color: #eeeeee; text-align:center;'>unit_price</th> \n";
					text += "<th style='background-color: #eeeeee; text-align:center;'>category_id</th> \n";
					text += "</tr>\n";
					
					for (var i = 0; i < jasonData.length; i++) {
						text += "<tr>\n";
						text += "<td>" + jasonData[i].product_id	+ "</td>\n";
						text += "<td><a href='product_view?product_id=" + jasonData[i].product_id + "'>"	+ jasonData[i].product_name	+ "</a></td>\n";
						text += "<td>" + jasonData[i].brand	+ "</td>\n";
						text += "<td>" + jasonData[i].unit_price	+ "</td>\n";
						text += "<td>" + jasonData[i].category_id	+ "</td>\n";
						text += "</tr>\n";
					}
					$('#datatable').append(text);
				},
				failure : function(response) {
					alert('2');
					//alert(response);
				},
				error : function(xhr, status) {
					alert('3');
					alert(xhr.responseText);
				}
			});
		}		
	</script>	
		
		
</body>
</html>