package com.magicoh.view.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.magicoh.biz.user.UserVo;
import com.magicoh.biz.user.impl.UserDao;

public class LoginController implements Controller {

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("로그인 처리");
		
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		
		UserVo vo = new UserVo();
		vo.setId(id);
		vo.setPassword(password);
		
		UserDao userDAO = new UserDao();
		UserVo user = userDAO.getUser(vo);
		
		ModelAndView mav = new ModelAndView();
		if(user != null){	
			//return "getBoardList.do";	//해당 로직 처리후 뭔가 보여줄 정보가 있는 경우
			mav.setViewName("redirect:getBoardList.do");
		}else{
			//return "login";		//해당 로직 처리후 보여줄 정보가 없는 경우는 그냥 login.jsp(폼)를 보여주도록
			mav.setViewName("redirect:login.jsp");
		}
		return mav;
	}



}
