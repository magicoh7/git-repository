<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>  
<%@ include file="../header.jsp" %>  
<!-- %@ include file="sub_img.html"%--> 
<%@ include file="sub_menu.html" %>    
   
 <article>
	<!-- content-->
		<form method="post" action="ShoppingServlet?command=login" id="login_form" name="formm">
		<div id="login_content">
			<div>
				<h3>
					<label for="id">아이디</label>
				</h3>
				<span> <input type="text" id="login_id" name="mem_id"
					size="12" class="join_input" maxlength="20" required="required"> 
					<input type="hidden" name="reid" id="reid" value="no">
				</span> 
				<span class="error_next_box"> </span>
				<div class="check_font" id="login_id_error"></div>
			</div>
			<!-- PW -->
			<div>
				<h3>
					<label for="pwd1">비밀번호</label>
				</h3>
				<span> <input type="password" id="login_pwd" name="pwd" class="join_input" maxlength="20" required="required"> 
				<span id="alertTxt">사용불가</span>
				</span> <span class="error_next_box"></span>
				<div class="check_font" id="login_pwd_error"></div>
			</div>
			<div class="clear"></div>
			<!-- 가입하기 -->
	        <div id="buttons">
	            <input type="button" value="로그인" class="submit" id="btnLogin" >
	            <input type="button" value="회원가입" class="cancel"
	                 onclick="location='ShoppingServlet?command=join_form'">
	            <input type="button" value="아이디 비밀번호 찾기" class="submit"
	                 onclick="location='ShoppingServlet?command=find_id_form'">     
	        </div>
		</div>
		</form>
	<!-- content-->
</article>
  
<%@ include file="../footer.jsp" %>      
