<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../header.jsp"%>

	<script type="text/javascript">
	</script>

<!-- wrapper -->
<div id="wrapper">
<article>
	<!-- content-->
	<div id="join_content">
		<form id="join_form" method="post"	name="formm">
			<div>
				<h3>
					<label for="id">아이디</label>
				</h3>
				<span> <input type="text" id="mem_id" name="mem_id"
					size="12" class="join_input" maxlength="20" required="required"> 
					<input type="hidden" name="reid" id="reid" value="no">
				</span> <span class="error_next_box"> </span>
				<div class="check_font" id="id_check"></div>
			</div>
			<!-- PW1 -->
			<div>
				<h3>
					<label for="pwd1">비밀번호</label>
				</h3>
				<span> <input type="password" id="pwd" name="pwd" class="join_input" maxlength="20" required="required"> 
				<span id="alertTxt">사용불가</span>
				</span> <span class="error_next_box"></span>
			</div>
			<!-- PW2 -->
			<div>
				<h3>
					<label for="pwd2">비밀번호 확인</label>
				</h3>
				<span> <input type="password" id="pwdConfirm" name="pwdConfirm" class="join_input" 
						maxlength="20" required="required">
				</span> <span class="error_pwdConfirm"></span>
				<div class="check_font" id="error_pwdConfirm"></div>
			</div>
			<!-- NAME -->
			<div>
				<h3>
					<label for="name">이름</label>
				</h3>
				<span> <input type="text" name="name"
					class="join_input" maxlength="20" required="required">
				</span> <span class="error_next_box"></span>
				<div class="check_font" id="error_name"></div>
			</div>
			<!-- EMAIL -->
			<div>
				<h3>
					<label for="email">이메일<span class="optional"></span></label>
				</h3>
				<span> <input type="email" id="email" name="email"	class="join_input" maxlength="100" required="required" />
				</span><input type="hidden" name="reemail"  id="reemail" value="no"> 
				<span class="error_next_box"></span>
				<div class="check_font" id="error_email"></div>
			</div>
			<!-- MOBILE -->
			<div>
				<h3>
					<label for="phoneNo">연락처</label>
				</h3>
				<span> <input type="tel" name="phone" id="phone"
					class="join_input" maxlength="16" placeholder="전화번호 입력" required="required">
				</span><input type="hidden" name="rephone" id="rephone" value="no"> 
				<span class="error_next_box"></span>
				<div class="check_font" id="error_phone"></div>
			</div>

			<!-- 우편번호 -->
			<div>
				<h3>
					<label for="yy">우편번호</label>
				</h3>
				<div id="bir_wrap">
					<div id="bir_yy">
						<span> 
						<input type="text" name="zipNum" id="zipNum" class="join_input" size="10" maxlength="6">
						</span>
					</div>
					<div id="bir_mm">
						<span > 
							<input type="button" value="주소찾기" class="dup" onclick="post_zip()">
						</span>
					</div>
					
				</div>
				<span class="error_next_box"></span>
			</div>
			<!-- 주소 -->
			<div>
				<div id="bir_wrap">
					<div id="bir_yy">
						<span> 
						<input type="text" name="addr1" id="addr1" class="join_input" size="10" maxlength="6" required="required">
						</span>
						<div class="check_font" id="error_addr1"></div>
					</div>
					<div id="bir_yy">
						<span class="box"> 
						<input type="text" name="addr2" id="addr2" class="join_input" size="10" maxlength="6" required="required">
						</span>
						<div class="check_font" id="error_addr2"></div>
					</div>
				</div>
				<span class="error_next_box"></span>
			</div>

			<!-- 가입하기 -->
			<div class="btn_area">
				<button type="button" id="btnJoin" onclick="go_save()">
					<span>가입하기</span>
				</button>
			</div>
		</form>
	</div>
	<!-- content-->
</article>
</div>

<%@ include file="../footer.jsp"%>
