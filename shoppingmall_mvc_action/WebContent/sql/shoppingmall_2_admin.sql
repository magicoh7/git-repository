-- drop table worker;
create table worker(          
    id          varchar2(20)  primary key,
    pwd         varchar2(20),
    name        varchar2(40),
    phone       varchar2(20)
);
insert into worker values('admin', '1234', '홍관리', '010-777-7777');
insert into worker values('pinksung', '1234', '명강사', '010-999-9696');

-- drop table member;
--//구형 member
create table member_old(   
    id         varchar2(20)  primary key,
    pwd        varchar2(20),     
    name       varchar2(40),
    email      varchar2(40),
    zip_num    varchar2(7),
    address    varchar2(100),
    phone      varchar2(20),
    useyn      char(1)       default 'y',
    regdate     date          default sysdate
);

alter table member rename  to member_old
alter table member1 rename  to member
commit
--// 신형 member
create table member(
    mem_code number(5),
    mem_id varchar2(20) not null,
    pwd varchar2(20) not null,
    name varchar2(40) not null,
    email varchar2(40),
    phone char(15),
    zip_num char(7),
    address1 varchar2(50),
    address2 varchar2(50),
    previledge char(1) default '0',
    useyn char(1) default 'y',
    regdate date default sysdate,
    moddate date default sysdate,
    constraint pk_member primary key(mem_code)
);
create sequence seq_member_code start with 1 increment by 1;
ALTER TABLE member ADD CONSTRAINT mem_id_uk UNIQUE (mem_id);

Select mem_id, name, email, zip_Num, address1, address2, phone, useyn, regdate
from member


insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'magicoh', '1234', '매직오', '133-110', '서울시성동구성수동1가', '1번지21호', '010-1387-5678');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'two', '1234', '이백합', '130-120', '서울시송파구잠실2동', '리센츠 아파트 201동 505호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'marry', '1234', '결혼이', '130-120', '서울시관악구 삼성동', '삼성산뜨란채 307동 1402호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'would', '1234', '권유', '130-120', '서울시관악구 삼성동', '삼성산뜨란채 307동 1402호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'going', '1234', '추진력', '130-120', '서울시 강서구 수박동', '이편한세상 302동 1010호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'gogogo', '1234', '활동가', '130-120', '서울시 강서구 수박동', '이편한세상 302동 1010호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'duriahn', '1234', '과일님', '130-120', '서울시 강서구 수박동', '이편한세상 302동 1010호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'forbos', '1234', '포보스', '130-120', '경기도 수원시 세류동', '레볼루션 2동 1101호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'couldyou', '1234', '권유형', '130-120', '경기도 수원시 세류동', '레볼루션 2동 1101호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'billhalf', '1234', '반쪽빌', '130-120', '경기도 수원시 세류동', '레볼루션 2동 1101호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'trueworld', '1234', '정직세상', '130-120', '경기도 수원시 세류동', '레볼루션 2동 1101호', '010-2323-6858');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'badasori', '1234', '이어부', '130-120', '서울시 동작구 사당2동', '세트레빌 101동 1513호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'dudungsil', '1234', '설렘이', '130-120', '서울시 동작구 사당2동', '세트레빌 101동 1513호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'buble', '1234', '거품목욕', '130-120', '서울시 동작구 사당2동', '세트레빌 101동 1513호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'peace', '1234', '우리평화', '130-120', '서울시 동작구 사당2동', '세트레빌 101동 1513호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'east', '1234', '서쪽', '130-120', '서울시 구로구 구로2동', '레미안 아파트 301동 1513호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'round', '1234', '둥근이', '130-120', '서울시 구로구 구로2동', '레미안 아파트 301동 1513호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'sunflower', '1234', '해바라기', '130-120', '서울시 구로구 구로2동', '레미안 아파트 301동 1513호', '011-123-4567');
insert into member(mem_code, mem_id, pwd, name, zip_num, address1, address2, phone) values(seq_member_code.nextval, 'thunder', '1234', '태풍이', '130-120', '서울시 구로구 구로2동', '레미안 아파트 301동 1513호', '011-123-4567');

commit

select * from member1
desc member

update member set email='dreamcometrue@google.com'

select *  From member  Where mem_id='magicoh'


-- drop table product;
Create table product(
    code number(5),
    name varchar2(100),
    kind char(1),
    cost_price number(8) default 0,
    list_price number(8) default 0,
    sales_margin number(8) default 0,
    content varchar2(1000),
    image varchar2(50) default 'default.jpg',
    useyn char(1)  default 'y',
    bestyn char(1)  default 'n',
    regdate date default sysdate,
   primary key(code)
)
Create sequence product_code_seq increment by 1 start with 1



Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, 'CK진 남여 간절기 데일리룩', '1', 23000, 25000, 2000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate)
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '완벽한 니트 베스트 상품 ', '1', 23000, 25000, 2000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '킨록 by 킨록앤더슨', '1', 23000, 25000, 2000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '타미진 모던시크 블랙 룩', '1', 23000, 25000, 2000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '디젤 프리뷰 신상 코디룩', '1', 23000, 25000, 2000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '타미진 데님 스타일링 코디 상품', '2', 20000, 17000, 3000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '컨셉원 감성코디 포인트룩', '2', 20000, 17000, 3000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '리버클래시 데일리룩', '2', 20000, 17000, 3000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '간절기 데일리 커플룩 제안', '2', 20000, 17000, 3000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '세련되고 멋스러운 데일리룩', '2', 20000, 17000, 3000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '리바이스 가을 신상 코디', '2', 20000, 17000, 3000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '미리 만나는 NBA 가을룩', '3', 13200, 10000, 3200, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '0', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '팀버랜드 데일리룩', '3', 13200, 10000, 3200, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '0', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '간절기 플리스 커플룩 제안', '3', 13200, 10000, 3200, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '0', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, 'NBA 가을 신상 커플코디룩', '4', 56000, 50000, 6000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '미리 준비하는 가을 아이템!', '4', 56000, 50000, 6000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '눈에 밟히는 그녀의 신상룩', '4', 56000, 50000, 6000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '컨셉원 데님과 셔켓 코디제안', '4', 56000, 50000, 6000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '예작 아트셔츠', '4', 56000, 50000, 6000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '지오다노 캐주얼 코디 룩', '5', 79000, 70000, 9000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '여름 커플 아이템', '5', 79000, 70000, 9000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '클라이드앤 커플 스트릿룩', '5', 79000, 70000, 9000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '엄브로 8월의 데일리룩', '5', 79000, 70000, 9000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '타미진 그레이&네이비 코디룩', '5', 79000, 70000, 9000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '리바이스 클래식 청남방 코디', '5', 79000, 70000, 9000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '리바이스 데님 데일리룩', '5', 79000, 70000, 9000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '1', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '매일 신기 편한 스트랩 샌들', '6', 120000, 80000, 32000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '베이지 커플시밀러룩', '6', 120000, 80000, 32000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '시원한느낌의 바캉스 룩', '6', 120000, 80000, 32000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, 'CK진의 장마철 필수템!', '6', 120000, 80000, 32000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '리바이스의 여름휴가룩', '6', 120000, 80000, 32000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, 'NBA 스타일리쉬한 커플룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '가을 스타일리쉬 코디룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '시원한 로고티셔츠 코디 제안', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, 'CK진 여름 데일리룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '타미진 데일리 코디룩~', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '스누피와 루즈핏으로 편한 룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '누구나 편안하게 고민 없이 백팩 PICK', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '뉴에라 스포티 커플 코디룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '여름 일상룩 코디 제안 !!', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '장마철 편안한 캐주얼룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '타미진 스트라이프 커플룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;
Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) Values(product_code_seq.nextval, '캘빈클라인 데일리 스포티룩', '7', 70000, 50000, 20000, 'CK진 남여 간절기 데일리룩', 'noImage.jpg', '1', '0', sysdate) ;


Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '크로그다일부츠', '2', 40000, 50000, 10000, '오지니랄 크로그다일부츠 입니다.', 'w2.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '롱부츠', '2', 23000, 26000, 3000, '따뜻한 롱부츠 입니다..', 'w-28.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '힐', '2', 15500, 16500, 1000, '여성용전용 힐', 'w-26.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '롱부츠', '2', 80000, 95000, 15000, '따뜻한 롱부츠 입니다..', 'w-28.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '슬리퍼', '2', 20000, 27000, 7000, '편안한 슬리퍼입니다.', 'w-25.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '회색힐', '2', 60000, 75000, 15000, '여성용전용 힐', 'w9.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '여성부츠', '2', 10000, 15000, 5000, '여성용 부츠', 'w4.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '핑크샌달', '2', 35000, 48000, 18000, '사계절용 샌달입니다.', 'w-10.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '슬리퍼', '2', 55000, 75000, 20000, '편안한 슬리퍼입니다.', 'w11.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '스니커즈', '2', 30000, 5000, 20000, '활동성이 좋은 스니커즈입니다.', 'w1.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '샌달', '2', 15000, 25500, 10500, '사계절용 샌달입니다.', 'w-09.jpg', '1', '1', sysdate);

Insert Into product(code, name, kind, cost_price, list_price, sales_margin,content, image, useyn, bestyn, regdate) 
Values(product_code_seq.nextval, '스니커즈', '2', 85000, 95000, 10000, '활동성이 좋은 스니커즈입니다.', 'w-05.jpg', '1', '1', sysdate);



-- // Best Product View  //----------------------------------------------------------------------------------------
create or replace view best_product_view
as
select p.code as code, p.name as name, p.list_price as list_price, p.image as image
from product p
where p.useyn = 'y'
and p.bestyn = 'y'
and rownum < 5

-- // New Product View  //----------------------------------------------------------------------------------------
create or replace view new_product_view
as
select p.code as code, p.name as name, p.list_price as list_price, p.image as image, p.regdate regdate
from product p
where p.useyn = 'y'
and p.bestyn = 'n'
and rownum < 5
order by regdate desc

-- // All Product View  //----------------------------------------------------------------------------------------
create or replace view all_product_view
as
select p.code as code, p.name as name, p.list_price as list_price, p.image as image, p.regdate regdate
from product p
where p.useyn = 'y'

commit

Create Table address(
    zip_num varchar2(7),
    sido varchar2(30),
    gugun varchar2(30),
    dong varchar2(100),
    zip_code varchar2(30),
    bunji varchar2(30)
)



-- drop table orders
-- desc order

drop table orders

create table orders(
    order_id number(10) primary key,
    mem_id  varchar2(20) not null,
    status varchar(1) default '0',
    sub_total number(15) default 0,
    tax  number(5) default 0,        
    total number(15) default 0,
    discount  number(5) default 0,
    grand_total number(15) default 0,
    name varchar2(40),
    phone varchar2(13),                            
    email varchar2(40),                             
    zip_num varchar2(7),                          
    address1  varchar2(50),                      
    address2  varchar2(50),                      
    content varchar(100),               
    regdate date default sysdate,            
    moddate date default sysdate            
)
create sequence seq_order_id increment by 1 start with 1;
--alter table orders modify tax number(5) default 0;
--alter table orders rename column mem_code to mem_id
--alter table orders modify mem_id varchar2(20) 
desc orders
21	25
22	31
select * from member  ---------------------------------
select * from product   ---------------------------------
select * from orders
21	25	0	265000
22	31	0	265000

insert into orders(order_id, mem_id, sub_total, total, grand_total, name) values(seq_order_id.nextval, 25, 265000, 265000, 265000, '매직오');
insert into orders(order_id, mem_id, sub_total, total, grand_total, name) values(seq_order_id.nextval, 31, 265000, 265000, 265000, '과일님');

 drop table order_item

create table order_item(
    order_item_id number(10) primary key, 
    order_id number(10)  references orders(order_id),
    code  number(5) references product(code),
    name varchar2(20),
    quantity number(5),
    unit_price number(10),
    status varchar(1) default '0',
    regdate date default sysdate
);    
create sequence seq_order_item_id increment by 1 start with 1 ;
--alter table order_item modify status varchar2(1) default '0'

insert into order_item(order_item_id, order_id, code, quantity, unit_price) values(seq_order_item_id.nextval, 41, 1, 10, 26500);
insert into order_item(order_item_id, order_id, code, quantity, unit_price) values(seq_order_item_id.nextval, 41, 5, 10, 23500);
insert into order_item(order_item_id, order_id, code, quantity, unit_price) values(seq_order_item_id.nextval, 41, 8, 10, 22500);
insert into order_item(order_item_id, order_id, code, quantity, unit_price) values(seq_order_item_id.nextval, 42, 11, 5, 23200);
insert into order_item(order_item_id, order_id, code, quantity, unit_price) values(seq_order_item_id.nextval, 42, 13, 5, 66000);

commit
select * from order_item

-- // Order & Order Item View  //----------------------------------------------------------------------------------------
create or replace view order_item_view
as
select i.order_item_id as order_item_id, o.order_id as order_id, i.status status, o.mem_id, o.name as member_name, 
i.code as product_code, p.name as product_name, i.quantity, i.unit_price, o.zip_num, o.address1||' '||o.address2 as address, 
o.phone, to_char(o.regdate, 'yyyy-MM-dd') as regdate
from orders o inner join order_item i
on o.order_id = i.order_id
inner join product p
on i.code = p.code


commit

select * from order_item_view
--where member_name like '%'||'과일님'||'%'
order by status asc, order_id desc, order_item_id asc

select * from order_item_view
where mem_code=31 
and status='0' and order_id=22
order by status asc, order_id desc, order_item_id asc

drop view order_item_view


commit


-- // Cart & Cart Item //------카트는 한 개의 테이블로 가는 게 맞다.------------------------------------------

-- // 1 Table Cart // -----------------------------------------
-- Drop table cart
Create Table cart(
    cart_id number(10),
    mem_id varchar2(20) references member(mem_id),
    product_code  number(5) references product(code),
    name varchar2(20),
    unit_price number(5),
    quantity number(5) default 1,
    status char(1) default '0',
    regdate date default sysdate,
    primary key(cart_id)
) 
Create sequence seq_cart_id increment by 1 start with 1
    
commit


--// 2 Table Cart //------------------------------------------------------
select * from cart
Drop Table cart

create table cart(
    cart_id number(10) primary key,
    mem_id varchar2(20) not null,
    name varchar2(40),
    phone varchar2(13),                            
    email varchar2(40),                             
    zip_num varchar2(7),                          
    address1  varchar2(50),                      
    address2  varchar2(50),                      
    content varchar(100),               
    regdate date default sysdate,            
    moddate date default sysdate            
)
create sequence seq_cart_id increment by 1 start with 1;

--cart_item
-- Drop table cart_item

create table cart_item(
    cart_item_id number(10) primary key,
    cart_id number(10) references cart(cart_id),
    code number(5) references product(code),
    name varchar2(20),
    quantity number(5),
    unit_price number(5),
    regdate date default sysdate  
)
create sequence seq_cart_item_id increment by 1 start with 1;

commit

-- // Cart & Cart Item View //-----------------------------------------------------------------------------------------------------
create or replace view cart_item_view
as
select i.cart_item_id as cart_item_id, c.cart_id as cart_id, c.mem_id, c.name as member_name, 
i.code as product_code, i.name as product_name, i.quantity, i.unit_price, c.zip_num, c.address1||' '||c.address2 as address, 
c.phone, to_char(c.regdate, 'yyyy-MM-dd') as regdate
from cart c inner join cart_item i
on c.cart_id = i.cart_id

desc cart_item_view




-- Drop table qna_board
create table qna_board (
  no number(5) primary key,           -- 글번호 
  subject varchar2(300) not null,       -- 제목
  content varchar2(2000) ,                -- 문의내용
  member_id varchar2(20) references member(id) , -- 주문자 아이디(FK : member.id) 
  member_name varchar2(40),             -- 작성자명 
  files varchar2(50) ,                            -- 파일 첨부
  regdate date default  sysdate,            -- 작성일
  moddate date default sysdate,             --수정일
  hit number(5) default 0 ,                -- 조회수
  groups number(3) default 0 ,          -- 게시물을 묶어주는 group
  parents number(3) default 0,           -- 부모글 여부
  steps number(3) default 0 ,             -- 원글로 부터 깊이
  indents number(3) default 0,            -- 들여쓰기용
  delyn char(1) default 'n'
);  
commit
create sequence qna_board_no_seq increment by 1 start with 1;
-- Alter Table qna_board add moddate date default sysdate;
select * from qna_board order by no desc, steps asc

Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '나와 결혼해주실래요?', '나와 결혼해주실래요? 내용입니다.', 'marry');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '대한민국 나의 사랑하는 조국', '나와 결혼해주실래요? 내용입니다.', 'would');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '앞만 보고 열심히 나가자', '나와 결혼해주실래요? 내용입니다.', 'going');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '모든 일에는 때가 있다는 말씀', '나와 결혼해주실래요? 내용입니다.', 'gogogo');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '미래에 닥쳐올 일을 미리 준비하면 고난이 적다', '나와 결혼해주실래요? 내용입니다.', 'duriahn');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '한 번 신뢰를 저버린 사람은 또 그럴 수 있다.', '나와 결혼해주실래요? 내용입니다.', 'forbos');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '인생은 자신과의 싸움이란걸 늦은 나이에 깨닫는게 사람이다.', '나와 결혼해주실래요? 내용입니다.', 'couldyou');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '나이가 들수록 투자는 보수적으로 해야 한다.', '나와 결혼해주실래요? 내용입니다.', 'billhalf');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '한 방면에 미치면 원하는 곳에 미치게 된다.', '나와 결혼해주실래요? 내용입니다.', 'trueworld');
Insert Into qna_board(no, subject, content, member_id) Values(qna_board_no_seq.nextval, '말 보다는 실력으로 무장하고 있어야 살아남는다', '나와 결혼해주실래요? 내용입니다.', 'badasori');

select * from qna_board
commit

SELECT CONSTRAINT_NAME
      ,CONSTRAINT_TYPE
      ,TABLE_NAME
      ,R_CONSTRAINT_NAME -- 이놈이 부모 테이블이다.
  FROM USER_CONSTRAINTS
 WHERE CONSTRAINT_NAME = 'SYS_C0020851'
 
SELECT TABLE_NAME
      ,CONSTRAINT_NAME
      ,STATUS
  FROM USER_CONSTRAINTS
 WHERE CONSTRAINT_NAME = 'SYS_C0020614'




-- Create View --------------------------------------------------
create or replace view cart_view
as
select o.cseq, o.id, o.pseq, m.name mname, p.name pname, 
o.quantity, o.indate, p.price2, o.result 
from cart o, member m, product p 
where o.id = m.id and o.pseq = p.pseq
and result='1'; -- 미처리

create or replace view order_view
as
select d.odseq, o.oseq, o.id, o.indate, d.pseq,d.quantity, m.name mname,
m.zip_num, m.address, m.phone, p.name pname, p.price2, d.result   
from orders o, order_detail d, member m, product p 
where o.oseq=d.oseq and o.id = m.id and d.pseq = p.pseq;
           
-- 베스트 상품
create or replace view best_pro_view
as
select pseq, name, price2, image 
from( select rownum, pseq, name, price2, image 
      from product  
      where bestyn='y' 
      order by indate desc)
where  rownum <=4;

-- 신상품
create or replace view new_pro_view
as
select pseq, name, price2, image 
from( select rownum, pseq, name, price2, image 
      from product  
      where useyn='y' 
      order by indate desc)
where  rownum <=4;

/****/

Select * from product
desc product;
commit

select * from product

Select code, name, kind, cost_price, list_price,
						 sales_margin, content, image,useyn, bestyn, regdate 
						 From product 
						 Where code = 41

desc product
