<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="../header.jsp" %>  
<%@ include file="sub_img.html"%> 
<%@ include file="sub_menu.html" %>

<article>
	<h1>QnA 작성하기</h1>
	<!-- [1] 파일을 업로드 하기 위해서는 폼태그를 post 방식으로 전송하고,
	인코딩 타입을 multipart/form-data 로 지정해야 한다. -->
	<form name="frm" method="post" enctype="multipart/form-data">
		<!-- 원글 -->
		<input type="hidden" name="member_id" value="${workerId}" />
		
		<table id="qnaWriteForm">
			<tr>
				<th>제목</th>
				<td>
					<input type="text" name="subject" size="67" maxlength="100" value="">
				</td>
			</tr>
			<tr>
				<th>내용</th>
				<td><textarea name="content" rows="8" cols="70"></textarea>
				<script>
						CKEDITOR.replace('content');
					</script>
				</td>
			</tr>
			<tr>
				<th>첨부파일</th>
				<td>
					<input type="file" name="files">
				</td>
			</tr>
		</table>
		<input class="btn" type="button" value="저장" onClick="go_qnaSave()">
		<input class="btn" type="button" value="목록" onClick="go_mov()">
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>