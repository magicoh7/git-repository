<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<%@ include file="../header.jsp" %>  
<%@ include file="sub_img.html"%> 
<%@ include file="sub_menu.html" %>

<article>
	<h1>QnA 답변하기</h1>
	<!-- [1] 파일을 업로드 하기 위해서는 폼태그를 post 방식으로 전송하고,
	인코딩 타입을 multipart/form-data 로 지정해야 한다. -->
	<form name="frm" method="post" enctype="multipart/form-data">
		<!-- 원글 -->
		<input type="hidden" name="no" value="${qna.no }" />
		<input type="hidden" name="groups" value="${qna.groups }" />
		<input type="hidden" name="steps" value="${qna.steps }" />
		<input type="hidden" name="indents" value="${qna.indents }" />
		<input type="hidden" name="member_id" value="${workerId}" />
		
		<table id="qnaWriteForm">
			<!-- 원글 -->
			<tr>
				<th>원글 제목</th>
				<td>${qna.subject}</td>
			</tr>
			<tr>
				<th>원글 작성자</th>
				<td>${qna.member_id}</td>
			</tr>
			<tr>
				<th>원글 작성일</th>
				<td><fmt:formatDate value="${qna.moddate}" type="date"/></td>
			</tr>
			<tr>
				<th>원글 조회수</th>
				<td>${qna.hit}</td>
			</tr>
			<!-- 답글 작성-->
			<tr>
				<th>답글 제목</th>
				<td>
					<input type="text" name="subject" size="67" maxlength="100" value="[답글]&nbsp;${qna.subject }">
				</td>
			</tr>
			<tr>
				<th>답글 내용</th>
				<td><textarea name="content" rows="8" cols="70"></textarea>
				<script>
						CKEDITOR.replace('content');
					</script>
				</td>
			</tr>
			<tr>
				<th>첨부파일</th>
				<td>
					<input type="file" name="files">
				</td>
			</tr>
		</table>
		<div id="btnDiv" >
			<input class="btn" type="button" value="저장" onClick="go_replySave()">
			<input class="btn" type="button" value="목록" onClick="go_mov()">
		</div>
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>