//상품 상세 페이지에서 [장바구니담기] 클릭시 호출



$(':input').each(function(index){
result += "태그명 : " + this.tagName
+ ", type 속성명 : " + $(this).attr('type') + '\n';
});


function go_cart() {
	if (document.formm.quantity.value == "") {
		alert("수량을 입력하여 주세요.");
		document.formm.quantity.focus();
	} else {
		document.formm.action = "ShoppingServlet?command=cart_insert";
		document.formm.submit();
	}
}

//장바구니 상품 삭제
function delete_cart_item() {
	var count = 0;

	if (document.formm.deleteCart.length == undefined) {
		if (document.formm.deleteCart.checked == true) {
			count++;
		}
	}

	for (var i = 0; i < document.formm.deleteCart.length; i++) {
		//alert("" + document.formm.deleteCart[i].checked);
		if (document.formm.deleteCart[i].checked == true) {
			count++;
			//alert("" + count);
		}
	}
	if (count == 0) {
		alert("삭제할 항목을 선택해 주세요.");
	} else {
		document.formm.action = "ShoppingServlet?command=cart_delete";
		document.formm.submit();
	}
}

//장바구니 리스트에서 주문하기 클릭시 상품 선택 체크박스 검사
function go_order_insert() {
	alert('go_order_insert');
	
	var count = 0;
	if (document.formm.selectedCart.length == undefined) {
		if (document.formm.selectedCart.checked == true) {
			count++;
		}
	}
	for (var i = 0; i < document.formm.selectedCart.length; i++) {
		//alert("" + document.formm.selectedCart[i].checked);
		if (document.formm.selectedCart[i].checked == true) {
			count++;
			//alert("" + count);
		}
	}
	if (count == 0) {
		alert("주문할 항목을 선택해 주세요.");
	} else {
		document.formm.action = "ShoppingServlet?command=order_insert";
		document.formm.submit();
	}
}

function go_order_delete() {
	var count = 0;

	if (document.formm.oseq.length == undefined) {
		if (document.formm.oseq.checked == true) {
			count++;
		}
	}

	for (var i = 0; i < document.formm.oseq.length; i++) {
		if (document.formm.oseq[i].checked == true) {
			count++;
		}
	}
	if (count == 0) {
		alert("삭제할 항목을 선택해 주세요.");

	} else {
		document.formm.action = "ShoppingServlet?command=order_delete";
		document.formm.submit();
	}
}

function go_order() {
	document.formm.action = "ShoppingServlet?command=mypage";
	document.formm.submit();
}