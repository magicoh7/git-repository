// *************** QnA 관련 자바스크립트 *********************/

//qna 검색
function go_qnaSearch() {
	document.frm.action = "ShoppingServlet?command=qna_list";
	document.frm.submit();
}

//qna 전체조회
function go_qnaTotal() {
	var theForm = document.frm;
	theForm.searchText.value = "";
	theForm.action =  "ShoppingServlet?command=qna_list";
	theForm.submit();
}

//QnA 상세 보기
function go_qnaDetail(pageNum, no) {
	
	var theForm = document.frm;
	// QnA 상세 보기 페이지에서 다시 리스트로 돌아왔을 경우 현재 페이지로
	// 돌아올 수 있도록 하기 위해서 현재 페이지 번호를 넘겨준다.
	theForm.action =  "ShoppingServlet?command=qna_detail&pageNum=" +
						pageNum + "&no=" + no;
	theForm.submit();
}

function go_qnaWriteForm(pageNum) {
	
	var theForm = document.frm;
	theForm.action = "ShoppingServlet?command=qna_write_form&pageNum=" + pageNum;
	theForm.submit();
}

//qna 글쓰기 저장시 입력값 체크
function go_qnaSave() 
{
	var theForm = document.frm;
	
	if (theForm.subject.value == '') {
		alert('제목을 입력하세요.');
		theForm.subject.focus();
	} else {
		theForm.encoding = "multipart/form-data";
		theForm.action = "ShoppingServlet?command=qna_write";
		theForm.submit();
	}
}

function go_replyForm(pageNum, no) {
	var theForm = document.frm;
	theForm.action =  "ShoppingServlet?command=qna_reply_form&pageNum=" +
	pageNum + "&no=" + no;
	theForm.submit();
}

//답글 등록 화면에서 저장시 입력값 체크
function go_replySave() 
{
	var theForm = document.frm;
	
	if (theForm.subject.value == '') {
		alert('답글 제목을 입력하세요.');
		theForm.subject.focus();
	} else {
		theForm.encoding = "multipart/form-data";
		theForm.action = "ShoppingServlet?command=qna_reply";
		theForm.submit();
	}
}

//답글 목록으로 이동
function go_qna_list(pageNum) {
	document.frm.action = "ShoppingServlet?command=qna_list&pageNum=" + pageNum ;
	document.frm.submit();
}

//qna 수정 사항 저장시 입력값 체크
function go_qnaUpdateSave() 
{
	var theForm = document.frm;
	
	if (theForm.subject.value == '') {
		alert('제목을 입력하세요.');
		theForm.subject.focus();
	} else {
		theForm.encoding = "multipart/form-data";
		theForm.action = "ShoppingServlet?command=qna_update";
		theForm.submit();
	}
}
