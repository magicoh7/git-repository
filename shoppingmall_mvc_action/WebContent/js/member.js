//회원가입 화면에서 사용
$(document).ready(function(){
	//아이디 중복 체크			
	$("#mem_id").blur(function() {
		var mem_id = $('#mem_id').val();
		$("#reid").val('no');		//유효성검증 초기화
		
		$.ajax({
			url : 'ShoppingServlet?command=id_check',
			type : 'POST',
			data: {mem_id: mem_id},
			success : function(data) {
				console.log(data);		
				if (data == 0) { 			// 0 - 아이디 중복
						$("#id_check").text("사용중인 아이디입니다.");
						$("#id_check").css("color", "red");
						//$("#btnJoin").attr("disabled", true);
						//$('#mem_id').focus();
						return false;
				} else if(data == 1) {		//1 - 중복아니므로 아이디 적합성 검사 
					$('#id_check').text('');
					if(getTextLength(mem_id) < 4){	//문자열 길이 검사
						$('#id_check').text("아이디는 영문 또는 영문 숫자 4~12자리 입력하세요.");
						$('#id_check').css('color', 'red');
						//$("#btnJoin").attr("disabled", true);
						//$('#mem_id').focus();
						return false;
					}

					$('#id_check').text('적합한 아이디입니다.');
					$('#id_check').css('color', 'blue');
					$("#reid").val('yes');	//유효성 검증 완료
				}
			}, 
			error : function() {
				console.log("중복 체크 실패");
			}
		});
	});
	
	//회원가입 비밀번호 + 비밀번호 확인 검사
	$("#pwdConfirm").blur(function() {
		$("#error_pwdConfirm").text("");
		 if($("input[name=pwd]").val() != $("input[name=pwdConfirm]").val()){ 
	         $("#error_pwdConfirm").text("비밀번호가 일치하지 않습니다.");
			 $("#error_pwdConfirm").css("color", "red");
	         return false;
	      }
	});
	
    //회원가입 이메일 유효성 검사
    $("#email").blur(function(){
        var email = $(this).val();
        $("#error_email").text("");		//이메일 에러 메시지 클리어
        $("#reemail").val('no');		//이메일 유효성검증 초기화
        // 값을 입력안한경우는 아예 체크를 하지 않는다.
        if( email == '' || email == 'undefined') 
        	return;
        // 이메일 유효성 검사
        if(! email_check(email) ) {
            $("#error_email").text("잘못된 형식의 이메일 주소입니다.예)dream@naver.com");
			 $("#error_email").css("color", "red");
            //$(this).focus();
            return;
        }
        $("#reemail").val('yes');		//이메일 유효성검증 완료
    });
	
    //회원가입 핸드폰 번호 유효성 검사
    $("#phone").blur(function(){
        var phone = $(this).val();
        $("#error_phone").text("");	//폰번호 에러 메시지 클리어
        $("#rephone").val('no');		//휴대폰번호 유효성검증 초기화
        // 값을 입력안한경우는 아예 체크를 하지 않는다.
        if( phone == '' || phone == 'undefined') 
        	return;
        // 핸드폰 번호 유효성 검사
        if(! isCelluar(phone) ) {
            //alert('휴대폰 번호가 올바르지 않습니다.');
            $("#error_phone").text("잘못된 형식의 전화번호입니다. 예)010-2345-6789");
			 $("#error_phone").css("color", "red");
            //$(this).focus();
            return false;
        }
        $("#rephone").val('yes');		//휴대폰번호 유효성검증 완료
    });

  //[로그인] 로그인 버튼 클릭시 호출			
	$("#btnLogin").click(function() {
		var mem_id = $('#login_id').val();
		var pwd = $('#login_pwd').val();
		
		  if (document.formm.login_id.value == "") {
			    alert("아이디를 확인하세요.");
			    document.formm.login_id.focus();
			    return false;
		  } else if (document.formm.login_pwd.value == "") {
			    alert("비밀번호를 확인하세요.");
			    document.formm.login_pwd.focus();
			    return false;
		  }		
		
		$.ajax({
			url : 'ShoppingServlet?command=login',
			type : 'POST',
			data : $("#login_form").serialize(),
			success : function(data) {
				console.log(data);		
				if (data == -1) { 			// -1 - 아이디 없음
					$('#login_id_error').text('');
					$("#login_id_error").text("존재하지 않는 아이디 입니다.");
					$("#login_id_error").css("color", "red");
					return false;
				} else if(data == 0) {		//0 - 비밀번호 틀림
					$('#login_pwd_error').text('');
					$("#login_pwd_error").text("비밀번호가 틀립니다.");
					$("#login_pwd_error").css("color", "red");
					return false;
				}  else if(data == 1) {		//1 - 아이디 비밀번호 맞음 
					location.href="ShoppingServlet?command=index";
				}
			}, 
			error : function() {
				console.log("데이터베이스 오류, 관리자에게 문의하세요.");
			}
		});
	});
    
    
});	

//텍스트박스 사이즈 조회(회원가입시 입력 문자열 크기 체크)
function getTextLength(str) {
     var len = 0;
     for (var i = 0; i < str.length; i++) {
         if (escape(str.charAt(i)).length == 6) {
             len++;
         }
         len++;
     }
     return len;
 }

// 이메일 체크
function email_check( email ) {
    var regex=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return (email != '' && email != 'undefined' && regex.test(email));

}

// 핸드폰 번호 유효성 검사
function isCelluar(asValue) {
	var regExp = /^01(?:0|1|[6-9])-(?:\d{3}|\d{4})-\d{4}$/;
	return regExp.test(asValue); // 형식에 맞는 경우 true 리턴
}

//회원가입 저장 버튼 클릭시 호출
function go_save() {
	
  if (document.formm.mem_id.value == "" || document.formm.reid.value == "no") {
    alert("아이디를 확인하세요.");
    document.formm.mem_id.focus();
  } else if (document.formm.pwd.value == "") {
    alert("비밀번호를 확인하세요.");
    document.formm.pwd.focus();
  } else if (document.formm.pwdConfirm.value == "") {
	  alert("비밀번호를 확인하세요.");
	  document.formm.pwdConfirm.focus();
  } else if ((document.formm.pwd.value != document.formm.pwdConfirm.value)) {
    alert("비밀번호가 다릅니다.");
    document.formm.pwdConfirm.focus();
  } else if (document.formm.name.value == "") {
    alert("이름을 확인하세요.");
    document.formm.name.focus();
  } else if (document.formm.email.value == ""  || document.formm.reemail.value == "no") {
	  alert("이메일을 확인하세요.");
	  document.formm.email.focus();
  } else if (document.formm.phone.value == ""  || document.formm.rephone.value == "no") {
    alert("연락처를 확인하세요.");
    document.formm.phone.focus();
  } else if (document.formm.zipNum.value == "") {
    alert("우편번호를 확인하세요.");
    document.formm.zipNum.focus();
  } else if (document.formm.addr1.value == "") {
    alert("주소를 확인하세요.");
    document.formm.addr1.focus();
  } else if (document.formm.addr2.value == "") {
    alert("주소를 확인하세요.");
    document.formm.addr2.focus();
  } else {	//모든 조건을 만족하면
		$.ajax({
			url : 'ShoppingServlet?command=join',
			type : 'POST',
			data : $("#join_form").serialize(),
			success : function(data) {
				console.log(data);		
				if (data == 1) { 		// 1 - 회원가입완료
						alert('회원가입이 완료되었습니다.');
						location.href="ShoppingServlet?command=login_form";
						return false;
				} else if(data == 0) {	//0 - 회원가입실패 
					alert('회원가입에 실패했습니다. 다시 시도하세요.');
				}
			}, 
			error : function() {
				console.log("데이터베이스 오류! 관리자에게 문의하세요.");
			}
		});	  
  }
}
/**
 * 회원가입 화면에서 [중복체크] 버튼 클릭시 호출
 * **/
function idcheck() {
  if (document.formm.mem_id.value == "") {
    alert('아이디를 확인하세요.');
    document.formm.mem_id.focus();
    return;
  }
  var url = "ShoppingServlet?command=id_check_form&mem_id=" + document.formm.mem_id.value;
  window.open( url, "_blank_1", "toolbar=no, menubar=no, scrollbars=yes, resizable=no, width=330, height=200");
}

//주소찾기 버튼 클릭시 호출
function post_zip() {
  var url = "ShoppingServlet?command=find_zip_num";
  window.open( url, "_blank_1",
  			"toolbar=no, menubar=no, scrollbars=yes, resizable=no, width=550, height=300, top=300, left=300, ");
}

/**
 * 회원가입 > 약관동의 화면에서 [Next] 버튼 클릭시 호출
 * **/
function go_next() {
  if (document.formm.okon1[0].checked == true) {
    document.formm.action = "ShoppingServlet?command=join_form";
    document.formm.submit();
  } else if (document.formm.okon1[1].checked == true) {
    alert('회원가입 이용 약관에 동의해주세요.');
  }
}