
/*
 ** 상품보기(productDetail.jsp)에서 [장바구니에 담기] 버튼 클릭시 호출 
 */
function go_cart() {
	
  if (document.formm.quantity.value == "") {
    alert("수량을 입력하여 주세요.");
    document.formm.quantity.focus();
  } else {
	  var num = Number(document.formm.quantity.value);
	  alert(num);
	  if(num<0){
		  alert('상품 수량을 확인하세요.');
		  return false;
	  }
    document.formm.action = "ShoppingServlet?command=cart_insert";
    document.formm.submit();
  }
}

function go_cart_delete() {
  var count = 0;
  for ( var i = 0; i < document.formm.cseq.length; i++) {
    if (document.formm.cseq[i].checked == true) {
      count++;
    }
  }
  if (count == 0) {
    alert("삭제할 항목을 선택해 주세요.");

  } else {
    document.formm.action = "ShoppingServlet?command=cart_delete";
    document.formm.submit();
  }
}

function go_order_insert() {
	
	alert('go_order_insert');
	
	//미처리 체크된 갯수 확인
	count = $('input:checkbox[id="status"]:checked').length;
	
  document.formm.action = "ShoppingServlet?command=order_insert";
  document.formm.submit();
}

function go_order_delete() {
  var count = 0;
  for ( var i = 0; i < document.formm.oseq.length; i++) {
    if (document.formm.oseq[i].checked == true) {
      count++;
    }
  }
  if (count == 0) {
    alert("삭제할 항목을 선택해 주세요.");

  } else {
    document.formm.action = "ShoppingServlet?command=order_delete";
    document.formm.submit();
  }
}

function go_order() {
  document.formm.action = "ShoppingServlet?command=mypage";
  document.formm.submit();
}