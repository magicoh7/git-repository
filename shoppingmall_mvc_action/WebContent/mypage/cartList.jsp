<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../header.jsp"%>
<%@ include file="sub_img.html"%>
<%@ include file="sub_menu.jsp"%>


<article>
	<h2>장바구니 목록</h2>
	<form name="formm" method="post">
		<c:choose>
			<c:when test="${cartList.size() == 0}">
				<h3 style="color: red; text-align: center;">장바구니가 비었습니다.</h3>
			</c:when>
			<c:otherwise>
				<table id="cartList">
					<tr>
						<th>선택</th>
						<th>상품명</th>
						<th>수량</th>
						<th>가격</th>
						<th>금액</th>
						<th>주문일</th>
						<th>삭제</th>
					</tr>

					<c:forEach items="${cartList}" var="cartVo">
						<tr>
							<td>
								<input type="checkbox" name="selectedCart"	value="${cartVo.cart_id}">
							</td>
							<td>
								<a href="ShoppingServlet?command=product_detail&code=${cartVo.product_code}">${cartVo.product_name}</a>
							</td>
							<td><input type="number" name="quantity" class="quantity" size="2" min="1" value="${cartVo.quantity}"><br></td>
							<td>
								<fmt:formatNumber value="${cartVo.list_price}" />
							</td>
							<td class="amt">
								<fmt:formatNumber value="${cartVo.list_price * cartVo.quantity}" />
							</td>
							<td>
								<fmt:formatDate value="${cartVo.regdate}" type="date" />
							</td>
							<td>
								<input type="checkbox" name="deleteCart" value="${cartVo.cart_id}">
							</td>
						</tr>
					</c:forEach>

					<tr>
						<th colspan="2">총 액</th>
						<th colspan="2"><fmt:formatNumber value="${totalAmt}" type="currency" /><br></th>
						<th colspan="2"><input type="text" value="${totalAmt }" readonly><br></th>
						<th><a href="#" onclick="delete_cart_item()"><h3>삭제하기</h3></a></th>
					</tr>
				</table>
			</c:otherwise>
		</c:choose>

		<div class="clear"></div>

		<div id="buttons" style="float: right">
			<input type="button" value="쇼핑 계속하기" class="cancel" onclick="location.href='ShoppingServlet?command=index'">
			<c:if test="${cartList.size() != 0}">
				<input type="button" value="주문하기" class="submit" onclick="go_order_insert()">
			</c:if>
		</div>
	</form>
</article>

<%@ include file="../footer.jsp"%>