<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@ include file="../header.jsp"%>
<%@ include file="sub_img.html"%>
<%@ include file="sub_menu.jsp"%>

<article>
	<h2>My Page(${title})</h2>
	<form name="formm" method="post">
		<table id="cartList">
			<tr>
				<th>주문일자</th>
				<th>주문번호</th>
				<th>총결제금액</th>
				<th>상품명</th>
				<th>주문수량</th>
				<th>주문금액</th>
				<th>주문상태</th>
				<th>상세보기</th>
			</tr>
			<c:forEach items="${orderItemList}" var="orderVo">
				<tr>
					<td><fmt:formatDate value="${orderVo.regdate}" type="date" /></td>
					<td>${orderVo.order_id}</td>
					<td><fmt:formatNumber value="${orderVo.grand_total}" type="currency" /></td>
					<td>
						<a href="ShoppingServlet?command=product_detail&code=${orderVo.product_code}">${orderVo.product_name}</a>
					</td>	
					<td><fmt:formatNumber value="${orderVo.quantity}" /></td>
					<td><fmt:formatNumber value="${orderVo.amt_per_product}" type="currency" /></td>
					<td>
						<c:choose>
							<c:when test='${orderVo.status=="0"}'>
								입금대기중
							</c:when>
							<c:when test='${orderVo.status=="1"}'>
								결제완료
							</c:when>
							<c:when test='${orderVo.status=="2"}'>
								배송준비중
							</c:when>
							<c:when test='${orderVo.status=="3"}'>
								배송중
							</c:when>
							<c:when test='${orderVo.status=="4"}'>
								배송완료
							</c:when>
							<c:when test='${orderVo.status=="5"}'>
								반품
							</c:when>
							<c:when test='${orderVo.status=="6"}'>
								교환
							</c:when>
							<c:when test='${orderVo.status=="7"}'>
								환불
							</c:when>
						</c:choose>
					</td>
					<td>
						<a href="ShoppingServlet?command=order_detail&order_id=${orderVo.order_id}">조회</a>
					</td>
				</tr>
			</c:forEach>
		</table>

		<div class="clear"></div>
		<div id="buttons" style="float: right">
			<input type="button" value="쇼핑 계속하기" class="cancel"
				onclick="location.href='ShoppingServlet?command=index'">
		</div>
	</form>
</article>
<%@ include file="../footer.jsp"%>
