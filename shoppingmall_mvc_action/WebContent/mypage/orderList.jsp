<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../header.jsp"%>
<%@ include file="sub_img.html"%>
<%@ include file="sub_menu.jsp"%>
<article>
	<h2>주문 완료</h2>
	<form name="formm" method="post">
		<table id="cartList">
			<tr>
				<th>상품명</th>
				<th>수 량</th>
				<th>상품금액</th>
				<th>주문일</th>
			</tr>
			<c:forEach items="${orderItemList}" var="orderVo">
				<tr>
					<td><a href="ShoppingServlet?command=product_detail&code=${cartVO.product_code}">
							${orderVo.product_name}</a>
					</td>
					<td>${orderVo.quantity}</td>
					<td>
						<fmt:formatNumber value="${orderVo.unit_price * orderVo.quantity}" type="currency" />
					</td>
					<td>
						<fmt:formatDate value="${orderVo.regdate}" type="date" />
					</td>
				</tr>
			</c:forEach>
			<tr>
				<th colspan="2">주문 총금액</th>
				<th colspan="2"><fmt:formatNumber value="${totalAmt}" type="currency" /><br></th>
			</tr>
			<tr>
				<th colspan="4">주문 처리가 완료되었습니다.</th>
			</tr>
		</table>

		<div class="clear"></div>
		<div id="buttons" >
			<input type="button" value="쇼핑 계속하기" class="cancel" onclick="location.href='ShoppingServlet?command=index'">
		</div>
	</form>
</article>
<%@ include file="../footer.jsp"%>