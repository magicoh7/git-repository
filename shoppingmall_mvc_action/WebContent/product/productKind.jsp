<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>  

<%@ include file="../header.jsp" %>
<!-- 카테고리별 서브 화면의 중앙 이미지(모든 서브 화면에 공통) -->  
<%@ include file="sub_img.html"%>
<!-- 카테고리별 서브 화면의 좌측 메뉴(top menu를 아래로 배치, 모든 서브 화면에 공통) --> 
<%@ include file="sub_menu.html" %>       
  <article>
    <h2> Item</h2>     
    <c:forEach items="${productKindList }"  var="productVo">
      <div id="item">
        <a href="ShoppingServlet?command=product_detail&code=${productVo.code}"> 
          <img src="product_images/${productVo.image}" />
          <h3>${productVo.name} </h3>        
          <p>${productVo.list_price} </p>
        </a>  
      </div>
    </c:forEach>    
    <div class="clear"></div>
  </article>
<%@ include file="../footer.jsp" %>    