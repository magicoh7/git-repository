<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
  
<%@ include file="../header.jsp" %>  
<%@ include file="sub_img.html"%> 
<%@ include file="sub_menu.html" %>
       
  <article>
    <h1> Item </h1>
    <div id="itemdetail" >
		<form  method="post" name="formm">    
			<fieldset>
				<h2> Item detail Info</h2>  
				<a href="ShoppingServlet?command=product_detail&code=${productVo.code}">         
					<span style="float: left;">
						<img  src="product_images/${productVo.image}"  />
					</span>
					<h2> ${productVo.name} </h2>  
				</a>
				<label> 가 격 :  </label>  
				<p>${productVo.list_price} 원</p>
				<label> 수 량 : </label>
				<input type="number" name="quantity" class="quantity" size="2" value="1"><br>
				
				<!-- input  type="hidden" name="list_price" value="${productVo.list_price}"><br-->
				<input  type="hidden" name="product_code" value="${productVo.code}"><br>
			</fieldset>
			<div class="clear"></div>
			<div id="buttons">
				<input type="button" value="장바구니에 담기" class="submit" onclick="go_cart()"> 
				<input type="button" value="즉시 구매" class="submit" onclick="go_order()"> 
				<input type="reset" value="취소" class="cancel">
			</div>
		</form>  
    </div>
  </article>
<%@ include file="../footer.jsp" %>    