//천원 단위 콤머 찍어주기(예시 1,000,000)
function NumFormat(t) 
{
	s = t.value;
	s = s.replace(/\D/g, '');
	l = s.length - 3;
	while (l > 0) {	//3자리 이상되면
		s = s.substr(0, l) + ',' + s.substr(l);
		l -= 3;
	}
	t.value = s;
	return t;
}

//상품 수정 화면 오픈과 동시에 판매가-원가 = 순마진 자동계산 
function go_ab() 
{
	var theForm = document.frm;
	var a = theForm.list_price.value.replace(/,/g, '');
	var b = theForm.cost_price.value.replace(/,/g, '');
	var ab = parseInt(a) - parseInt(b);
	theForm.sales_margin.value = ab;
}

// 상품 등록 화면에서 저장시 입력값 체크
function go_save() 
{
	var theForm = document.frm;
	
	if (theForm.kind.value == '') {
		alert('상품분류를 선택하세요.');
		theForm.kind.focus();
	} else if (theForm.name.value == '') {
		alert('상품명을 입력하세요.');
		theForm.name.focus();
	} else if (theForm.cost_price.value == '') {
		alert('원가를 입력하세요.');
		theForm.cost_price.focus();
	} else if (theForm.list_price.value == '') {
		alert('판매가를 입력하세요.');
		theForm.list_price.focus();
	} 
//	else if (theForm.content.value == '') {
//		alert('상품상세를 입력하세요.');
//		theForm.content.focus();
//	} 
	else if (theForm.image.value == '') {
		alert('상품이미지를 등록하세요.');
		theForm.image.focus();
	} else {
		theForm.encoding = "multipart/form-data";
		//DB 저장 위해 숫자에서 콤머 제거
		theForm.cost_price.value = removeComma(theForm.cost_price);
		theForm.list_price.value = removeComma(theForm.list_price);
		theForm.sales_margin.value = removeComma(theForm.sales_margin);

		theForm.action = "ShoppingServlet?command=admin_product_write";
		theForm.submit();
	}
}

//콤머(,) 제거
function removeComma(input) 
{
	return input.value.replace(/,/gi, "");
}

// productWrite.jsp에서 사용한다. 상품 리스트로 이동한다.
function go_mov()
{
	var theForm = document.frm;
	theForm.action = "ShoppingServlet?command=admin_product_list";
	theForm.submit();
}

//상품 검색 - projectList.jsp
function go_search() {
	var theForm = document.frm;
	theForm.action =  "ShoppingServlet?command=admin_product_list";
	theForm.submit();
}

//상품 전체 검색
function go_total() {
	var theForm = document.frm;
	theForm.searchText.value = "";
	theForm.action =  "ShoppingServlet?command=admin_product_list";
	theForm.submit();
}

//상품 상세 보기
function go_detail(pageNum, code) {
	var theForm = document.frm;
	// 상품 상세 보기 페이지에서 다시 상품 리스트로 돌아왔을 경우 현재 페이지로
	// 돌아올 수 있도록 하기 위해서 현재 페이지 번호를 쿼리 스트링으로 넘겨준다.
	theForm.action =  "ShoppingServlet?command=admin_product_detail&pageNum=" +
						pageNum + "&code=" + code;
	
	theForm.submit();
}


//상품등록 페이지로 이동
function go_wrt() {
	var theForm = document.frm;
	theForm.action = "ShoppingServlet?command=admin_product_write_form";
	theForm.submit();
}

// 특정 상품목록 페이지로 이동
function go_list(pageNum) {
	var theForm = document.frm;
	//상품 리스트로 이동하되 현재 페이지를 쿼리 스트링으로 넘긴다.
	theForm.action = "ShoppingServlet?command=admin_product_list&tpage=" + pageNum;
	theForm.submit();
}

// 상품 수정 화면으로 이동
function go_modify(pageNum, code) {
	var theForm = document.frm;
	//현재 페이지를 쿼리 스트링으로 넘긴다.
	theForm.action = "ShoppingServlet?command=admin_product_update_form&pageNum=" + pageNum + "&code=" + code;
	theForm.submit();
}

//수정사항 저장시 Validation
function go_modify_save(pageNum, code) {
	var theForm = document.frm;

	if (theForm.kind.value == '') {
		alert('상품분류를 선택하세요');
		theForm.kind.focus();
	} else if (theForm.name.value == '') {
		alert('상품명을 입력하세요');
		theForm.name.focus();
	} else if (theForm.cost_price.value == '') {
		alert('원가를 입력하세요');
		theForm.cost_price.focus();
	} else if (theForm.list_price.value == '') {
		alert('판매가를 입력하세요');
		theForm.list_price.focus();
	} 
//	else if (theForm.content.value == '') {
//		alert('상품 상세 정보를 입력하세요');
//		theForm.content.focus();
//	} 
	else {
		if (confirm('수정하시겠습니까?')) {
			// [1] 상품을 삭제하지 않는 대신 사용하지 않음을 products 테이블의 useyn 컬럼에 1을 채워 넣기 위해서
			// useyn hidden 태그에 1을 지정한다.
			if (theForm.useyn.checked == true) {
				theForm.useyn.value = "y";
			}
			if(theForm.bestyn.checked == true) {
				theForm.bestyn.value = "y";
			}
			theForm.encoding = "multipart/form-data";
			// theForm.seq.value=seq;
			theForm.cost_price.value = removeComma(theForm.cost_price);
			theForm.list_price.value = removeComma(theForm.list_price);
			theForm.sales_margin.value = removeComma(theForm.sales_margin);
			// [2] products 테이블의 상품 정보를 수정하는 처리를 하는 product_modsave.jsp 페이지로
			// 이동하되 상품 코드를 전달해준다. 상품코드로 폴더를 생성해서 그곳에 이미지 파일을 업로드하기 때문이다.			
			theForm.action = "ShoppingServlet?command=admin_product_update&pageNum=" + pageNum;
			theForm.submit();
		}
	}
}

function go_mod_mov(tpage, pseq) {
	var theForm = document.frm;
	theForm.action = 'ShoppingServlet?command=admin_product_detail&tpage=' + tpage + "&pseq=" + pseq;
	theForm.submit();
}
