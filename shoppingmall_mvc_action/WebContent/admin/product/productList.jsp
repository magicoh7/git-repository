<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>


	<article>
		<h1>상품리스트</h1>
		<form name="frm" method="post">
			<table>
				<tr>
					<td width="642">상품명 <input type="text" name="searchText"> 
						<input class="btn" type="button" name="btn_search" value="검색" onClick="go_search()"> 
						<input class="btn" type="button" name="btn_total" value="전체보기 " onClick="go_total()"> 
						<input class="btn" type="button" name="btn_write" value="상품등록"  onClick="go_wrt()">
					</td>
				</tr>
			</table>
			<table id="productList">
				<tr>
					<th>번호</th>
					<th>상품명</th>
					<th>원가</th>
					<th>판매가</th>
					<th>등록일</th>
					<th>사용유무</th>
				</tr>
				<c:choose>
					<c:when test="${products.size() <= 0}">
						<tr>
							<td colspan="6" align="center" height="23">조회 결과가 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:set var="num" value="${totalRecord - ((pageNum - 1) * listCount) }" ></c:set>
						<c:forEach items="${products}" var="product">
							<tr>
								<td align="center"><c:out value="${num}" /></td>
								<td style="text-align: left; padding-left: 10px; padding-right: 0px;">
									<a href="#" onClick="go_detail('${pageNum}', '${product.code}')">
										${product.name}
									</a>
								</td>
								<td><fmt:formatNumber value="${product.cost_price}" /></td>
								<td><fmt:formatNumber value="${product.list_price}" /></td>
								<td><fmt:formatDate value="${product.regdate}" /></td>
								<td>
									<c:choose>
										<c:when test='${product.useyn=="0"}'>미사용</c:when>
										<c:otherwise>사용</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<c:set var="num" value="${num-1 }"></c:set>
						</c:forEach>
						<tr>
							<td colspan="6" style="text-align: center;">${pageNavigator}</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</table>
		</form>
	</article>

<%@ include file="/admin/footer.jsp"%>
</body>
</html>