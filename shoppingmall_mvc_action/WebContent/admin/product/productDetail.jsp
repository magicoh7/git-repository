<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>

<article>
	<h1>상품 상세 보기</h1>
	<form name="frm" method="post">
		<table id="productDetail">
			<tr>
				<th>상품분류</th>
				<td colspan="5">${kind}</td>
			</tr>
			<tr>
				<th align="center">상품명</th>
				<td colspan="5">${product.name}</td>
			</tr>

			<tr>
				<th>원가 [A]</th>
				<td width="60"><fmt:formatNumber value="${product.cost_price}" /></td>
				<th>판매가 [B]</th>
				<td width="60"><fmt:formatNumber value="${product.list_price}" /></td>
				<th>판매마진[B-A]</th>
				<td><fmt:formatNumber value="${product.sales_margin}" /></td>
			</tr>

			<tr>
				<th>상세설명</th>
				<td colspan="5">${product.content}</td>
			</tr>

			<tr>
				<th>상품이미지</th>
				<td colspan="5" align="center">
					<img src="product_images/${product.image}" width="200pt" >
				</td>
			</tr>
		</table>
		<div id="btnDiv" >
			<!--수정 버튼이 눌리면 상품 수정 페이지로 이동하되 현재 페이지 번호와 상품 코드 전달 -->
			<input class="btn" type="button" value="수정" onClick="go_modify('${pageNum}','${product.code}')">
			<input class="btn" type="button" value="목록" onClick="go_list('${pageNum}')">
		</div>
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>