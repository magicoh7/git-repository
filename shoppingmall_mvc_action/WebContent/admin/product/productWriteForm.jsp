<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>

<article>
	<h1>상품등록</h1>
	<!-- [1] 파일을 업로드 하기 위해서는 폼태그를 post 방식으로 전송하고,
	인코딩 타입을 multipart/form-data 로 지정해야 한다. -->
	<form name="frm" method="post" enctype="multipart/form-data">
		<table id="productUpdate">
			<tr>
				<th>상품분류</th>
				<td colspan="5"><select name="kind">
						<c:forEach items="${kindList}" var="kind" varStatus="status">
							<option value="${status.count}">${kind}</option>
						</c:forEach>
				</select>
			</td>	
			<tr>
				<th>상품명</th>
				<td width="343" colspan="5">
					<input type="text" name="name" size="47" maxlength="100" value="킬힐">
				</td>
			</tr>
			<tr>
				<th>원가[A]</th>
				<td width="70">
					<input type="text" name="list_price" size="11" onBlur="go_ab()" onKeyUp='NumFormat(this)' value="0" >
				</td>
				<th>판매가[B]</th>
				<td width="70">
					<input type="text" name="cost_price" size="11" onBlur="go_ab()" onKeyUp='NumFormat(this)' value="0" >
				</td>
				<th>판매마진[B-A]</th>
				<td width="72">
					<input type="text" name="sales_margin" size="11"  onKeyUp='NumFormat(this)' readonly >
				</td>
			</tr>

			<tr>
				<th>상세설명</th>
				<td colspan="5">
					<textarea name="content" id="content" rows="8" cols="70"></textarea>
					<script>
						CKEDITOR.replace('content');
					</script>
				</td>
			</tr>
			<tr>
				<th>상품이미지</th>
				<td width="343" colspan="5">
					<input type="file" name="image">
				</td>
			</tr>
		</table>
		
		<input class="btn" type="button" value="등록" onClick="go_save()">
		<input class="btn" type="button" value="취소" onClick="go_mov()">
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>