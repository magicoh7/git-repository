<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>

<article>
	<h1>상품수정</h1>
	<form name="frm" method="post" enctype="multipart/form-data">
		<input type="hidden" name="code" value="${product.code}"> 
		<input type="hidden" name="pageNum" value="${pageNum }" > 
		<input type="hidden" name="nonMakeImg" value="${product.image}">
		
		<table id="productUpdate">
			<tr>
				<th>상품분류</th>
				<td colspan="5"><select name="kind">
						<c:forEach items="${kinds}" var="kind" varStatus="status">
							<c:choose>
								<c:when test="${product.kind == status.count}">
									<option value="${status.count}" selected="selected">${kind}</option>
								</c:when>
								<c:otherwise>
									<option value="${status.count}">${kind}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<th>상품명</th>
				<td width="343" colspan="5">
					<input type="text" name="name" size="47" maxlength="100" value="${product.name}">
				</td>
			</tr>
			<tr>
				<th>원가[A]</th>
				<td width="70">
					<input type="text" name="cost_price" size="11" onBlur="go_ab()" onKeyUp='NumFormat(this)' value="${product.cost_price}">
				</td>
				<th>판매가[B]</th>
				<td width="70">
					<input type="text" name="list_price" size="11" onBlur="go_ab()" onKeyUp='NumFormat(this)' value="${product.list_price}">
				</td>
				<th>판매마진[B-A]</th>
				<td width="72">
					<input type="text" name="sales_margin" size="11" readonly onKeyUp='NumFormat(this)'>
				</td>
			</tr>
			<tr>
				<th>베스트상품</th>
				<td><c:choose>
						<c:when test='${product.bestyn=="y"}'>
							<input type="checkbox" name="bestyn" value="y" checked="checked">
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="bestyn" value="n">
						</c:otherwise>
					</c:choose></td>
				<th>사용유무</th>
				<td><c:choose>
						<c:when test='${product.useyn=="y"}'>
							<input type="checkbox" name="useyn" value="y" checked="checked">
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="useyn" value="n">
						</c:otherwise>
					</c:choose>
				</td>
				<th></th>
				<td width="72">
					
				</td>
				
			</tr>
			<tr>
				<th>상세설명</th>
				<td colspan="5">
					<textarea name="content" id="content" rows="8" cols="70">${product.content}
					</textarea>
					<script>
						CKEDITOR.replace('content');
					</script>
				</td>
			</tr>
			<tr>
				<th>상품이미지</th>
				<td colspan="5">
					<img src="product_images/${product.image}" width="200pt" /> <br> 
					<input type="file" name="image">
				</td>
			</tr>
		</table>
		<div id="btnDiv" >
			<input class="btn" type="button" value="저장" onClick="go_modify_save('${pageNum}','${product.code}')"> 
			<input class="btn" type="button" value="취소" onClick="go_mov()">
		</div>
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>