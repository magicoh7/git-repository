<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>쇼핑몰 관리자 화면</title>
	<link rel="stylesheet" href="admin/css/admin.css">
	
	
	<script type="text/javascript">
		function admin_check() {
			if (document.frm.id.value == "") {
				alert("아이디를 입력하세요.");
				document.frm.id.focus();
				return false;
			} else if (document.frm.pwd.value == "") {
				alert("비밀번호를 입력하세요.");
				document.frm.pwd.focus();
				return false;
			}
			return true;
		}
	</script>
</head>

<body>
	<div id="wrap" align="center">
		<header>
			<div id="logo">
				<a href="admin/adminLoginForm.jsp">
					<img src="admin/images/bar_01.gif" style="float: left"><img src="admin/images/text.gif">
				</a>
			</div>
		</header>
		<div class="clear"></div>
		<article>
			<div id="loginform">
				<form name="frm" method="post" action="ShoppingServlet?command=admin_login">
					<table>
						<tr>
							<td>아 이 디</td>
							<td><input type="text" name="id" size="10" value="admin" required></td>
						</tr>
						<tr>
							<td>비밀번호</td>
							<td><input type="password" name="pwd" size="10" value="1234"  required></td>
						</tr>
						<tr align="center">
							<td colspan="2"><input class="btn" type="submit" value="관리자로그인" onclick="return admin_check()"><br>
							<br>
								<h4 style="color: red">${msg}</h4></td>
						</tr>
					</table>
				</form>
			</div>
		</article>
	</div>
</body>
</html>