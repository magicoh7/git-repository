<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>

<script type="text/javascript">

	//주문처리(입금확인)
	function go_order_save() {
		var count = 0;
		
		//미처리 체크된 갯수 확인 
		count = $('input:checkbox[id="status"]:checked').length;
		
		if (count == 0) {
			alert("주문처리할 항목을 선택해 주세요.");
		} else {
			document.frm.action = "ShoppingServlet?command=admin_order_save";
			document.frm.submit();
		}
	}
	
	//주문자명으로 검색
	function go_search() {
		var theForm = document.frm;
		theForm.action =  "ShoppingServlet?command=admin_order_list";
		theForm.submit();
	}

	
</script>

<article>
	<h1>주문리스트</h1>
	<form name="frm" method="post">
		<table style="float: right;">
			<tr>
				<td>
					주문자명 : <input type="text" name="searchText"> 
					<input class="btnSearch" type="button" value="검색" onclick="go_search()">
				</td>
			</tr>
		</table>
		<br>
		<table id="orderList">
			<tr>
				<th>번호</th>
				<th>처리여부</th>
				<th>주문자</th>
				<th>상품명</th>
				<th>수량</th>
				<th>단가</th>
				<th>우편번호</th>
				<th>배송지</th>
				<th>전화</th>
				<th>주문일</th>
			</tr>
			<c:forEach items="${orderItemList}" var="orderItemVO">
				<tr>
					<c:choose>
							<c:when test='${orderItemVO.status=="0"}'>
								<td>
									<span style="font-weight: bold; color: blue">${orderItemVO.order_item_id}</span>
								</td>
								<td>
					        		(<input type="checkbox" id="status" name="status" value="${orderItemVO.order_item_id}"> 입금대기)
					        	</td>
					        </c:when>
							<c:when test='${orderItemVO.status=="1"}'>
								<td>
									<span style="font-weight: bold; color: red">${orderItemVO.order_item_id}</span>
								</td>
								<td>
					        		(<input type="checkbox" checked="checked" disabled="disabled">결제완료)
					        	</td>
					        </c:when>
							<c:otherwise>
								<td>
									<span style="font-weight: bold; color: red">${orderItemVO.order_item_id}</span>
								</td>
								<td>	
						        	(<input type="checkbox" checked="checked" disabled="disabled">배송대기)
						        </td>	
						    </c:otherwise>
						</c:choose>
						
					<td>${orderItemVO.member_name}</td>
					<td>${orderItemVO.product_name}</td>
					<td>${orderItemVO.quantity}</td>
					<td><fmt:formatNumber value="${orderItemVO.unit_price }" pattern="#,###"/>
					<td>${orderItemVO.zip_num}</td>
					
					<td>
				        <c:choose>
							<c:when test="${fn:length(orderItemVO.address) > 10}">
								<c:out value="${fn:substring(orderItemVO.address, 0, 10)}"/>....
				   			</c:when>
							<c:otherwise>
				            	<c:out value="${orderItemVO.address}"/>
				        	</c:otherwise> 
						</c:choose>
					</td>
					
					<td>${orderItemVO.phone}</td>
					<td><fmt:formatDate value="${orderItemVO.regdate}" /></td>
				</tr>
			</c:forEach>
		</table>
		<input type="button" class="btn" style="width: 200px"
			value="주문처리(입금확인)" onClick="go_order_save()">
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>