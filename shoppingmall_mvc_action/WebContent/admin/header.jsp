<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>쇼핑몰 관리자 화면</title>
	<!-- link rel="stylesheet" href="admin/css/shopping.css" -->
	<link rel="stylesheet" href="admin/css/admin.css">
	<script src="jquery-1.6.2.js" type="text/javascript"></script>
	<script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
	<script type="text/javascript" src="admin/product/product.js"></script>
	<script type="text/javascript" src="admin/qna/qna.js"></script>
	
	<!-- 관리자 권한체크 -->
	<c:choose>
		<c:when test="${empty sessionScope.worker.id}">
			<script type="text/javascript">
				location.href = 'ShoppingServlet?command=admin_login_form';
			</script>
		</c:when>
	</c:choose>
</head>
<body onload="go_ab()">
	<div id="wrap">
		<header>
			<div id="logo">
				<a href="ShoppingServlet?command=admin_login_form"> 
					<img style="width: 800px" src="admin/images/bar_01.gif"> 
					<img src="admin/images/text.gif">
				</a>
			</div>
			<input class="btn" type="button" value="logout" style="float: center;"
				onClick="location.href='ShoppingServlet?command=admin_logout'">
		</header>
		<div class="clear"></div>
