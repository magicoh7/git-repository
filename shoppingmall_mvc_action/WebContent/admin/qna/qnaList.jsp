<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>


	<article>
		<h1>QnA 리스트</h1>
		<form name="frm" method="post">
			<table>
				<tr>
					<td width="642">제목명 <input type="text" name="searchText"> 
						<input class="btn" type="button" name="btn_search" value="검색" onClick="go_qnaSearch()"> 
						<input class="btn" type="button" name="btn_total" value="전체보기 " onClick="go_qnaTotal()"> 
						<input class="btn" type="button" name="btn_write" value="QnA등록"  onClick="go_qnaWriteForm('${pageNum}')">
					</td>
				</tr>
			</table>
			<table id="productList">
				<tr>
					<th>번호</th>
					<th>제목</th>
					<th>작성자</th>
					<th>조회수</th>
					<th>등록일</th>
					<th>사용유무</th>
					<th>groups</th>
					<th>steps</th>
					<th>indents</th>
				</tr>
				<c:choose>
					<c:when test="${qnaBoards.size() <= 0}">
						<tr>
							<td colspan="9" align="center" height="23">조회 결과가 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:set var="num" value="${totalRecord - ((pageNum - 1) * listCount) }" ></c:set>
						<c:forEach items="${qnaBoards}" var="qna">
							<tr>
								<td align="center"><c:out value="${num}" /></td>
								<td style="text-align: left; padding-left: 10px; padding-right: 0px;">
									<c:forEach begin="1" end="${qna.indents}">&nbsp;</c:forEach>
									<a href="#" onClick="go_qnaDetail('${pageNum}', '${qna.no}')">
										${qna.subject}
									</a>
								</td>
								<td>${qna.member_id }</td>
								<td>${qna.hit }</td>
								<td><fmt:formatDate value="${qna.moddate}" /></td>
								<td>
									<c:choose>
										<c:when test='${qna.delyn=="0"}'>미사용</c:when>
										<c:otherwise>사용</c:otherwise>
									</c:choose>
								</td>
								<td>${qna.groups }</td>
								<td>${qna.steps }</td>
								<td>${qna.indents }</td>
							</tr>
							<c:set var="num" value="${num-1 }"></c:set>
						</c:forEach>
						<tr>
							<td colspan="9" style="text-align: center;">${pageNavigator}</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</table>
		</form>
	</article>

<%@ include file="/admin/footer.jsp"%>
</body>
</html>