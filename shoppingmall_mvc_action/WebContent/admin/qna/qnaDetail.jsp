<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>

	<script type="text/javascript">
		//qna 수정 화면으로 이동(qna.js파일로 이동하면 오류 발생???)
		function go_modifyReply(pageNum, no) {
			var theForm = document.frm;
			theForm.action = "ShoppingServlet?command=admin_qna_update_form&pageNum=" + pageNum + "&no=" + no;
			theForm.submit();
		}
	</script>

<article>
	<h1>QnA 게시물 내용 보기3</h1>
	<form name="frm" method="post">
		<table id="qnaDetail">
			<tr>
				<th align="center">번호</th>
				<td>${qna.no}</td>
			</tr>
			<tr>
				<th align="center">제목</th>
				<td>${qna.subject}</td>
			</tr>
			<tr>
				<th align="center">내용</th>
				<td>${qna.content}</td>
			</tr>
			<tr>
				<th align="center">작성자ID</th>
				<td>${qna.member_id}</td>
			</tr>
			<tr>
				<th align="center">첨부파일</th>
				<td>${qna.files}</td>
			</tr>
			<tr>
				<th align="center">조회수</th>
				<td>${qna.hit}</td>
			</tr>
			<tr>
				<th>삭제여부</th>
				<td>
					<c:choose>
						<c:when test='${qna.delyn=="y"}'>
							삭제
						</c:when>
						<c:otherwise>
							미삭제
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
		<div id="btnDiv" >
			<input class="btn" type="button" value="답변하기" onClick="go_replyForm('${pageNum}','${qna.no}')">
			<!-- 본인 게시물인 경우 수정/삭제 버튼 활성화 -->
			<c:if test="${sessionScope.worker.id == qna.member_id}">
					<input class="btn" type="button" value="수정하기" onClick="go_modifyReply('${pageNum}','${qna.no}')">
					<input class="btn" type="button" value="삭제하기" onClick="go_deleteReply('${pageNum}','${qna.no}')">
			</c:if>
			<input class="btn" type="button" value="목록" onClick="go_qna_list('${pageNum}')">
		</div>	
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>