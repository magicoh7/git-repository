<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>

<script type="text/javascript">
	function go_member_list(pageNum) {
		document.frm.action = "ShoppingServlet?command=admin_member_list&pageNum=" + pageNum ;
		document.frm.submit();
	}
</script>

<article>
	<h1>회원 정보 상세 보기</h1>
	<form name="frm" method="post">
		<input type="hidden" name="mem_code" value="${member.mem_code }" />
		<table id="memberDetail">
			<tr>
				<th align="center">회원ID</th>
				<td>${member.mem_id}</td>
			</tr>
			<tr>
				<th align="center">회원명</th>
				<td>${member.name}</td>
			</tr>
			<tr>
				<th align="center">우편번호</th>
				<td>${member.zip_num}</td>
			</tr>
			<tr>
				<th align="center">주소</th>
				<td>${member.address1}</td>
			</tr>
			<tr>
				<th>전화번호</th>
				<td>${member.phone}</td>
			</tr>
			<tr>
				<th>사용여부</th>
				<td>
					<c:choose>
						<c:when test='${member.useyn=="y"}'>
							사용중
						</c:when>
						<c:otherwise>
							미사용
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
		<div id="btnDiv" >
			<!--목록 페이지로 이동하되 현재 페이지를 전달해 준다. -->
			<input class="btn" type="button" value="목록" onClick="go_member_list('${pageNum}')">
		</div>	
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>
