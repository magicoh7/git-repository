<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/admin/header.jsp"%>
<%@ include file="/admin/sub_menu.jsp"%>

<script type="text/javascript">
	function go_search() {
		document.frm.action = "ShoppingServlet?command=admin_member_list";
		document.frm.submit();
	}
	
	//회원 정보 상세 보기
	function go_MemberDetail(pageNum, mem_code) {
		var theForm = document.frm;
		// 회원 상세 보기 페이지에서 다시 회원 리스트로 돌아왔을 경우 현재 페이지로
		// 돌아올 수 있도록 하기 위해서 현재 페이지 번호를 넘겨준다.
		theForm.action =  "ShoppingServlet?command=admin_member_detail&pageNum=" +
							pageNum + "&mem_code=" + mem_code;
		theForm.submit();
	}
</script>

<article>
	<h1>회원리스트</h1>
	<form name="frm" method="post">
		<table style="float: right;">
			<tr>
				<td>회원 이름 
					<input type="text" name="searchText"> <input class="btn" type="button" value="검색" onclick="go_search()">
				</td>
			</tr>
		</table>
		<br>
		<table id="productList">
			<tr>
				<th>번호</th>
				<th>아이디(탈퇴여부)</th>
				<th>이름</th>
				<th>이메일</th>
				<th>우편번호</th>
				<th>주소</th>
				<th>전화</th>
				<th>가입일</th>
			</tr>
			<c:choose>
				<c:when test="${members.size() <= 0}">
					<tr>
						<td colspan="8" align="center" height="23">조회 결과가 없습니다.</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:set var="num" value="${totalRecord - ((pageNum - 1) * listCount) }" ></c:set>
					<c:forEach items="${members}" var="member" >
						<tr>
							<td align="center"><c:out value="${num}" /></td>
							<td style="text-align: left; padding-left: 10px; padding-right: 0px;">
								<a href="#" onClick="go_MemberDetail('${pageNum}', '${member.mem_code}')">${member.mem_id}</a>
								<c:choose>
									<c:when test='${member.useyn=="y"}'>
										<input type="checkbox" name="useyn" disabled="disabled">
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="useyn" checked="checked" disabled="disabled">
									</c:otherwise>
								</c:choose>
							</td>
							<td style="text-align: left; padding-left: 10px; padding-right: 0px;">
								<a href="#" onClick="go_MemberDetail('${pageNum}', '${member.mem_code}')">${member.name}</a>
							</td>
							<td>
						        <c:choose>
									<c:when test="${fn:length(member.email) > 10}">
										<c:out value="${fn:substring(member.email, 0, 10)}"/>....
						   			</c:when>
									<c:otherwise>
						            	<c:out value="${member.email}"/>
						        	</c:otherwise> 
								</c:choose>
							</td>
							<td>${member.zip_num}</td>
							<td>
						        <c:choose>
									<c:when test="${fn:length(member.address1) > 30}">
										<c:out value="${fn:substring(member.address1, 0, 30)}"/>....
						   			</c:when>
									<c:otherwise>
						            	<c:out value="${member.address1}"/>
						        	</c:otherwise> 
								</c:choose>
					         </td> 
							<td>${member.phone}</td>
							<td><fmt:formatDate value="${member.regdate}" /></td>
						</tr>
						<c:set var="num" value="${num-1 }"></c:set>
					</c:forEach>
					<tr>
						<td colspan="8" style="text-align: center;">${pageNavigator}</td>
					</tr>
				</c:otherwise>
			</c:choose>	
		</table>
	</form>
</article>
<%@ include file="/admin/footer.jsp"%>
</body>
</html>