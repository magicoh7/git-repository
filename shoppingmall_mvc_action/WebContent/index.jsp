<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="../header.jsp"%>

<!--접속후 첫 화면의 메인 이미지 들어가는 곳 시작 --->
<div class="clear"></div>
<div id="main_img">
	<img src="images/main_img.jpg">
</div>
<!--메인 이미지 들어가는 곳 끝--->

<div class="clear"></div>

<div id="front">
	<h2>New Item</h2>
	<div id="bestProduct">
		<c:forEach items="${newProductList }" var="productVo">
			<div id="item">
				<a href="ShoppingServlet?command=product_detail&code=${productVo.code}">
					<img src="product_images/${productVo.image}" />
					<h3>${productVo.name}</h3>
					<p>${productVo.list_price}</p>
				</a>
			</div>
		</c:forEach>
	</div>
	<div class="clear"></div>

	<h2>Best Item</h2>
	<div id="bestProduct">
		<c:forEach items="${bestProductList}" var="productVo">
			<div id="item">
				<a href="ShoppingServlet?command=product_detail&code=${productVo.code}">
					<img src="product_images/${productVo.image}" />
					<h3>${productVo.name}</h3>
					<p>${productVo.list_price}</p>
				</a>
			</div>
		</c:forEach>
	</div>
	<div class="clear"></div>

	<h2>All Item</h2>
	<div id="bestProduct">
		<c:forEach items="${allProductList}" var="productVo">
			<div id="item">
				<a href="ShoppingServlet?command=product_detail&code=${productVo.code}">
					<img src="product_images/${productVo.image}" />
					<h3>${productVo.name}</h3>
					<p>${productVo.list_price}</p>
				</a>
			</div>
		</c:forEach>
	</div>
	<div class="clear"></div>
</div>

<%@ include file="../footer.jsp"%>
