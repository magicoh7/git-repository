package com.magicoh.dto;

import java.util.Date;

public class ProductVo {
	 private Integer code;				//상품코드primary key	자동 매핑을 위해서(int -> Integer) 	
	 private String name;				//상품명
	 private String kind;				//상품종류
	 private Integer cost_price;		//원가
	 private Integer list_price;		//정가
	 private Integer sales_margin;		//마진
	 private String content;			//상품내용
	 private String image;				//상품이미지명
	 private String useyn;				//사용유무
	 private String bestyn;				//인기상품유무
	 private Date regdate;				//상품등록일
	 
	 public ProductVo() {
	 }
	 
	 public Integer getCode() {
		return code;
	 }
	 
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public Integer getCost_price() {
		return cost_price;
	}
	public void setCost_price(Integer cost_price) {
		this.cost_price = cost_price;
	}
	public Integer getList_price() {
		return list_price;
	}
	public void setList_price(Integer list_price) {
		this.list_price = list_price;
	}
	public Integer getSales_margin() {
		return sales_margin;
	}
	public void setSales_margin(Integer sales_margin) {
		this.sales_margin = sales_margin;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUseyn() {
		return useyn;
	}
	public void setUseyn(String useyn) {
		this.useyn = useyn;
	}
	public String getBestyn() {
		return bestyn;
	}
	public void setBestyn(String bestyn) {
		this.bestyn = bestyn;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	@Override
	public String toString() {
		return "ProductVo [code=" + code + ", name=" + name + ", kind=" + kind + ", cost_price=" + cost_price
				+ ", list_price=" + list_price + ", sales_margin=" + sales_margin + ", content=" + content + ", image="
				+ image + ", useyn=" + useyn + ", bestyn=" + bestyn + ", regdate=" + regdate + "]";
	}
	 
	 
}
