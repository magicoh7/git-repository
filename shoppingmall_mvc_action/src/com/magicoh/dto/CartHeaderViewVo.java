package com.magicoh.dto;

import java.util.Date;

public class CartHeaderViewVo {
	
	private Integer cart_id;		//장바구니 번호
	private String member_id;			//사용자 ID	
	private String member_name;		//장바구니 사용자명	
	private Integer grand_total; 	//장바구니 총합계금액	
	private String phone;			//연락처
	private String email;			//이메일
	private String zip_num;			//배송지 우편번호
	private String address1;		//배송주소1
	private String address2;		//배송주소2
	private Date regdate;			//주문일
	public Integer getCart_id() {
		return cart_id;
	}
	public void setCart_id(Integer cart_id) {
		this.cart_id = cart_id;
	}
	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public String getMember_name() {
		return member_name;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	public Integer getGrand_total() {
		return grand_total;
	}
	public void setGrand_total(Integer grand_total) {
		this.grand_total = grand_total;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getZip_num() {
		return zip_num;
	}
	public void setZip_num(String zip_num) {
		this.zip_num = zip_num;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	@Override
	public String toString() {
		return "CartHeaderViewVo [cart_id=" + cart_id + ", member_id=" + member_id + ", member_name=" + member_name
				+ ", grand_total=" + grand_total + ", phone=" + phone + ", email=" + email + ", zip_num=" + zip_num
				+ ", address1=" + address1 + ", address2=" + address2 + ", regdate=" + regdate + "]";
	}

	
	
}
