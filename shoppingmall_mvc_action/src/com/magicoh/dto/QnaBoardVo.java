package com.magicoh.dto;

import java.sql.Date;

public class QnaBoardVo
{
	private Integer no;
	private String subject;
	private String content;
	private String member_id;
	private String member_name;
	private String files;
	private Date regdate;
	private Date moddate;
	private Integer hit;
	private String delyn;
	//계층형 답변하기 게시판을 위한 변수
	private Integer groups;
	private Integer steps;
	private Integer indents;
	
	public QnaBoardVo() {
	}
	
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public String getMember_name() {
		return member_name;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}

	public String getFiles() {
		return files;
	}
	public void setFiles(String files) {
		this.files = files;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	public Integer getHit() {
		return hit;
	}
	public void setHit(Integer hit) {
		this.hit = hit;
	}
	public Integer getGroups() {
		return groups;
	}
	public void setGroups(Integer groups) {
		this.groups = groups;
	}
	public Integer getSteps() {
		return steps;
	}
	public void setSteps(Integer steps) {
		this.steps = steps;
	}
	public Integer getIndents() {
		return indents;
	}
	public void setIndents(Integer indents) {
		this.indents = indents;
	}
	public String getDelyn() {
		return delyn;
	}
	public void setDelyn(String delyn) {
		this.delyn = delyn;
	}

	@Override
	public String toString() {
		return "QnaBoardVo [no=" + no + ", subject=" + subject + ", content=" + content + ", member_id=" + member_id
				+ ", member_name=" + member_name + ", files=" + files + ", regdate=" + regdate + ", moddate=" + moddate
				+ ", hit=" + hit + ", delyn=" + delyn + ", groups=" + groups + ", steps=" + steps + ", indents="
				+ indents + "]";
	}



}

