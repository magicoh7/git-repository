package com.magicoh.dto;

import java.util.Date;

	public class CartItemViewVo {
		
		private Integer cart_id;		//장바구니 번호
		private String member_id;		//장바구니 사용자
		private Integer product_code;	//상품code
		private String product_name;	//상품명
		private Integer list_price;		//단가
		private Integer quantity;		//수량
		private Date regdate;			//주문일
		
		public CartItemViewVo() {
		}

		public Integer getCart_id() {
			return cart_id;
		}

		public void setCart_id(Integer cart_id) {
			this.cart_id = cart_id;
		}

		
		public String getMember_id() {
			return member_id;
		}

		public void setMember_id(String member_id) {
			this.member_id = member_id;
		}

		public Integer getProduct_code() {
			return product_code;
		}

		public void setProduct_code(Integer product_code) {
			this.product_code = product_code;
		}

		public String getProduct_name() {
			return product_name;
		}

		public void setProduct_name(String product_name) {
			this.product_name = product_name;
		}

		public Integer getList_price() {
			return list_price;
		}

		public void setList_price(Integer list_price) {
			this.list_price = list_price;
		}

		public Integer getQuantity() {
			return quantity;
		}

		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public Date getRegdate() {
			return regdate;
		}

		public void setRegdate(Date regdate) {
			this.regdate = regdate;
		}

		@Override
		public String toString() {
			return "CartItemViewVo [cart_id=" + cart_id + ", member_id=" + member_id + ", product_code=" + product_code
					+ ", product_name=" + product_name + ", list_price=" + list_price + ", quantity=" + quantity
					+ ", regdate=" + regdate + "]";
		}

	
}
