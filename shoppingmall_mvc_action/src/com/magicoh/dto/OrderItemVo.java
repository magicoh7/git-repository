package com.magicoh.dto;

import java.util.Date;

	public class OrderItemVo {
		
		private Integer order_item_id; 
		private Integer order_id;
		private Integer code;			//상품code
		private String name;			//상품명
		private Integer unit_price;
		private Integer quantity;
		private String status;
		private Date regdate;
		
		public OrderItemVo() {
		}

		public Integer getOrder_item_id() {
			return order_item_id;
		}

		public void setOrder_item_id(Integer order_item_id) {
			this.order_item_id = order_item_id;
		}

		public Integer getOrder_id() {
			return order_id;
		}

		public void setOrder_id(Integer order_id) {
			this.order_id = order_id;
		}

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getUnit_price() {
			return unit_price;
		}

		public void setUnit_price(Integer unit_price) {
			this.unit_price = unit_price;
		}

		public Integer getQuantity() {
			return quantity;
		}

		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Date getRegdate() {
			return regdate;
		}

		public void setRegdate(Date regdate) {
			this.regdate = regdate;
		}

		@Override
		public String toString() {
			return "OrderItemVo [order_item_id=" + order_item_id + ", order_id=" + order_id + ", code=" + code
					+ ", name=" + name + ", unit_price=" + unit_price + ", quantity=" + quantity + ", status=" + status
					+ ", regdate=" + regdate + "]";
		}

}
