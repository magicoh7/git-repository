package com.magicoh.dto;

import java.util.Date;

public class OrderVo {

	private Integer order_id;
    private String mem_id;
    private String status;
    private Integer sub_total;
    private Integer tax;     
    private Integer total;
    private Integer discount;
    private Integer grand_total;
    private String name;
    private String phone;                           
    private String email;                           
    private String zip_num;                         
    private String address1;                      
    private String address2;                     
    private String content;               
    private Date regdate;         
    private Date moddate;            
	
	public OrderVo() {
	}

	public Integer getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}



	public String getMem_id() {
		return mem_id;
	}

	public void setMem_id(String mem_id) {
		this.mem_id = mem_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getSub_total() {
		return sub_total;
	}

	public void setSub_total(Integer sub_total) {
		this.sub_total = sub_total;
	}

	public Integer getTax() {
		return tax;
	}

	public void setTax(Integer tax) {
		this.tax = tax;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Integer getGrand_total() {
		return grand_total;
	}

	public void setGrand_total(Integer grand_total) {
		this.grand_total = grand_total;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZip_num() {
		return zip_num;
	}

	public void setZip_num(String zip_num) {
		this.zip_num = zip_num;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getRegdate() {
		return regdate;
	}

	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}

	public Date getModdate() {
		return moddate;
	}

	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}

	@Override
	public String toString() {
		return "OrderVo [order_id=" + order_id + ", mem_id=" + mem_id + ", status=" + status + ", sub_total="
				+ sub_total + ", tax=" + tax + ", total=" + total + ", discount=" + discount + ", grand_total="
				+ grand_total + ", name=" + name + ", phone=" + phone + ", email=" + email + ", zip_num=" + zip_num
				+ ", address1=" + address1 + ", address2=" + address2 + ", content=" + content + ", regdate=" + regdate
				+ ", moddate=" + moddate + "]";
	}




}
