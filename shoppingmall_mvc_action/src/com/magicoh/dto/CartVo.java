package com.magicoh.dto;

import java.util.Date;

public class CartVo {
	private Integer cart_id;
	private String mem_id;
	private Integer product_code;
	private Integer quantity;
	private String status;
	private String regdate;
	
	public CartVo() {
	}

	public Integer getCart_id() {
		return cart_id;
	}

	public void setCart_id(Integer cart_id) {
		this.cart_id = cart_id;
	}

	public String getMem_id() {
		return mem_id;
	}

	public void setMem_id(String mem_id) {
		this.mem_id = mem_id;
	}

	public Integer getProduct_code() {
		return product_code;
	}

	public void setProduct_code(Integer product_code) {
		this.product_code = product_code;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRegdate() {
		return regdate;
	}

	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}

	@Override
	public String toString() {
		return "CartVo [cart_id=" + cart_id + ", mem_id=" + mem_id + ", product_code=" + product_code + ", quantity="
				+ quantity + ", status=" + status + ", regdate=" + regdate + "]";
	}


	
}
