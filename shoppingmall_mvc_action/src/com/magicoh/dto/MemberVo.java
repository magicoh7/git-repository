package com.magicoh.dto;

import java.util.Date;

public class MemberVo {
	private Integer mem_code;
	private String mem_id; 				
	private String pwd; 					
	private String name; 				
	private String email; 				
	private String phone; 				
	private String zip_num; 				
	private String address1; 			
	private String address2; 			
	private String previledge; 			
	private String useyn; 				
	private Date regdate; 			
    private Date moddate; 			
	
	public MemberVo() {
	}


	public Integer getMem_code() {
		return mem_code;
	}


	public void setMem_code(Integer mem_code) {
		this.mem_code = mem_code;
	}


	public String getMem_id() {
		return mem_id;
	}


	public void setMem_id(String mem_id) {
		this.mem_id = mem_id;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getZip_num() {
		return zip_num;
	}


	public void setZip_num(String zip_num) {
		this.zip_num = zip_num;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getPreviledge() {
		return previledge;
	}


	public void setPreviledge(String previledge) {
		this.previledge = previledge;
	}


	public String getUseyn() {
		return useyn;
	}


	public void setUseyn(String useyn) {
		this.useyn = useyn;
	}


	public Date getRegdate() {
		return regdate;
	}


	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}


	public Date getModdate() {
		return moddate;
	}


	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}

}
