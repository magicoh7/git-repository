package com.magicoh.dto;

import java.util.Date;

	public class OrderItemViewVo {
		
		private Integer order_item_id;	//주문item 번호	
		private String status;			//주문item 상태
		private Integer order_id;		//주문 번호
		private String mem_id;			//주문 사용자 ID	
		private String member_name;		//주문 사용자명	
		private Integer product_code;	//상품code
		private String product_name;	//상품명
		private Integer quantity;		//수량
		private Integer unit_price;		//단가
		private String zip_num;			//배송지 우편번호
		private String address;			//배송주소
		private String phone;			//연락처		
		private Date regdate;			//주문등록일
		
		public OrderItemViewVo() {
		}

		public Integer getOrder_item_id() {
			return order_item_id;
		}

		public void setOrder_item_id(Integer order_item_id) {
			this.order_item_id = order_item_id;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Integer getOrder_id() {
			return order_id;
		}

		public void setOrder_id(Integer order_id) {
			this.order_id = order_id;
		}

		public String getMem_id() {
			return mem_id;
		}

		public void setMem_id(String mem_id) {
			this.mem_id = mem_id;
		}

		public String getMember_name() {
			return member_name;
		}

		public void setMember_name(String member_name) {
			this.member_name = member_name;
		}

		public Integer getProduct_code() {
			return product_code;
		}

		public void setProduct_code(Integer product_code) {
			this.product_code = product_code;
		}

		public String getProduct_name() {
			return product_name;
		}

		public void setProduct_name(String product_name) {
			this.product_name = product_name;
		}

		public Integer getUnit_price() {
			return unit_price;
		}

		public void setUnit_price(Integer unit_price) {
			this.unit_price = unit_price;
		}

		public Integer getQuantity() {
			return quantity;
		}

		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public String getZip_num() {
			return zip_num;
		}

		public void setZip_num(String zip_num) {
			this.zip_num = zip_num;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public Date getRegdate() {
			return regdate;
		}

		public void setRegdate(Date regdate) {
			this.regdate = regdate;
		}

		
}
