package com.magicoh.controller;

import com.magicoh.controller.action.Action;
import com.magicoh.controller.action.CartDeleteAction;
import com.magicoh.controller.action.CartInsertAction;
import com.magicoh.controller.action.CartListAction;
import com.magicoh.controller.action.ContractAction;
import com.magicoh.controller.action.FindZipNumAction;
import com.magicoh.controller.action.IdCheckAction;
import com.magicoh.controller.action.IndexAction;
import com.magicoh.controller.action.JoinAction;
import com.magicoh.controller.action.JoinFormAction;
import com.magicoh.controller.action.LoginAction;
import com.magicoh.controller.action.LoginFormAction;
import com.magicoh.controller.action.LogoutAction;
import com.magicoh.controller.action.MyPageAction;
import com.magicoh.controller.action.OrderDetailAction;
import com.magicoh.controller.action.OrderInsertAction;
import com.magicoh.controller.action.OrderListAction;
import com.magicoh.controller.action.ProductDetailAction;
import com.magicoh.controller.action.ProductKindAction;
import com.magicoh.controller.action.QnaDetailAction;
import com.magicoh.controller.action.QnaListAction;
import com.magicoh.controller.action.QnaReplyAction;
import com.magicoh.controller.action.QnaReplyFormAction;
import com.magicoh.controller.action.QnaUpdateAction;
import com.magicoh.controller.action.QnaUpdateFormAction;
import com.magicoh.controller.action.QnaWriteAction;
import com.magicoh.controller.action.QnaWriteFormAction;
import com.magicoh.controller.admin.action.AdminLoginAction;
import com.magicoh.controller.admin.action.AdminLoginFormAction;
import com.magicoh.controller.admin.action.AdminLogoutAction;
import com.magicoh.controller.admin.action.AdminMemberDetailAction;
import com.magicoh.controller.admin.action.AdminMemberListAction;
import com.magicoh.controller.admin.action.AdminOrderListAction;
import com.magicoh.controller.admin.action.AdminOrderSaveAction;
import com.magicoh.controller.admin.action.AdminProductDetailAction;
import com.magicoh.controller.admin.action.AdminProductListAction;
import com.magicoh.controller.admin.action.AdminProductUpdateAction;
import com.magicoh.controller.admin.action.AdminProductUpdateFormAction;
import com.magicoh.controller.admin.action.AdminProductWriteAction;
import com.magicoh.controller.admin.action.AdminProductWriteFormAction;
import com.magicoh.controller.admin.action.AdminQnaDetailAction;
import com.magicoh.controller.admin.action.AdminQnaListAction;
import com.magicoh.controller.admin.action.AdminQnaReplyAction;
import com.magicoh.controller.admin.action.AdminQnaReplyFormAction;
import com.magicoh.controller.admin.action.AdminQnaUpdateAction;
import com.magicoh.controller.admin.action.AdminQnaUpdateFormAction;
import com.magicoh.controller.admin.action.AdminQnaWriteAction;
import com.magicoh.controller.admin.action.AdminQnaWriteFormAction;

public class ActionFactory {

	private static ActionFactory instance = new ActionFactory();
	
	private ActionFactory() {
	}
	
	public static ActionFactory getInstance() {
		return instance;
	}
	
	public Action getAction(String command) {
		
		Action action = null;
		
		//일반 사용자용
		//메인화면과 상품보기
		if (command.equals("index")) {
			action = new IndexAction();
		}else if (command.equals("catagory")) {
			action = new ProductKindAction();
		}else if (command.equals("product_detail")) {
			action = new ProductDetailAction();
		}
		//회원가입
		else if (command.equals("contract")) {
			action = new ContractAction();
		}else if (command.equals("join_form")) {
			action = new JoinFormAction();
		}else if (command.equals("id_check")) {
			action = new IdCheckAction();
		}else if (command.equals("find_zip_num")) {
			action = new FindZipNumAction();
		} else if (command.equals("join")) {
			action = new JoinAction();
		} else if (command.equals("login_form")) {
			action = new LoginFormAction();
		} else if (command.equals("login")) {
			action = new LoginAction();
		} else if (command.equals("logout")) {
			action = new LogoutAction();
		}
		//cart
		else if (command.equals("cart_insert")) {
			action = new CartInsertAction();
		} else if (command.equals("cart_list")) {
			action = new CartListAction();
		} else if (command.equals("cart_delete")) {
			action = new CartDeleteAction();
		} else if (command.equals("order_insert")) {
			action = new OrderInsertAction();
		} else if (command.equals("order_list")) {
			action = new OrderListAction();
		} else if (command.equals("order_detail")) {
			action = new OrderDetailAction();
		}
		//마이페이지
		else if (command.equals("mypage")) {
			action = new MyPageAction();
		} else if (command.equals("order_detail")) {
			action = new OrderDetailAction();
		}
		//QnA
		else if(command.equals("qna_list")) {
			action = new QnaListAction();
		}else if(command.equals("qna_detail")) {
			action = new QnaDetailAction();
		}else if(command.equals("qna_write_form")) {
			action = new QnaWriteFormAction();
		}else if(command.equals("qna_write")) {
			action = new QnaWriteAction();
		}else if(command.equals("qna_reply_form")) {
			action = new QnaReplyFormAction();
		}else if(command.equals("qna_reply")) {
			action = new QnaReplyAction();
		}else if(command.equals("qna_update_form")) {
			action = new QnaUpdateFormAction();
		}else if(command.equals("qna_update")) {
			action = new QnaUpdateAction();
		}
		
		//관리자용 로그인==================================
		if(command.equals("admin_login_form")) {
			//action = new IndexAction();
			action = new AdminLoginFormAction();
		}else if(command.equals("admin_login")) {
			action = new AdminLoginAction();
		}else if(command.equals("admin_logout")) {
			action = new AdminLogoutAction();
		}
		//상품
		else if(command.equals("admin_product_list")) {
			action = new AdminProductListAction();
		}else if(command.equals("admin_product_detail")) {
			action = new AdminProductDetailAction();
		}else if(command.equals("admin_product_update_form")) {
			action = new AdminProductUpdateFormAction();
		}else if(command.equals("admin_product_update")) {
			action = new AdminProductUpdateAction();
		}else if(command.equals("admin_product_write_form")) {
			action = new AdminProductWriteFormAction();
		}else if(command.equals("admin_product_write")) {
			action = new AdminProductWriteAction();
		}
		//주문(리스트/주문상태 업데이트)
		else if(command.equals("admin_order_list")) {
			action = new AdminOrderListAction();
		}else if(command.equals("admin_order_save")) {
			action = new AdminOrderSaveAction();
		}		
		//회원
		else if(command.equals("admin_member_list")) {
			action = new AdminMemberListAction();
		}else if(command.equals("admin_member_detail")) {
			action = new AdminMemberDetailAction();
		}
		//QnA
		else if(command.equals("admin_qna_list")) {
			action = new AdminQnaListAction();
		}else if(command.equals("admin_qna_detail")) {
			action = new AdminQnaDetailAction();
		}else if(command.equals("admin_qna_write_form")) {
			action = new AdminQnaWriteFormAction();
		}else if(command.equals("admin_qna_write")) {
			action = new AdminQnaWriteAction();
		}else if(command.equals("admin_qna_reply_form")) {
			action = new AdminQnaReplyFormAction();
		}else if(command.equals("admin_qna_reply")) {
			action = new AdminQnaReplyAction();
		}else if(command.equals("admin_qna_update_form")) {
			action = new AdminQnaUpdateFormAction();
		}else if(command.equals("admin_qna_update")) {
			action = new AdminQnaUpdateAction();
		}
	
		return action;
	}
}
