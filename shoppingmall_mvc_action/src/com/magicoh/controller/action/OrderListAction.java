package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.magicoh.dao.OrderDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.OrderItemViewVo;
import com.magicoh.dto.OrderVo;

public class OrderListAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/orderList.jsp";

		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			int order_id = 0;
			if(request.getParameter("order_id") != null) {
				order_id = Integer.parseInt(request.getParameter("order_id"));
			}

			OrderDao orderDao = OrderDao.getInstance();
			ArrayList<OrderItemViewVo> orderItemList = orderDao.selectOrderById(loginUser.getMem_id(), order_id);

			//화면 하단에 보여줄 총액
			int totalAmt = 0;
			for (OrderItemViewVo orderVo : orderItemList) {
				totalAmt += orderVo.getUnit_price() * orderVo.getQuantity();
			}

			request.setAttribute("orderItemList", orderItemList);
			request.setAttribute("totalAmt", totalAmt);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
