package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.OrderDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.OrderItemViewVo;

public class OrderDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/orderDetail.jsp";

		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");
		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			int order_id = Integer.parseInt(request.getParameter("order_id"));
			OrderDao orderDao = OrderDao.getInstance();
			ArrayList<OrderItemViewVo> orderList = orderDao.selectOrderById(loginUser.getMem_id(), order_id);

			int totalPrice = 0;
			for (OrderItemViewVo orderItem : orderList) {
				totalPrice += orderItem.getUnit_price() * orderItem.getQuantity();
			}
			request.setAttribute("orderDetail", orderList.get(0));	//처음 한개는 
			request.setAttribute("orderList", orderList);
			request.setAttribute("totalPrice", totalPrice);
			System.out.println(orderList);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
