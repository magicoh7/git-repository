package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.AddressDao;
import com.magicoh.dto.AddressVo;

public class FindZipNumAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/findZipNum.jsp";

		String dong = "";
		if(request.getParameter("dong") != "") {
			dong = request.getParameter("dong");
		}

		System.out.println("FindZipNum dong : " + dong);
		
		if (dong != null && dong.trim().equals("") == false) {
			AddressDao addressDao = AddressDao.getInstance();
			ArrayList<AddressVo> addressList = addressDao.selectAddressByDong(dong.trim());
			request.setAttribute("addressList", addressList);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
