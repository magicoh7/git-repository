package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.ProductDao;
import com.magicoh.dto.ProductVo;

public class IndexAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/index.jsp";

		System.out.println("IndexAction");
		
		//메인화면(index.jsp)에 보여줄 데이터 조회(신규/베스트/전체)
		ProductDao productDao = ProductDao.getInstance();
		ArrayList<ProductVo> newProductList = productDao.selectNewProducts();
		ArrayList<ProductVo> bestProductList = productDao.selectBestProducts();
		ArrayList<ProductVo> allProductList = productDao.selectAllProducts();

		request.setAttribute("newProductList", newProductList);
		request.setAttribute("bestProductList", bestProductList);
		request.setAttribute("allProductList", allProductList);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

}
