package com.magicoh.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.QnaBoardDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.QnaBoardVo;

public class QnaReplyFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "qna/qnaReplyForm.jsp";
		
		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			int pageNum = 1;
			int no = 0;
			if(request.getParameter("pageNum") != null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum"));
			}
			if(request.getParameter("no") != null){
				no = Integer.parseInt(request.getParameter("no"));
			}
			
			//원글 조회
			QnaBoardDao dao = QnaBoardDao.getInstance();
			QnaBoardVo qna = dao.selectOneQnaByNo_g(no);
			
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("qna", qna);
		}
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}


}
