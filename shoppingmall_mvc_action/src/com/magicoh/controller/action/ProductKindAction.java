package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.ProductDao;
import com.magicoh.dto.ProductVo;


public class ProductKindAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productKind.jsp";
		String kind = "";
		
		if(request.getParameter("kind") != "") {
			kind = request.getParameter("kind");
		}

		ProductDao productDao = ProductDao.getInstance();
		ArrayList<ProductVo> productKindList = productDao.listKindProduct(kind);

		request.setAttribute("productKindList", productKindList);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
