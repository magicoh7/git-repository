package com.magicoh.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.ProductDao;
import com.magicoh.dto.ProductVo;


public class ProductDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productDetail.jsp";
		String code = "";
		
		if(request.getParameter("code") != "") {
			code = request.getParameter("code");
		}

		ProductDao productDao = ProductDao.getInstance();
		ProductVo productVo = productDao.selectProductByCode(code);

		request.setAttribute("productVo", productVo);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
