package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.AdminDao;
import com.magicoh.dao.QnaBoardDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.QnaBoardVo;

public class QnaUpdateFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//조회 결과 보여줄 페이지
		String url = "qna/qnaUpdateForm.jsp";
		
		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			//파라미터(요청 페이지, 상품코드)
			int pageNum = 1;
			int no = 0;
			if(request.getParameter("pageNum") != null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum"));
			}
			if(request.getParameter("no") != null){
				no = Integer.parseInt(request.getParameter("no"));
			}
			
			//System.out.println("AdminProductListAction pageNum / no: " + pageNum + " / " + no);
			
			//수정할 QnA 조회
			QnaBoardDao dao = QnaBoardDao.getInstance();
			QnaBoardVo qna = new QnaBoardVo();
			qna = dao.selectOneQnaByNo_g(no);
			
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("qna", qna);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}

}
