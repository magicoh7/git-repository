package com.magicoh.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.ProductDao;
import com.magicoh.dao.QnaBoardDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.ProductVo;
import com.magicoh.dto.QnaBoardVo;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

public class QnaWriteAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "ShoppingServlet?command=qna_list";
		int pageNum = 1;
		
		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
		
			if(request.getParameter("pageNum") != null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum"));
			}
			
			//get servlet context info from HttpSession 
			session = request.getSession();
			
			int maxPostSize = 5 * 1024 * 1024;
			String savePath = "product_images";
			ServletContext context = session.getServletContext();
			String saveDirectory = context.getRealPath(savePath);
			
			MultipartRequest multi = new MultipartRequest(request, 
														saveDirectory, 
														maxPostSize, 
														"UTF-8",
														new DefaultFileRenamePolicy());
			
			System.out.println("multi.getParameter(member_id) : " + multi.getParameter("member_id"));
			
			QnaBoardVo qna = new QnaBoardVo();
			qna.setMember_id(loginUser.getMem_id());			//세션에서 사용자ID
			qna.setSubject(multi.getParameter("subject"));		//답글 정보
			qna.setContent(multi.getParameter("content"));
			//qna.setMember_id(multi.getParameter("member_id"));
			qna.setFiles(multi.getParameter("files"));
			if(multi.getFilesystemName("files") != null) {
				qna.setFiles(multi.getFilesystemName("files"));
				//System.out.println("첨부 파일명 : " + multi.getFilesystemName("files"));
			}
			
			//디비 삽입
			QnaBoardDao dao = QnaBoardDao.getInstance();
			dao.InsertQna_g(qna);
		}
		request.setAttribute("pageNum", pageNum);
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}


}
