package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.OrderDao;
import com.magicoh.dto.CartItemViewVo;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.OrderItemMypageViewVo;
import com.magicoh.dto.OrderItemViewVo;
import com.magicoh.dto.OrderVo;

public class MyPageAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/mypage.jsp";

		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			OrderDao orderDao = OrderDao.getInstance();
			ArrayList<OrderItemMypageViewVo> orderItemList = orderDao.selectOrderItemByMemberId(loginUser.getMem_id());
			//화면 하단에 보여줄 총액
			int totalPrice = 0;
			for (OrderItemMypageViewVo orderItemVo : orderItemList) {
				totalPrice += orderItemVo.getUnit_price() * orderItemVo.getQuantity();
			}

			request.setAttribute("orderItemList", orderItemList);
			request.setAttribute("totalPrice", totalPrice);
			request.setAttribute("title", "진행중인 주문내역");
		}
		request.getRequestDispatcher(url).forward(request, response);

	}
}
