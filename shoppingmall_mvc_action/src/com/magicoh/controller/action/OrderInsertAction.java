package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.CartDao;
import com.magicoh.dao.OrderDao;
import com.magicoh.dto.CartHeaderViewVo;
import com.magicoh.dto.CartItemViewVo;
import com.magicoh.dto.CartVo;
import com.magicoh.dto.MemberVo;

public class OrderInsertAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = ""; //"ShoppingServlet?command=order_list";

		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");
		
		String[] arrCartId = null; 		
		String[] arrQuantity = null;	
				
		if(request.getParameterValues("selectedCart") != null){
			arrCartId = request.getParameterValues("selectedCart");
		}
		if(request.getParameterValues("quantity") != null){
			arrQuantity = request.getParameterValues("quantity");
		}
		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			ArrayList<CartItemViewVo> cartItemList = new ArrayList<CartItemViewVo>();
			int grandTotal = 0;
			//장바구니에서 선택된 상품(item) => Order_item에 저장
			for (int i=0; i<arrCartId.length;i++) {
				OrderDao orderDao = OrderDao.getInstance();
				CartItemViewVo cartViewVo = orderDao.selectCartItemByMemberId(arrCartId[i]);
				//장바구니 화면에서 받아온 수량으로 update
				cartViewVo.setQuantity(Integer.parseInt(arrQuantity[i]));	
				grandTotal += Integer.parseInt(arrQuantity[i]) * cartViewVo.getList_price(); //상품별 금액 합산
				cartItemList.add(cartViewVo);
			}
			//주문 헤더에 넣을 데이터 만드는 작업
			OrderDao orderDao = OrderDao.getInstance();
			CartHeaderViewVo cartHeaderViewVo = orderDao.selectCartByMemberId(loginUser.getMem_id());
			cartHeaderViewVo.setGrand_total(grandTotal);
			//저장작업
			int max_order_id = orderDao.insertOrder(cartItemList, cartHeaderViewVo);
			System.out.println("주문을 모두 처리하고 돌려받은 max_order_id : " + max_order_id);
			url = "ShoppingServlet?command=order_list&order_id=" + max_order_id;	//저장한 주문번호를 넘겨줌
		}
		response.sendRedirect(url);
	}
}
