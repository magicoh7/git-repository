package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.QnaBoardDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.QnaBoardVo;

public class QnaListAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		//조회 결과 보여줄 페이지
		String url = "qna/qnaList.jsp";
		
		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");

		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			//파라미터(요청 페이지, 검색어)
			int pageNum = 1;
			String searchText = "";
	
			if(request.getParameter("pageNum") != null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum"));
			}
			if(request.getParameter("searchText") != null){
				searchText = request.getParameter("searchText");
			}
			
			System.out.println("QnaListAction pageNum : " + pageNum);
			
			//QnA 리스트 조회
			List<QnaBoardVo> qnaBoards = new ArrayList<QnaBoardVo>();
			QnaBoardDao dao = QnaBoardDao.getInstance();
			qnaBoards = dao.selectAllQna_g(loginUser.getMem_id(), pageNum, searchText);
			int listCount = dao.getListCount();
			int totalRecord = dao.totalRecord(searchText);
			String pageNavigator = dao.getPageNavigator(totalRecord, pageNum, searchText);
	
			//send request setting
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("listCount", listCount);
			request.setAttribute("totalRecord", totalRecord);
			request.setAttribute("searchText", searchText);
			request.setAttribute("qnaBoards", qnaBoards);
			request.setAttribute("pageNavigator", pageNavigator);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}

}
