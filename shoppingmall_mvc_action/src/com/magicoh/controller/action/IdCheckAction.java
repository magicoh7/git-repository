package com.magicoh.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.MemberDao;

public class IdCheckAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String url = "/member/join.jsp";

		String mem_id = "";
		String result = "0"; // 기본은 중복으로 설정

		if (request.getParameter("mem_id") != "") {
			mem_id = request.getParameter("mem_id");
		}

		MemberDao dao = MemberDao.getInstance();
		result = dao.confirmId(mem_id);

		PrintWriter out = response.getWriter();
		System.out.println("IdCheckAction  result : " + result);
		out.write(result);

//		request.setAttribute("requestedId", mem_id);
//		request.setAttribute("data", result);
//		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
//		dispatcher.forward(request, response);

	}
}
