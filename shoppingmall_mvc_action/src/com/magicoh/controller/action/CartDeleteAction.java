package com.magicoh.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.CartDao;

public class CartDeleteAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "ShoppingServlet?command=cart_list";

		String[] arrCart = request.getParameterValues("deleteCart");

		for (String cartId : arrCart) {
			CartDao cartDao = CartDao.getInstance();
			cartDao.deleteCart(Integer.parseInt(cartId));
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
