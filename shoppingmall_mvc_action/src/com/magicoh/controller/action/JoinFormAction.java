package com.magicoh.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.MemberDao;

public class JoinFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/member/join.jsp";
		response.setContentType("text/html; charset=UTF-8");

		//회원가입 입력폼에서 전달되온 파라미터
		String mem_id = "";
		
		if(request.getParameter("mem_id") != ""){
			mem_id = request.getParameter("mem_id");
		}
		
		
		request.setAttribute("requestedId", mem_id);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
