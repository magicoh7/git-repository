package com.magicoh.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.MemberDao;
import com.magicoh.dto.MemberVo;

public class LoginAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/login_fail.jsp";
		String result = "";
		String id = "";
		String pwd = "";
		
		MemberVo memberVo = new MemberVo();
		HttpSession session = request.getSession();

		
		if(request.getParameter("mem_id") != "") {
			id = request.getParameter("mem_id");
		}
		if(request.getParameter("pwd") != "") {
			pwd = request.getParameter("pwd");
		}

		//System.out.println(" LoginAction : " + id);
		
		MemberDao memberDao = MemberDao.getInstance();
		memberVo = memberDao.loginCheck(id);

		if (memberVo.getMem_id() != null) {
			if (memberVo.getPwd().equals(pwd)) {	//id, pwd ok
				//session.removeAttribute("loginUser");
				session.setAttribute("loginUser", memberVo);
				url = "ShoppingServlet?command=index";
				result = "1";
			} else {		//id exist but pwd not correct
				result = "0";
			}
		} else {
			result = "-1";	//id not exist
		}

		PrintWriter out = response.getWriter();
		//System.out.println("로그인 result : " + result);
		out.write(result);
		
		//request.getRequestDispatcher(url).forward(request, response);
	}
}
