package com.magicoh.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.CartDao;
import com.magicoh.dto.CartVo;
import com.magicoh.dto.MemberVo;

public class CartInsertAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "ShoppingServlet?command=cart_list";

		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");
		
		String product_code = "";		
		String quantity = "";
		
		if(request.getParameter("product_code") != "") {
			product_code = request.getParameter("product_code");
		}
		if(request.getParameter("quantity") != "") {
			quantity = request.getParameter("quantity");
		}
		
		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			CartVo cartVo = new CartVo();
			cartVo.setMem_id(loginUser.getMem_id());;
			cartVo.setProduct_code(Integer.parseInt(product_code));
			cartVo.setQuantity(Integer.parseInt(quantity));
		
			CartDao cartDao = CartDao.getInstance();
			cartDao.insertCart(cartVo);
		}
		//response.sendRedirect(url);
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}
}
