package com.magicoh.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.CartDao;
import com.magicoh.dto.CartItemViewVo;
import com.magicoh.dto.CartVo;
import com.magicoh.dto.MemberVo;


public class CartListAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/cartList.jsp";
		int totalAmt = 0;

		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");
		
		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			CartDao cartDao = CartDao.getInstance();
			ArrayList<CartItemViewVo> cartList = cartDao.selectCartByMemberId(loginUser.getMem_id());
			for (CartItemViewVo cartViewVo : cartList) {
				totalAmt += cartViewVo.getList_price() * cartViewVo.getQuantity();
			}

			request.setAttribute("cartList", cartList);
			request.setAttribute("totalAmt", totalAmt);
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
