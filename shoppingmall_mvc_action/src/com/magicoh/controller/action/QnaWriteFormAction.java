package com.magicoh.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dto.MemberVo;

public class QnaWriteFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "qna/qnaWriteForm.jsp";
		
		HttpSession session = request.getSession();
		MemberVo loginUser = (MemberVo) session.getAttribute("loginUser");
		int pageNum = 1;

		if (loginUser == null) {
			url = "ShoppingServlet?command=login_form";
		} else {
			//파라미터(요청 페이지, 검색어)
			if(request.getParameter("pageNum") != null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum"));
			}
		}

		request.setAttribute("pageNum", pageNum);
		request.getRequestDispatcher(url).forward(request, response);

	}

}
