package com.magicoh.controller.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.MemberDao;
import com.magicoh.dto.MemberVo;


public class JoinAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/login.jsp";
		
		String result = "0"; // 회원가입 상황 기본은 실패로 설정(1-저장성공/2-실패)
		
		HttpSession session = request.getSession();

		MemberVo memberVo = new MemberVo();

		memberVo.setMem_id(request.getParameter("mem_id"));
		memberVo.setPwd(request.getParameter("pwd"));
		memberVo.setName(request.getParameter("name"));
		memberVo.setEmail(request.getParameter("email"));
		memberVo.setPhone(request.getParameter("phone"));
		memberVo.setZip_num(request.getParameter("zipNum"));
		memberVo.setAddress1(request.getParameter("addr1"));
		memberVo.setAddress2(request.getParameter("addr2"));

		//session.setAttribute("mem_id", request.getParameter("mem_id"));

		System.out.println("IdCheckAction id : " + memberVo.getMem_id());

		MemberDao dao = MemberDao.getInstance();
		result = dao.insertMember(memberVo);

		PrintWriter out = response.getWriter();
		//System.out.println("회원 저장  result : " + result);
		out.write(result);
		
		//RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		//dispatcher.forward(request, response);
	}
}
