package com.magicoh.controller.admin.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.controller.action.Action;

public class AdminQnaWriteFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "admin/qna/qnaWriteForm.jsp";
		
		//파라미터(요청 페이지, 검색어)
		int pageNum = 1;
		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}

		request.setAttribute("pageNum", pageNum);
		request.getRequestDispatcher(url).forward(request, response);

	}

}
