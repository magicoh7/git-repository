package com.magicoh.controller.admin.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.OrderDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.OrderItemViewVo;
import com.magicoh.dto.OrderVo;

public class AdminOrderListAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String url = "admin/order/orderList.jsp";
		String searchText = "";
		if (request.getParameter("searchText") != null) {
			searchText = request.getParameter("searchText");
		}

		OrderDao dao = OrderDao.getInstance();
		ArrayList<OrderItemViewVo> orderItemList = dao.selectOrderItemList(searchText);

		request.setAttribute("orderItemList", orderItemList);

		request.getRequestDispatcher(url).forward(request, response);
	}
}
