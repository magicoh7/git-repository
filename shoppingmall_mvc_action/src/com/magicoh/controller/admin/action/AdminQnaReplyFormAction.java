package com.magicoh.controller.admin.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.QnaBoardDao;
import com.magicoh.dto.QnaBoardVo;

public class AdminQnaReplyFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "admin/qna/qnaReplyForm.jsp";
		
		int pageNum = 1;
		int no = 0;
		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		if(request.getParameter("no") != null){
			no = Integer.parseInt(request.getParameter("no"));
		}
		
		//원글 조회
		QnaBoardDao dao = QnaBoardDao.getInstance();
		QnaBoardVo qna = dao.selectOneQnaByNo(no);
		
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("qna", qna);
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}


}
