package com.magicoh.controller.admin.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.ProductDao;
import com.magicoh.dto.ProductVo;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

public class AdminProductUpdateAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "ShoppingServlet?command=admin_product_list";
		int pageNum = 1;
		
		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		
		//multi/part-form request
		HttpSession session = request.getSession();
		
		int maxPostSize = 5 * 1024 * 1024;
		String savePath = "product_images";
		ServletContext context = session.getServletContext();
		String saveDirectory = context.getRealPath(savePath);
		
		MultipartRequest multi = new MultipartRequest(request, 
													saveDirectory, 
													maxPostSize, 
													"UTF-8",
													new DefaultFileRenamePolicy());
		
		ProductVo product = new ProductVo();
		product.setCode(Integer.parseInt(multi.getParameter("code")));
		product.setName(multi.getParameter("name"));
		product.setKind(multi.getParameter("kind"));
		product.setCost_price(Integer.parseInt(multi.getParameter("cost_price")));
		product.setList_price(Integer.parseInt(multi.getParameter("list_price")));
		product.setSales_margin(Integer.parseInt(multi.getParameter("sales_margin")));
		product.setContent(multi.getParameter("content"));
		if(multi.getFilesystemName("image") == null) {
			product.setImage(multi.getParameter("nonMakeImg"));
		} else {
			product.setImage(multi.getFilesystemName("image"));
		}
		product.setUseyn(multi.getParameter("useyn"));
		product.setBestyn(multi.getParameter("bestyn"));
		
		//디비 업데이트
		ProductDao dao = ProductDao.getInstance();
		dao.updateProduct(product);
		
		request.setAttribute("pageNum", pageNum);
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}


}
