package com.magicoh.controller.admin.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.AdminDao;
import com.magicoh.dao.ProductDao;
import com.magicoh.dto.ProductVo;

public class AdminProductUpdateFormAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		//조회 결과 보여줄 페이지
		String url = "admin/product/productUpdateForm.jsp";
		
		//파라미터(요청 페이지, 상품코드)
		int pageNum = 1;
		String code = "";
		
		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		if(request.getParameter("code") != null){
			code = request.getParameter("code");
		}
		
		//System.out.println("AdminProductListAction pageNum / code: " + pageNum + " / " + code);
		
		//상품 리스트 조회
		ProductDao dao = ProductDao.getInstance();
		ProductVo product = new ProductVo();
		product = dao.selectProductByCode(code);
		
		//value of send to jsp setting
		String[] kinds = { "0", "Heels", "Boots", "Sandals", "Slipers", "Shcakers", "Sale" };
		request.setAttribute("kinds", kinds);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("product", product);
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}

}
