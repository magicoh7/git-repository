package com.magicoh.controller.admin.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.MemberDao;
import com.magicoh.dto.MemberVo;

public class AdminMemberDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//조회 결과 보여줄 페이지
		String url = "admin/member/memberDetail.jsp";
		
		//파라미터(요청 페이지, 상품코드)
		int pageNum = 1;
		String mem_id = "";
		
		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		if(request.getParameter("mem_code") != null){
			mem_id = request.getParameter("mem_id");
		}
		
		System.out.println("AdminMemberDetailAction pageNum / Member id: " + pageNum + " / " + mem_id);
		
		//상품 리스트 조회
		MemberDao dao = MemberDao.getInstance();
		MemberVo member = dao.selectMemberById(mem_id);
		
		//value of send to jsp setting
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("member", member);
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);

	}

}
