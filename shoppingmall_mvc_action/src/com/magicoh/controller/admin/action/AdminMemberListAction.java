package com.magicoh.controller.admin.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.MemberDao;
import com.magicoh.dto.MemberVo;

public class AdminMemberListAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		//조회 결과 보여줄 페이지
		String url = "admin/member/memberList.jsp";
		
		//파라미터(요청 페이지, 검색어)
		int pageNum = 1;
		String searchText = "";

		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		if(request.getParameter("searchText") != null){
			searchText = request.getParameter("searchText");
		}
		
		//회원 리스트 조회
		List<MemberVo> members = new ArrayList<MemberVo>();

		MemberDao dao = MemberDao.getInstance();
		members = dao.selectAllMember(pageNum, searchText);
		int listCount = dao.getListCount();
		int totalRecord = dao.totalRecord(searchText);
		String pageNavigator = dao.getPageNavigator(totalRecord, 
												pageNum, searchText);

		//send request setting
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("listCount", listCount);
		request.setAttribute("totalRecord", totalRecord);
		request.setAttribute("searchText", searchText);
		request.setAttribute("members", members);
		request.setAttribute("pageNavigator", pageNavigator);
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}

}
