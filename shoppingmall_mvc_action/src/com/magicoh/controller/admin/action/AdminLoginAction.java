package com.magicoh.controller.admin.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.AdminDao;
import com.magicoh.dto.WorkerVo;

public class AdminLoginAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//로그인 실패시 이동할 경로(다시 로그인 폼으로 이동)
		String url = "ShoppingServlet?command=admin_login_form";
		
		System.out.println("AdminLoginAction : ---------------");
		
		String id = request.getParameter("id").trim();
		String pwd = request.getParameter("pwd").trim();
		String msg = null;
		
		//id, pwd check
		WorkerVo worker = null;
		AdminDao dao = AdminDao.getInstance();
		worker = dao.checkAdmin(id, pwd);
		
		if(worker != null) {
			HttpSession session = request.getSession();
			session.setAttribute("worker", worker);
			url = "ShoppingServlet?command=admin_product_list";
		}else {
			msg = "아이디와 비밀번호를 확인하세요.";
			request.setAttribute("msg", msg);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
		
	}

}
