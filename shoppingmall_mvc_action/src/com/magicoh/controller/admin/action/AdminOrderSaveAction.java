package com.magicoh.controller.admin.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.OrderDao;

public class AdminOrderSaveAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "ShoppingServlet?command=admin_order_list";
		String[] statusArr = null;
		
		//체크된 미처리 항목의 key 값(order_item_id)
		if(request.getParameterValues("status") != null) {
			statusArr = request.getParameterValues("status");
			System.out.println("체크된 항목 갯수 : " + statusArr.length);

			for (String order_item_id : statusArr) {
				OrderDao orderDao = OrderDao.getInstance();
				orderDao.updateOrderStatus(order_item_id);
			}
			
		}	

		request.getRequestDispatcher(url).forward(request, response);
	}
}
