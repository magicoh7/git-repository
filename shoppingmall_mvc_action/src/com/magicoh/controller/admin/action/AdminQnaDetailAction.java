package com.magicoh.controller.admin.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.controller.action.Action;
import com.magicoh.dao.MemberDao;
import com.magicoh.dao.QnaBoardDao;
import com.magicoh.dto.MemberVo;
import com.magicoh.dto.QnaBoardVo;

public class AdminQnaDetailAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//조회 결과 보여줄 페이지
		String url = "admin/qna/qnaDetail.jsp";
		
		//파라미터(요청 페이지, 상품코드)
		int pageNum = 1;
		int no = 0;
		
		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		if(request.getParameter("no") != null){
			no = Integer.parseInt(request.getParameter("no"));
		}
		
		System.out.println("AdminQnaDetailAction pageNum / Qna No: " + pageNum + " / " + no);
		
		//QnA 리스트 조회
		QnaBoardDao dao = QnaBoardDao.getInstance();
		QnaBoardVo qna = dao.selectOneQnaByNo(no);
		//value of send to jsp setting
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("qna", qna);
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);

	}

}
