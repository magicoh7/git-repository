package com.magicoh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.magicoh.dto.AddressVo;
import com.magicoh.util.DbManager;

public class AddressDao {
	private AddressDao() {
	}

	private static AddressDao instance = new AddressDao();

	public static AddressDao getInstance() {
		return instance;
	}

	public ArrayList<AddressVo> selectAddressByDong(String dong) {
		ArrayList<AddressVo> list = new ArrayList<AddressVo>();

		String sql = "Select zip_num,"
					+ " sido,"
					+ " gugun,"
					+ " dong,"
					+ " zip_code,"
					+ " bunji"
					+ " From address "
					+ " Where dong like '%'||?||'%'";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, dong);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				AddressVo addressVo = new AddressVo();
				addressVo.setzipNum(rs.getString("zip_num"));
				addressVo.setSido(rs.getString("sido"));
				addressVo.setGugun(rs.getString("gugun"));
				addressVo.setDong(rs.getString("dong"));
				addressVo.setzipCode(rs.getString("zip_code"));
				addressVo.setBunji(rs.getString("bunji"));
				list.add(addressVo);
				//System.out.println(addressVo.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
