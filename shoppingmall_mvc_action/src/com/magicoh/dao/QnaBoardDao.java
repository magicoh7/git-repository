package com.magicoh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.magicoh.dto.QnaBoardVo;
import com.magicoh.util.DbManager;

public class QnaBoardDao {
	//paging variables
	public int listCount = 5; 			// 한 페이지에 보여줄 게시물 목록수
	public int pagePerBlock = 3; 		// 한 페이지에 보여줄 페이지 블럭

	private static QnaBoardDao instance = new QnaBoardDao();
	
	private QnaBoardDao() {
	}
	
	public static QnaBoardDao getInstance() {
		return instance;
	}

	public int getListCount() {
		return listCount;
	}
	
	//일반 사용자용 ==================================================	
	//자신의 QnA 조회  
	public ArrayList<QnaBoardVo> selectAllQna_g(String member_id, int pageNum, String searchText) {
		ArrayList<QnaBoardVo> qnaList = new ArrayList<QnaBoardVo>();

		String sql = "Select "
					+ " no, "
					+ " subject, "
					+ " member_id, "
					+ " member_name,"
					+ " moddate, "
					+ " hit, "
					+ " delyn,"
					+ " groups, "
					+ " steps, "
					+ " indents"
					+ " From qna_board "
					+ " Where member_id=?"
					+ " And subject like '%'||?||'%' "
					+ " Order By "
					+ " groups Desc,"
					+ " steps Asc";

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		int absolutePage = 1;

		try {
			con = DbManager.getConnection();
			absolutePage = (pageNum - 1) * listCount + 1;	//시작게시물
			pstmt = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
										ResultSet.CONCUR_UPDATABLE);

			if (searchText.equals("")) {
				pstmt.setString(1, member_id);
				pstmt.setString(2, "%");
			} else {
				pstmt.setString(1, member_id);
				pstmt.setString(2, searchText);
			}

			rs = pstmt.executeQuery();

			System.out.println("이동할 absolutepage : " + absolutePage);
			
			if (rs.next()) {
				rs.absolute(absolutePage);	//읽어 올 시작 게시물로 커서 이동
				int count = 0;
				//한 페이지에 표시할 게시물 만큼 get	
				while (count < listCount) {
					QnaBoardVo qna = new QnaBoardVo();
					qna.setNo(rs.getInt("no"));
					qna.setSubject(rs.getString("subject"));
					qna.setMember_id(rs.getString("member_id"));
					qna.setMember_name(rs.getString("member_name"));
					qna.setModdate(rs.getDate("moddate"));
					qna.setHit(rs.getInt("hit"));
					qna.setGroups(rs.getInt("groups"));
					qna.setSteps(rs.getInt("steps"));
					qna.setIndents(rs.getInt("indents"));
					qna.setDelyn(rs.getString("delyn"));
					qnaList.add(qna);
					if (rs.isLast()) {
						break;
					}
					rs.next();
					count++;
					System.out.println(qna.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(con, pstmt, rs);
		}
		return qnaList;
	}
	
	//qna 원글 등록
	public int InsertQna_g(QnaBoardVo qna) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int ar = 0;
		
		String sql = "Insert Into qna_board(" 
					+ " no," 
					+ " subject," 
					+ " content," 
					+ " member_id," 
					+ " files,"
					+ " regdate,"
					+ " moddate,"
					+ " hit,"
					+ " groups,"
					+ " steps,"
					+ " indents,"
					+ " delyn)"
					+ " Values("
					+ " qna_board_no_seq.nextval,"	//no
					+ " ?, ?, ?, ?,"				//subject, content, member_id, files
					+ " sysdate,"					//regdate
					+ " sysdate,"					//moddate
					+ " 0,"							//hit
					+ " qna_board_no_seq.currval,"	//groups
					+ " 0,"							//steps
					+ " 0,"							//indents
					+ " 'n')";

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, qna.getSubject());
			pstmt.setString(2, qna.getContent());
			pstmt.setString(3, qna.getMember_id());
			pstmt.setString(4, qna.getFiles());
			ar = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return ar;
	}
	
	//한 개의 qna 조회
	public QnaBoardVo selectOneQnaByNo_g(int no) {
		//조회수 증가
		upHit(no);
		
		String sql = "Select "
					+ " no, "
					+ " subject, "
					+ " content, "
					+ " files,"
					+ " member_id, "
					+ " member_name, "
					+ " moddate, "
					+ " hit, "
					+ " delyn, "
					+ " groups, "
					+ " steps, "
					+ " indents"
					+ " From qna_board " 
					+ " Where no = ?";
		
		//System.out.println("sql : " + sql);
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		QnaBoardVo qna = null;
		
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, no);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				qna = new QnaBoardVo();
				qna.setNo(rs.getInt("no"));
				qna.setSubject(rs.getString("subject"));
				qna.setContent(rs.getString("content"));
				qna.setMember_id(rs.getString("member_id"));
				qna.setFiles(rs.getString("files"));
				qna.setModdate(rs.getDate("moddate"));
				qna.setHit(rs.getInt("hit"));
				qna.setDelyn(rs.getString("delyn"));
				qna.setGroups(rs.getInt("groups"));
				qna.setSteps(rs.getInt("steps"));
				qna.setIndents(rs.getInt("indents"));
				System.out.println(qna.toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DbManager.close(conn, pstmt, rs);
		}
		
		return qna;
	}
	
	//원글에 대한 답변글쓰기
	public void replyQna_g(String id, QnaBoardVo qna)
	{
		System.out.println("replyQna_g id : " + id);
		
		//원글의 groups, steps + 1(답글이 위로 저장될 수 있도록 한칸씩 뒤로 밀림)
		replyShape_g(id, qna.getGroups(), qna.getSteps());
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int ar = 0;
		try
		{
			conn = DbManager.getConnection();
			// 트랜잭션 - 자동 커밋을 false로 한다.
			//conn.setAutoCommit(false);
			String sql = "Insert Into qna_board(" 
					+ " no," 
					+ " subject," 
					+ " content," 
					+ " member_id," 
					+ " files,"
					+ " regdate,"
					+ " moddate,"
					+ " hit,"
					+ " groups,"
					+ " steps,"
					+ " indents,"
					+ " delyn)"
					+ " Values("
					+ " qna_board_no_seq.nextval,"	//no
					+ " ?, ?, ?, ?,"				//subject, content, member_id, files
					+ " sysdate,"					//regdate
					+ " sysdate,"					//moddate
					+ " 0,"							//hit
					+ " ?,"							//groups
					+ " ?,"							//steps
					+ " ?,"							//indents
					+ " 'n')";

			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, qna.getSubject());	//subject
			pstmt.setString(2, qna.getContent());	//content
			pstmt.setString(3, qna.getMember_id());	//id
			pstmt.setString(4, qna.getFiles());		//files
			pstmt.setInt(5, qna.getGroups());		//원글의 groups
			pstmt.setInt(6, qna.getSteps() + 1);	//원글의 steps + 1
			pstmt.setInt(7, qna.getIndents() + 1);	//원글의 indents + 1
			ar = pstmt.executeUpdate();
			//conn.commit();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			DbManager.close(conn, pstmt);
		}
	}
	
	//답글을 쓰기 전에 기존에 있는 답글을 뒤로 밀어낸다(steps = steps+1)
		public int replyShape_g(String id, int groups, int steps)
		{
			Connection conn = null;
			PreparedStatement pstmt = null;
			int ar = 0;
			
			try
			{
				conn = DbManager.getConnection();
				String sql = "Update qna_board Set "
							+ " steps = steps + 1"
							+ " Where member_id=?"
							+ " And groups = ?"
							+ " AND steps > ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, id);
				pstmt.setInt(2, steps);
				pstmt.setInt(3, steps);
				ar = pstmt.executeUpdate();
				
			} catch (Exception e)
			{
				e.printStackTrace();
			} finally {
				DbManager.close(conn, pstmt);
			}
			return ar;
		}

		//QnA 업데이트
		public int updateQna_g(QnaBoardVo qna) {
			Connection conn = null;
			PreparedStatement pstmt = null;
			int ar = 0;
			
			String sql = "Update qna_board" +
						" Set subject = ?," +
						" content = ?," +
						" member_id = ?," +
						" files = ?," +
						" moddate = sysdate" +
						" Where no = ?";
			
			try {
				conn = DbManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, qna.getSubject());
				pstmt.setString(2, qna.getContent());
				pstmt.setString(3, qna.getMember_id());
				pstmt.setString(4, qna.getFiles());
				pstmt.setInt(5, qna.getNo());
				ar = pstmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbManager.close(conn, pstmt);
			}
			return ar;
		}
	
	//관리자용 ======================================================
	//관리자용 ======================================================
	//관리자용 ======================================================
	//전체 qna 레코드 조회  
	public ArrayList<QnaBoardVo> selectAllQna(int pageNum, String searchText) {
		ArrayList<QnaBoardVo> qnaList = new ArrayList<QnaBoardVo>();

		String sql = "Select "
					+ " no, "
					+ " subject, "
					+ " member_id, "
					+ " member_name,"
					+ " moddate, "
					+ " hit, "
					+ " delyn,"
					+ " groups, "
					+ " steps, "
					+ " indents"
					+ " From qna_board "
					+ " Where subject like '%'||?||'%' "
					+ " Order By "
					+ " groups Desc,"
					+ " steps Asc";

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		int absolutePage = 1;

		try {
			con = DbManager.getConnection();
			absolutePage = (pageNum - 1) * listCount + 1;	//시작게시물
			pstmt = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
										ResultSet.CONCUR_UPDATABLE);

			if (searchText.equals("")) {
				pstmt.setString(1, "%");
			} else {
				pstmt.setString(1, searchText);
			}

			rs = pstmt.executeQuery();

			System.out.println("이동할 absolutepage : " + absolutePage);
			
			if (rs.next()) {
				rs.absolute(absolutePage);	//읽어 올 시작 게시물로 커서 이동
				int count = 0;
				//한 페이지에 표시할 게시물 만큼 get	
				while (count < listCount) {
					QnaBoardVo qna = new QnaBoardVo();
					qna.setNo(rs.getInt("no"));
					qna.setSubject(rs.getString("subject"));
					qna.setMember_id(rs.getString("member_id"));
					qna.setMember_name(rs.getString("member_name"));
					qna.setModdate(rs.getDate("moddate"));
					qna.setHit(rs.getInt("hit"));
					qna.setGroups(rs.getInt("groups"));
					qna.setSteps(rs.getInt("steps"));
					qna.setIndents(rs.getInt("indents"));
					qna.setDelyn(rs.getString("delyn"));
					qnaList.add(qna);
					if (rs.isLast()) {
						break;
					}
					rs.next();
					count++;
					System.out.println(qna.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(con, pstmt, rs);
		}
		return qnaList;
	}

	//한 개의 qna 조회
	public QnaBoardVo selectOneQnaByNo(int no) {
		//조회수 증가
		upHit(no);
		
		String sql = "Select "
					+ " no, "
					+ " subject, "
					+ " content, "
					+ " files,"
					+ " member_id, "
					+ " member_name, "
					+ " moddate, "
					+ " hit, "
					+ " delyn, "
					+ " groups, "
					+ " steps, "
					+ " indents"
					+ " From qna_board " 
					+ " Where no = ?";
		
		//System.out.println("sql : " + sql);
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		QnaBoardVo qna = null;
		
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, no);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				qna = new QnaBoardVo();
				qna.setNo(rs.getInt("no"));
				qna.setSubject(rs.getString("subject"));
				qna.setContent(rs.getString("content"));
				qna.setMember_id(rs.getString("member_id"));
				qna.setFiles(rs.getString("files"));
				qna.setModdate(rs.getDate("moddate"));
				qna.setHit(rs.getInt("hit"));
				qna.setDelyn(rs.getString("delyn"));
				qna.setGroups(rs.getInt("groups"));
				qna.setSteps(rs.getInt("steps"));
				qna.setIndents(rs.getInt("indents"));
				System.out.println(qna.toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DbManager.close(conn, pstmt, rs);
		}
		
		return qna;
	}
	
	
	//전체 레코드 수(검색어가 있으면 검색어가 포함된 전체 레코드만)
	public int totalRecord(String searchText) {
		int totalRecords = 0;
		String sql = "Select count(*) "
				+	" From qna_board"
				+	" Where subject like '%'||?||'%'";

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet pageset = null;

		try {
			con = DbManager.getConnection();
			pstmt = con.prepareStatement(sql);

			if (searchText.equals("")) {
				pstmt.setString(1, "%");
			} else {
				pstmt.setString(1, searchText);
			}
			pageset = pstmt.executeQuery();

			if (pageset.next()) {
				totalRecords = pageset.getInt(1); // 레코드의 개수
				pageset.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(con, pstmt);
		}
		return totalRecords;
	}

	//qna 원글 등록
	public int InsertQna(QnaBoardVo qna) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int ar = 0;
		
		String sql = "Insert Into qna_board(" 
					+ " no," 
					+ " subject," 
					+ " content," 
					+ " member_id," 
					+ " files,"
					+ " regdate,"
					+ " moddate,"
					+ " hit,"
					+ " groups,"
					+ " steps,"
					+ " indents,"
					+ " delyn)"
					+ " Values("
					+ " qna_board_no_seq.nextval,"	//no
					+ " ?, ?, ?, ?,"				//subject, content, member_id, files
					+ " sysdate,"					//regdate
					+ " sysdate,"					//moddate
					+ " 0,"							//hit
					+ " qna_board_no_seq.currval,"	//groups
					+ " 0,"							//steps
					+ " 0,"							//indents
					+ " 'n')";

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, qna.getSubject());
			pstmt.setString(2, qna.getContent());
			pstmt.setString(3, qna.getMember_id());
			pstmt.setString(4, qna.getFiles());
			ar = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return ar;
	}
	
	//QnA 업데이트
	public int updateQna(QnaBoardVo qna) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int ar = 0;
		
		String sql = "Update qna_board" +
					" Set subject = ?," +
					" content = ?," +
					" member_id = ?," +
					" files = ?," +
					" moddate = sysdate" +
					" Where no = ?";
		
		
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, qna.getSubject());
			pstmt.setString(2, qna.getContent());
			pstmt.setString(3, qna.getMember_id());
			pstmt.setString(4, qna.getFiles());
			pstmt.setInt(5, qna.getNo());
			ar = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return ar;
	}
	
	//원글에 대한 답변글쓰기
	public void replyQna(QnaBoardVo qna)
	{
		//원글의 groups, steps + 1(답글이 위로 저장될 수 있도록 한칸씩 뒤로 밀림)
		replyShape(qna.getGroups(), qna.getSteps());
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int ar = 0;
	
		try
		{
			conn = DbManager.getConnection();

			// 트랜잭션 - 자동 커밋을 false로 한다.
			//conn.setAutoCommit(false);
			

			String sql = "Insert Into qna_board(" 
					+ " no," 
					+ " subject," 
					+ " content," 
					+ " member_id," 
					+ " files,"
					+ " regdate,"
					+ " moddate,"
					+ " hit,"
					+ " groups,"
					+ " steps,"
					+ " indents,"
					+ " delyn)"
					+ " Values("
					+ " qna_board_no_seq.nextval,"	//no
					+ " ?, ?, ?, ?,"				//subject, content, member_id, files
					+ " sysdate,"					//regdate
					+ " sysdate,"					//moddate
					+ " 0,"							//hit
					+ " ?,"							//groups
					+ " ?,"							//steps
					+ " ?,"							//indents
					+ " 'n')";

			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, qna.getSubject());
			pstmt.setString(2, qna.getContent());
			pstmt.setString(3, qna.getMember_id());
			pstmt.setString(4, qna.getFiles());
			pstmt.setInt(5, qna.getGroups());			//원글의 groups
			pstmt.setInt(6, qna.getSteps() + 1);		//원글의 steps + 1
			pstmt.setInt(7, qna.getIndents() + 1);		//원글의 indents + 1
			ar = pstmt.executeUpdate();

			//conn.commit();

		}catch(SQLException e) {
			e.printStackTrace();
//			if( conn != null ) 
//				try { 
//					conn.rollback(); 
//				} catch(SQLException sqle) {
//					sqle.printStackTrace();
//				} 
//			throw new RuntimeException(e);
		}
		finally {
			DbManager.close(conn, pstmt);
		}

	}

	//답글을 쓰기 전에 기존에 있는 답글을 뒤로 밀어낸다(steps = steps+1)
	public int replyShape(int groups, int steps)
	{
		Connection conn = null;
		PreparedStatement pstmt = null;
		int ar = 0;
		
		try
		{
			conn = DbManager.getConnection();
			String sql = "Update qna_board Set "
						+ " steps = steps + 1 Where groups = ? AND steps > ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, groups);
			pstmt.setInt(2, steps);
			ar = pstmt.executeUpdate();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return ar;
	}
		
	
	//조회수 증가
	public void upHit(int no)
	{
		Connection conn = null;
		PreparedStatement pstmt = null;

		try
		{
			conn = DbManager.getConnection();
			
			String sql = "UPDATE qna_board SET hit = hit + 1 WHERE no = ?" ;
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, no);
			int ar = pstmt.executeUpdate();
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}finally {
			DbManager.close(conn, pstmt);
		}
	}	
	
	/**
	 * 게시판 하단의 페이지 블럭을 만들어주는 함수
	 * @param totalRecord	- 총수
	 * @param listCount		- 노출될 목록 게시물 수
	 * @param pagePerBlock	- 노출될 블록 수 [ 1 2 3 4 5 ] [6,7,8,9,10] 5개씩
	 * @param pageNum		- 요청된 페이지 번호
	 * @param searchType	- 검색 항목(미사용)
	 * @param searchText	- 검색어
	 * @return
	 */
	public String getPageNavigator(int totalRecord, 
			//int listCount, 	int pagePerBlock, 
			int pageNum, String searchText) {
		
		StringBuffer sb = new StringBuffer();	
		
		if(totalRecord > 0) {	//총 레코드 수가 하나라도 있어야 페이징
			//총 페이지수
			int totalNumOfPage = (totalRecord % listCount == 0) ? 
					totalRecord / listCount :	totalRecord / listCount + 1;
			
			//페이지 블록수
			int totalNumOfBlock = (totalNumOfPage % pagePerBlock == 0) ? 
					totalNumOfPage / pagePerBlock : totalNumOfPage / pagePerBlock + 1;
			
			//요청된 페이지가 몇 번째 페이지 블럭에 있는지 확인(디테일 뷰로 갔다 돌아왔을 때 기억하고 있어야.)
			int currentBlock = (pageNum % pagePerBlock == 0) ? 
					pageNum / pagePerBlock :
					pageNum / pagePerBlock + 1;
			
			//시작페이지와 끝페이지(한 페이지 블럭 내에서)
			int startPage = (currentBlock - 1) * pagePerBlock + 1;
			int endPage = startPage + pagePerBlock - 1;
			
			if(endPage > totalNumOfPage)
				endPage = totalNumOfPage;
			
			boolean isNext = false;
			boolean isPrev = false;
			
			//현재 페이지 블럭이 토털 페이지 블럭 보다 작을 때만 >> 보여줌(총 페이지 블럭이 4이며 1,2,3 블럭에는 > 표시)
			if(currentBlock < totalNumOfBlock)
				isNext = true;
			//첫 페이지를 제외하고 <이전 버튼 표시
			if(currentBlock > 1)
				isPrev = true;
			//총 페이지 블럭이 1이면 <이전 이후> 표시 안함
			if(totalNumOfBlock == 1){
				isNext = false;
				isPrev = false;
			}
			//페이지 번호가 1보다 크면 모두 <이전 버튼 표시
			if(pageNum > 1){	// 쿼리스트링에서 & = &amp; 
				sb.append("<a href=\"").append("ShoppingServlet?command=admin_qna_list&amp;pageNum=1&amp;searchText="+searchText);
				sb.append("\" title=\"<<\"><<</a>&nbsp;");
			}
			//<< 가 있으면
			if (isPrev) {	
				int goPrevPage = startPage - pagePerBlock;			
				sb.append("&nbsp;&nbsp;<a href=\"").append("ShoppingServlet?command=admin_qna_list&amp;pageNum="+goPrevPage+"&amp;searchText="+searchText);
				sb.append("\" title=\"<\"><</a>");
			} else {
				
			}
			//페이지 블럭 표시
			for (int i = startPage; i <= endPage; i++) {
				if (i == pageNum) {
					sb.append("<a href=\"#\"><strong>").append(i).append("</strong></a>&nbsp;&nbsp;");
				} else {
					sb.append("<a href=\"").append("ShoppingServlet?command=admin_qna_list&amp;pageNum="+i+"&amp;searchText="+searchText);
					sb.append("\" title=\""+i+"\">").append(i).append("</a>&nbsp;&nbsp;");
				}
			}
			//다음 > 버튼 눌렀을 때 이동할 페이지
			if (isNext) {
				//이동할 페이지 = 현재 페이지블럭의 시작페이지 + 페이지당 표시할 블럭수
				int goNextPage = startPage + pagePerBlock;	
	
				sb.append("<a href=\"").append("ShoppingServlet?command=admin_qna_list&amp;pageNum="+goNextPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">\">></a>");
			} else {
				
			}
			//요청된 페이지가 전체 페이지수 보다 작으면 다음 > 버튼 표시
			if(totalNumOfPage > pageNum){
				sb.append("&nbsp;&nbsp;<a href=\"").append("ShoppingServlet?command=admin_qna_list&amp;pageNum="+totalNumOfPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">>\">>></a>");
			}
		}
		
		return sb.toString();
	}


	
}
