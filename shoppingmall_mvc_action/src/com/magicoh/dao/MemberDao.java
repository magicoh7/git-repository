package com.magicoh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.magicoh.dto.MemberVo;
import com.magicoh.util.DbManager;

public class MemberDao {
	//paging variables
	public int listCount = 5; 			// 한 페이지에 보여줄 게시물 목록수
	public int pagePerBlock = 3; 		// 한 페이지에 보여줄 페이지 블럭

	private static MemberDao instance = new MemberDao();
	
	private MemberDao() {
	}
	
	public static MemberDao getInstance() {
		return instance;
	}
	
	//페이징을 위해서 한 페이지 출력수 조회
	public int getListCount() {
		return listCount;
	}
	
	//전체 레코드 조회 
	public ArrayList<MemberVo> selectAllMember(int pageNum, String searchText) {
		
		System.out.println("selectAllMember");
		
		ArrayList<MemberVo> memberList = new ArrayList<MemberVo>();

		String sql = "Select mem_code,"
					+ " mem_id, "
					+ " name, "
					+ " email, "
					+ " zip_num, "
					+ " address1||' '||address2 address1, "
					+ " phone, "
					+ " useyn, "
					+ " regdate"
					+ " From member "
					+ " Where name like '%'||?||'%' "
					+ " Order by name asc";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		int absolutePage = 1;

		try {
			conn = DbManager.getConnection();
			absolutePage = (pageNum - 1) * listCount + 1;	//시작게시물
			pstmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
										ResultSet.CONCUR_UPDATABLE);

			if (searchText.equals("")) {
				pstmt.setString(1, "%");
			} else {
				pstmt.setString(1, searchText);
			}
			rs = pstmt.executeQuery();

			System.out.println("이동할 absolutepage : " + absolutePage);
			
			if (rs.next()) {
				rs.absolute(absolutePage);	//읽어 올 시작 게시물로 커서 이동
				int count = 0;
				//이동된 페이지에서 한 페이지에 표시할 게시물 만큼 get	
				while (count < listCount) {
					MemberVo member = new MemberVo();
					member.setMem_code(rs.getInt("mem_code"));
					member.setMem_id(rs.getString("mem_id"));
					member.setName(rs.getString("name"));
					member.setEmail(rs.getString("email"));
					member.setZip_num(rs.getString("zip_num"));
					member.setAddress1(rs.getString("address1"));
					//member.setAddress2(rs.getString("address2"));
					member.setPhone(rs.getString("phone"));
					member.setUseyn(rs.getString("useyn"));
					member.setRegdate(rs.getDate("regdate"));
					memberList.add(member);
					if (rs.isLast()) {
						break;
					}
					rs.next();
					count++;
					System.out.println(member.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return memberList;
	}
	
	//select one member
	public MemberVo selectMemberById(String mem_id) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String sql = "Select"
				+ " mem_code,"
				+ " mem_id,"
				+ " pwd,"
				+ " name,"
				+ " email,"
				+ " phone,"
				+ " zip_num,"
				+ " address1||' '||address2 address,"
				+ " previledge,"
				+ " useyn,"
				+ " regdate"
				+ " From member"
				+ " Where mem_id = ?";
		
		MemberVo member = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, mem_id);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				member = new MemberVo();
				member.setMem_code(rs.getInt("mem_code"));
				member.setMem_id(rs.getString("mem_id"));
				member.setPwd(rs.getString("pwd"));
				member.setName(rs.getString("name"));
				member.setEmail(rs.getString("email"));
				member.setPhone(rs.getString("phone"));
				member.setZip_num(rs.getString("zip_num"));
				member.setAddress1(rs.getString("address"));
				member.setPreviledge(rs.getString("previledge"));
				member.setUseyn(rs.getString("useyn"));
				member.setRegdate(rs.getDate("regdate"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return member;
	}
	
	//check id duplicates 
	public String confirmId(String mem_id) {
		String status = "";
		//String result = "";
		
		String sql = "select "
					+ " mem_id"
					+ " From member "
					+ " Where mem_id=?";

		Connection connn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			connn = DbManager.getConnection();
			pstmt = connn.prepareStatement(sql);
			pstmt.setString(1, mem_id);
			rs = pstmt.executeQuery();
			if (rs.next()) {	
				status = "0";	//duplicate
			} else {
				status = "1";	//not duplicate
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(connn, pstmt, rs);
		}
		
		//JSONObject jsonObj = new JSONObject();
		//jsonObj.put("status", status);
		//result = jsonObj.toString();
		//System.out.println("memberDao result : " + result);
		
		return status;
	}

	//login(id) 
	public MemberVo loginCheck(String mem_id) {
		MemberVo memberVo = new MemberVo();
		//String status = "0";
		
		String sql = "Select mem_id,"
					+ " pwd,"
					+ " name,"
					+ " email,"
					+ " phone,"
					+ " zip_num,"
					+ " address1,"
					+ " address2"
					+ " From member"
					+ " Where mem_id=?";

		System.out.println(" loginCheck sql : " + sql);
		
		Connection connn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			connn = DbManager.getConnection();
			pstmt = connn.prepareStatement(sql);
			pstmt.setString(1, mem_id);
			rs = pstmt.executeQuery();
			if (rs.next()) {	
				memberVo.setMem_id(rs.getString("mem_id"));
				memberVo.setPwd(rs.getString("pwd"));
				memberVo.setName(rs.getString("name"));
				memberVo.setEmail(rs.getString("email"));
				memberVo.setPhone(rs.getString("phone"));
				memberVo.setZip_num(rs.getString("zip_num"));
				memberVo.setAddress1(rs.getString("address1"));
				memberVo.setAddress2(rs.getString("address2"));
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(connn, pstmt, rs);
		}
		
		//JSONObject jsonObj = new JSONObject();
		//jsonObj.put("status", status);
		//result = jsonObj.toString();
		//System.out.println("memberDao result : " + result);
		
		return memberVo;
	}
	
	//회원가입 저장
	public String insertMember(MemberVo memberVo) {
		String result = "0";
		String sql = "Insert Into member("
					+ " mem_code,"
					+ " mem_id,"
					+ " pwd,"
					+ " name,"
					+ " email,"
					+ " phone,"
					+ " zip_num,"
					+ " address1,"
					+ " address2)"
					+ " Values(seq_member_code.nextval, ?, ?, ?, ?, ?, ?, ?, ?)";

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memberVo.getMem_id());
			pstmt.setString(2, memberVo.getPwd());
			pstmt.setString(3, memberVo.getName());
			pstmt.setString(4, memberVo.getEmail());
			pstmt.setString(5, memberVo.getPhone());
			pstmt.setString(6, memberVo.getZip_num());
			pstmt.setString(7, memberVo.getAddress1());
			pstmt.setString(8, memberVo.getAddress2());
			pstmt.executeUpdate();
			result = "1";	//저장성공
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return result;
	}
	
	//페이징을 위한 전체 레코드수(검색어가 있으면 검색어가 포함 전체 레코드)
	public int totalRecord(String searchText) {
		int totalRecord = 0;
		String sql = "Select count(*)"
					+ " From member"
					+ " Where name like '%'||?||'%'";

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet pageset = null;

		try {
			con = DbManager.getConnection();
			pstmt = con.prepareStatement(sql);

			if (searchText.equals("")) {
				pstmt.setString(1, "%");
			} else {
				pstmt.setString(1, searchText);
			}
			pageset = pstmt.executeQuery();

			if (pageset.next()) {
				totalRecord = pageset.getInt(1); // 레코드의 개수
				pageset.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(con, pstmt);
		}
		return totalRecord;
	}
	
	/**
	 * 게시판 하단의 페이지 블럭을 만들어주는 메소드
	 * @param totalRecord	- 총수
	 * @param listCount		- 노출될 목록 게시물 수
	 * @param pagePerBlock	- 노출될 블록 수 [ 1 2 3 4 5 ] [6,7,8,9,10] 5개씩
	 * @param pageNum		- 요청된 페이지 번호
	 * @param searchType	- 검색 항목(미사용)
	 * @param searchText	- 검색어
	 * @return
	 */
	public String getPageNavigator(int totalRecord, 
			//int listCount, 	int pagePerBlock, //클래스 상단에 멤버 변수로 지정
			int pageNum, String searchText) {
		
		StringBuffer sb = new StringBuffer();	
		
		if(totalRecord > 0) {	//총 레코드 수가 하나라도 있어야 페이징
			//총 페이지수
			int totalNumOfPage = (totalRecord % listCount == 0) ? 
					totalRecord / listCount :	totalRecord / listCount + 1;
			
			//페이지 블록수
			int totalNumOfBlock = (totalNumOfPage % pagePerBlock == 0) ? 
					totalNumOfPage / pagePerBlock : totalNumOfPage / pagePerBlock + 1;
			
			//요청된 페이지가 몇 번째 페이지 블럭에 있는지 확인(디테일 뷰로 갔다 돌아왔을 때 기억하고 있어야.)
			int currentBlock = (pageNum % pagePerBlock == 0) ? 
					pageNum / pagePerBlock :
					pageNum / pagePerBlock + 1;
			
			//시작페이지와 끝페이지(한 페이지 블럭 내에서)
			int startPage = (currentBlock - 1) * pagePerBlock + 1;
			int endPage = startPage + pagePerBlock - 1;
			
			if(endPage > totalNumOfPage)
				endPage = totalNumOfPage;
			
			boolean isNext = false;
			boolean isPrev = false;
			
			//현재 페이지 블럭이 토털 페이지 블럭 보다 작을 때만 >> 보여줌(총 페이지 블럭이 4이며 1,2,3 블럭에는 > 표시)
			if(currentBlock < totalNumOfBlock)
				isNext = true;
			//첫 페이지를 제외하고 <이전 버튼 표시
			if(currentBlock > 1)
				isPrev = true;
			//총 페이지 블럭이 1이면 <이전 이후> 표시 안함
			if(totalNumOfBlock == 1){
				isNext = false;
				isPrev = false;
			}
			//페이지 번호가 1보다 크면 모두 <이전 버튼 표시
			if(pageNum > 1){	// 쿼리스트링에서 & = &amp; 
				sb.append("<a href=\"").append("ShoppingServlet?command=admin_member_list&amp;pageNum=1&amp;searchText="+searchText);
				sb.append("\" title=\"<<\"><<</a>&nbsp;");
			}
			//<< 가 있으면
			if (isPrev) {	
				int goPrevPage = startPage - pagePerBlock;			
				sb.append("&nbsp;&nbsp;<a href=\"").append("ShoppingServlet?command=admin_member_list&amp;pageNum="+goPrevPage+"&amp;searchText="+searchText);
				sb.append("\" title=\"<\"><</a>");
			} else {
				
			}
			//페이지 블럭 표시
			for (int i = startPage; i <= endPage; i++) {
				if (i == pageNum) {
					sb.append("<a href=\"#\"><strong>").append(i).append("</strong></a>&nbsp;&nbsp;");
				} else {
					sb.append("<a href=\"").append("ShoppingServlet?command=admin_member_list&amp;pageNum="+i+"&amp;searchText="+searchText);
					sb.append("\" title=\""+i+"\">").append(i).append("</a>&nbsp;&nbsp;");
				}
			}
			//다음 > 버튼 눌렀을 때 이동할 페이지
			if (isNext) {
				//이동할 페이지 = 현재 페이지블럭의 시작페이지 + 페이지당 표시할 블럭수
				int goNextPage = startPage + pagePerBlock;	
	
				sb.append("<a href=\"").append("ShoppingServlet?command=admin_member_list&amp;pageNum="+goNextPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">\">></a>");
			} else {
				
			}
			//요청된 페이지가 전체 페이지수 보다 작으면 다음 > 버튼 표시
			if(totalNumOfPage > pageNum){
				sb.append("&nbsp;&nbsp;<a href=\"").append("ShoppingServlet?command=admin_member_list&amp;pageNum="+totalNumOfPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">>\">>></a>");
			}
		}
		
		return sb.toString();
	}
}
