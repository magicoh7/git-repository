package com.magicoh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.magicoh.dto.CartHeaderViewVo;
import com.magicoh.dto.CartItemViewVo;
import com.magicoh.dto.CartVo;
import com.magicoh.util.DbManager;


public class CartDao {

	private CartDao() {
	}

	private static CartDao instance = new CartDao();

	public static CartDao getInstance() {
		return instance;
	}

	public void insertCart(CartVo cartVo) {
		String sql = "Insert Into cart("
					+ " cart_id,"
					+ " mem_id,"
					+ " product_code,"
					+ " quantity)"
					+ " Values("
					+ " seq_cart_id.nextval, ?, ?, ?)";

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, cartVo.getMem_id());
			pstmt.setInt(2, cartVo.getProduct_code());
			pstmt.setInt(3, cartVo.getQuantity());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
	}

	//장바구니 목록 조회(view_cart_item)
	public ArrayList<CartItemViewVo> selectCartByMemberId(String mem_id) {
		ArrayList<CartItemViewVo> cartItemList = new ArrayList<CartItemViewVo>();
		String sql = "Select "
					+ " cart_id,"    
					+ " member_id,"    
					+ " product_code,"     
					+ " product_name,"  
					+ " list_price,"     
					+ " quantity,"     
					+ " regdate "
					+ " From view_cart_item"
					+ " Where"
					+ " member_id=?"
					+ " Order by cart_id desc";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, mem_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				CartItemViewVo cartItemVo = new CartItemViewVo();
				cartItemVo.setCart_id(rs.getInt("cart_id"));
				cartItemVo.setMember_id(rs.getString("member_id"));
				cartItemVo.setProduct_code(rs.getInt("product_code"));
				cartItemVo.setProduct_name(rs.getString("product_name"));
				cartItemVo.setQuantity(rs.getInt("quantity"));
				cartItemVo.setList_price(rs.getInt("list_price"));
				cartItemVo.setRegdate(rs.getDate("regdate"));
				cartItemList.add(cartItemVo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return cartItemList;
	}
	
	//카트 선택 삭제
	public void deleteCart(int cart_item_id) {
		String sql = "Delete From cart"
					+ " Where cart_id=?";

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, cart_item_id);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
	}
}
