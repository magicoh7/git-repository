package com.magicoh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.magicoh.dto.ProductVo;
import com.magicoh.util.DbManager;

public class ProductDao {
	//paging variables
	public int listCount = 5; 			// 한 페이지에 보여줄 게시물 목록수
	public int pagePerBlock = 3; 		// 한 페이지에 보여줄 페이지 블럭

	private static ProductDao instance = new ProductDao();
	
	private ProductDao() {
	}
	
	public static ProductDao getInstance() {
		return instance;
	}

	public int getListCount() {
		return listCount;
	}
	
	//전체 레코드 조회 - ResultSet TYPE_SCROLL_SENSITIVE 옵션을 통한 조회 
	public ArrayList<ProductVo> selectAll(int pageNum, String searchText) {
		ArrayList<ProductVo> productList = new ArrayList<ProductVo>();

		String sql = "select code, kind, regdate, name, cost_price, list_price, useyn, bestyn "
				+ " from product where name like '%'||?||'%' order by code desc";

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		int absolutePage = 1;

		try {
			con = DbManager.getConnection();
			absolutePage = (pageNum - 1) * listCount + 1;	//시작게시물
			pstmt = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
										ResultSet.CONCUR_UPDATABLE);

			if (searchText.equals("")) {
				pstmt.setString(1, "%");
			} else {
				pstmt.setString(1, searchText);
			}

			rs = pstmt.executeQuery();

			System.out.println("이동할 absolutepage : " + absolutePage);
			
			if (rs.next()) {
				rs.absolute(absolutePage);	//읽어 올 시작 게시물로 커서 이동
				int count = 0;
				//이동된 페이지에서 한 페이지에 표시할 게시물 만큼 get	
				while (count < listCount) {
					ProductVo product = new ProductVo();
					product.setCode(rs.getInt("code"));
					product.setName(rs.getString("name"));
					product.setKind(rs.getString("kind"));
					product.setCost_price(rs.getInt("cost_price"));
					product.setList_price(rs.getInt("list_price"));
					product.setUseyn(rs.getString("useyn"));
					product.setBestyn(rs.getString("bestyn"));
					product.setRegdate(rs.getDate("regdate"));
					productList.add(product);
					if (rs.isLast()) {
						break;
					}
					rs.next();
					count++;
					System.out.println(product.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(con, pstmt, rs);
		}
		return productList;
	}

	//한 개의 상품 조회(productDetail)
	public ProductVo selectProductByCode(String code) {
		
		String sql = "Select code, "
					+ " name,"
					+ " kind,"
					+ " cost_price,"
					+ " list_price," 
					+ " sales_margin,"
					+ " content,"
					+ " image,useyn,"
					+ " bestyn,"
					+ " regdate" 
					+ " From product" 
					+ " Where code =?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		ProductVo product = null;
		
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, Integer.parseInt(code));
			rs = pstmt.executeQuery();
			if(rs.next()) {
				product = new ProductVo();
				product.setCode(rs.getInt("code"));
				product.setName(rs.getString("name"));
				product.setKind(rs.getString("kind"));
				product.setCost_price(rs.getInt("cost_price"));
				product.setList_price(rs.getInt("list_price"));
				product.setSales_margin(rs.getInt("sales_margin"));
				product.setContent(rs.getString("content"));
				product.setImage(rs.getString("image"));
				product.setUseyn(rs.getString("useyn"));
				product.setBestyn(rs.getString("bestyn"));
				product.setRegdate(rs.getDate("regdate"));
				System.out.println(product.toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DbManager.close(conn, pstmt, rs);
		}
		
		return product;
	}
	
	// 신상품
	public ArrayList<ProductVo> selectNewProducts() {
		
		System.out.println("selectNewProducts");
		
		ArrayList<ProductVo> productList = new ArrayList<ProductVo>();
		String sql = "Select "
					+ " code,"
					+ " name,"
					+ " list_price,"
					+ " image"
					+ " From view_new_product";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ProductVo product = new ProductVo();
				product.setCode(rs.getInt("code"));
				product.setName(rs.getString("name"));
				product.setList_price(rs.getInt("list_price"));
				product.setImage(rs.getString("image"));
				productList.add(product);
				System.out.println(product.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return productList;
	}

	// 베스트 상품
	public ArrayList<ProductVo> selectBestProducts() {
		ArrayList<ProductVo> productList = new ArrayList<ProductVo>();
		String sql = "Select "
				+ " code,"
				+ " name,"
				+ " list_price,"
				+ " image"
				+ " From view_best_product";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ProductVo product = new ProductVo();
				product.setCode(rs.getInt("code"));
				product.setName(rs.getString("name"));
				product.setList_price(rs.getInt("list_price"));
				product.setImage(rs.getString("image"));
				productList.add(product);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return productList;
	}	
	
	// 전체 상품
	public ArrayList<ProductVo> selectAllProducts() {
		ArrayList<ProductVo> productList = new ArrayList<ProductVo>();
		String sql = "Select "
				+ " code,"
				+ " name,"
				+ " list_price,"
				+ " image"
				+ " From view_all_product";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ProductVo product = new ProductVo();
				product.setCode(rs.getInt("code"));
				product.setName(rs.getString("name"));
				product.setList_price(rs.getInt("list_price"));
				product.setImage(rs.getString("image"));
				productList.add(product);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return productList;
	}	
	
//	public ProductVO getProduct(String pseq) {
//		ProductVO product = null;
//		String sql = "select * from product where pseq=?";
//
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//			con = DBManager.getConnection();
//			pstmt = con.prepareStatement(sql);
//			pstmt.setString(1, pseq);
//			rs = pstmt.executeQuery();
//			if (rs.next()) {
//				product = new ProductVO();
//				product.setPseq(rs.getInt("pseq"));
//				product.setName(rs.getString("name"));
//				product.setKind(rs.getString("kind"));
//				product.setPrice1(rs.getInt("price1"));
//				product.setPrice2(rs.getInt("price2"));
//				product.setPrice3(rs.getInt("price3"));
//				product.setContent(rs.getString("content"));
//				product.setImage(rs.getString("image"));
//				product.setUseyn(rs.getString("useyn"));
//				product.setBestyn(rs.getString("bestyn"));
//				product.setIndate(rs.getTimestamp("indate"));
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			DBManager.close(con, pstmt, rs);
//		}
//		return product;
//	}
	
	
	
	//상품 카테고리 조회
	public ArrayList<ProductVo> listKindProduct(String kind) {
		ArrayList<ProductVo> productList = new ArrayList<ProductVo>();
		String sql = "Select "
					+ " code,"
					+ " name,"
					+ " list_price,"
					+ " image"
					+ " From product "
					+ " Where kind=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, kind);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				ProductVo product = new ProductVo();
				product.setCode(rs.getInt("code"));
				product.setName(rs.getString("name"));
				product.setList_price(rs.getInt("list_price"));
				product.setImage(rs.getString("image"));
				productList.add(product);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return productList;
	}
	
	//전체 레코드 수(검색어가 있으면 검색어가 포함된 전체 레코드만)
	public int totalRecord(String searchText) {
		int totalRecords = 0;
		String sql = "select count(*) from product where name like '%'||?||'%'";

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet pageset = null;

		try {
			con = DbManager.getConnection();
			pstmt = con.prepareStatement(sql);

			if (searchText.equals("")) {
				pstmt.setString(1, "%");
			} else {
				pstmt.setString(1, searchText);
			}
			pageset = pstmt.executeQuery();

			if (pageset.next()) {
				totalRecords = pageset.getInt(1); // 레코드의 개수
				pageset.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(con, pstmt);
		}
		return totalRecords;
	}

	//상품 등록
	public int InsertProduct(ProductVo product) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = 0;
		
		String sql = "Insert Into product(" +
						" code," +
						" name," +
						" kind," +
						" cost_price," +
						" list_price," +
						" sales_margin," +
						" content," +
						" image," +
						" regdate)" +
						" Values(product_code_seq.nextval, ?, ?, ?, ?, ?, ?, ?, sysdate)";
		
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, product.getName());
			pstmt.setString(2, product.getKind());
			pstmt.setInt(3, product.getCost_price());
			pstmt.setInt(4, product.getList_price());
			pstmt.setInt(5, product.getSales_margin());
			pstmt.setString(6, product.getContent());
			pstmt.setString(7, product.getImage());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return result;
	}
	
	//상품 업데이트
	public int updateProduct(ProductVo product) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = 0;
		
		String sql = "Update product Set " +
						" name = ?," +
						" kind = ?," +
						" cost_price = ?," +
						" list_price = ?," +
						" sales_margin = ?," +
						" content = ?," +
						" image = ?," +
						" useyn = ?," +
						" bestyn = ?" +
						" Where code = ?";
		
		
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, product.getName());
			pstmt.setString(2, product.getKind());
			pstmt.setInt(3, product.getCost_price());
			pstmt.setInt(4, product.getList_price());
			pstmt.setInt(5, product.getSales_margin());
			pstmt.setString(6, product.getContent());
			pstmt.setString(7, product.getImage());
			pstmt.setString(8, product.getUseyn());
			pstmt.setString(9, product.getBestyn());
			pstmt.setInt(10, product.getCode());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return result;
	}
	
	
	/**
	 * 게시판 하단의 페이지 블럭을 만들어주는 함수
	 * @param totalRecord	- 총수
	 * @param listCount		- 노출될 목록 게시물 수
	 * @param pagePerBlock	- 노출될 블록 수 [ 1 2 3 4 5 ] [6,7,8,9,10] 5개씩
	 * @param pageNum		- 요청된 페이지 번호
	 * @param searchType	- 검색 항목(미사용)
	 * @param searchText	- 검색어
	 * @return
	 */
	public String getPageNavigator(int totalRecord, 
			//int listCount, 	int pagePerBlock, 
			int pageNum, String searchText) {
		
		StringBuffer sb = new StringBuffer();	
		
		if(totalRecord > 0) {	//총 레코드 수가 하나라도 있어야 페이징
			//총 페이지수
			int totalNumOfPage = (totalRecord % listCount == 0) ? 
					totalRecord / listCount :	totalRecord / listCount + 1;
			
			//페이지 블록수
			int totalNumOfBlock = (totalNumOfPage % pagePerBlock == 0) ? 
					totalNumOfPage / pagePerBlock : totalNumOfPage / pagePerBlock + 1;
			
			//요청된 페이지가 몇 번째 페이지 블럭에 있는지 확인(디테일 뷰로 갔다 돌아왔을 때 기억하고 있어야.)
			int currentBlock = (pageNum % pagePerBlock == 0) ? 
					pageNum / pagePerBlock :
					pageNum / pagePerBlock + 1;
			
			//시작페이지와 끝페이지(한 페이지 블럭 내에서)
			int startPage = (currentBlock - 1) * pagePerBlock + 1;
			int endPage = startPage + pagePerBlock - 1;
			
			if(endPage > totalNumOfPage)
				endPage = totalNumOfPage;
			
			boolean isNext = false;
			boolean isPrev = false;
			
			//현재 페이지 블럭이 토털 페이지 블럭 보다 작을 때만 >> 보여줌(총 페이지 블럭이 4이며 1,2,3 블럭에는 > 표시)
			if(currentBlock < totalNumOfBlock)
				isNext = true;
			//첫 페이지를 제외하고 <이전 버튼 표시
			if(currentBlock > 1)
				isPrev = true;
			//총 페이지 블럭이 1이면 <이전 이후> 표시 안함
			if(totalNumOfBlock == 1){
				isNext = false;
				isPrev = false;
			}
			//페이지 번호가 1보다 크면 모두 <이전 버튼 표시
			if(pageNum > 1){	// 쿼리스트링에서 & = &amp; 
				sb.append("<a href=\"").append("ShoppingServlet?command=admin_product_list&amp;pageNum=1&amp;searchText="+searchText);
				sb.append("\" title=\"<<\"><<</a>&nbsp;");
			}
			//<< 가 있으면
			if (isPrev) {	
				int goPrevPage = startPage - pagePerBlock;			
				sb.append("&nbsp;&nbsp;<a href=\"").append("ShoppingServlet?command=admin_product_list&amp;pageNum="+goPrevPage+"&amp;searchText="+searchText);
				sb.append("\" title=\"<\"><</a>");
			} else {
				
			}
			//페이지 블럭 표시
			for (int i = startPage; i <= endPage; i++) {
				if (i == pageNum) {
					sb.append("<a href=\"#\"><strong>").append(i).append("</strong></a>&nbsp;&nbsp;");
				} else {
					sb.append("<a href=\"").append("ShoppingServlet?command=admin_product_list&amp;pageNum="+i+"&amp;searchText="+searchText);
					sb.append("\" title=\""+i+"\">").append(i).append("</a>&nbsp;&nbsp;");
				}
			}
			//다음 > 버튼 눌렀을 때 이동할 페이지
			if (isNext) {
				//이동할 페이지 = 현재 페이지블럭의 시작페이지 + 페이지당 표시할 블럭수
				int goNextPage = startPage + pagePerBlock;	
	
				sb.append("<a href=\"").append("ShoppingServlet?command=admin_product_list&amp;pageNum="+goNextPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">\">></a>");
			} else {
				
			}
			//요청된 페이지가 전체 페이지수 보다 작으면 다음 > 버튼 표시
			if(totalNumOfPage > pageNum){
				sb.append("&nbsp;&nbsp;<a href=\"").append("ShoppingServlet?command=admin_product_list&amp;pageNum="+totalNumOfPage+"&amp;searchText="+searchText);
				sb.append("\" title=\">>\">>></a>");
			}
		}
		
		return sb.toString();
	}

	// [미사용]페이지 이동을 위한 메소드
//	public String pageNumber(int tpage, String searchText) {
//		String str = "";
//
//		int totalRecords = totalRecord(searchText);
//		int page_count = totalRecords / pagePerBlock + 1;
//
//		if (totalRecords % pagePerBlock == 0) {
//			page_count--;
//		}
//		if (tpage < 1) {
//			tpage = 1;
//		}
//
//		int start_page = tpage - (tpage % listCount) + 1;	//시작페이지
//		int end_page = start_page + (pagePerBlock - 1);			//종료페이지
//
//		if (end_page > page_count) {
//			end_page = page_count;
//		}
//		if (start_page > listCount) {
//			str += "<a href='ShoppingServlet?command=admin_product_list&pageNum=1&searchText=" + searchText
//					+ "'>&lt;&lt;</a>&nbsp;&nbsp;";
//			str += "<a href='ShoppingServlet?command=admin_product_list&pageNum=" + (start_page - 1);
//			str += "&searchText=<%=product_name%>'>&lt;</a>&nbsp;&nbsp;";
//		}
//
//		for (int i = start_page; i <= end_page; i++) {
//			if (i == tpage) {
//				str += "<font color=red>[" + i + "]&nbsp;&nbsp;</font>";
//			} else {
//				str += "<a href='ShoppingServlet?command=admin_product_list&pageNum=" + i + "&searchText=" + searchText + "'>[" + i
//						+ "]</a>&nbsp;&nbsp;";
//			}
//		}
//
//		if (page_count > end_page) {
//			str += "<a href='ShoppingServlet?command=admin_product_list&pageNum=" + (end_page + 1) + "&searchText=" + searchText
//					+ "'> &gt; </a>&nbsp;&nbsp;";
//			str += "<a href='ShoppingServlet?command=admin_product_list&pageNum=" + page_count + "&searchText=" + searchText
//					+ "'> &gt; &gt; </a>&nbsp;&nbsp;";
//		}
//		return str;
//	}	
	
}
