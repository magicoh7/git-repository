package com.magicoh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.magicoh.dto.WorkerVo;
import com.magicoh.util.DbManager;

public class AdminDao {

	private static AdminDao instance = new AdminDao();
	
	private AdminDao() {
	}
	
	public static AdminDao getInstance() {
		return instance;
	}
	
	public WorkerVo checkAdmin(String id, String pwd) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		WorkerVo worker = null;
		
		String sql = "Select id, name, pwd "
					+ " From worker "
					+ " Where id = ?"
					+ " And pwd = ?";
		conn = DbManager.getConnection();
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, pwd);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				worker = new WorkerVo();
				worker.setId(rs.getString("id"));
				worker.setName(rs.getString("name"));
				System.out.println(worker.toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		
		return worker;
	}
	
}
