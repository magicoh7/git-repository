package com.magicoh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.magicoh.dto.CartHeaderViewVo;
import com.magicoh.dto.CartItemViewVo;
import com.magicoh.dto.CartVo;
import com.magicoh.dto.OrderItemMypageViewVo;
import com.magicoh.dto.OrderItemViewVo;
import com.magicoh.dto.OrderVo;
import com.magicoh.util.DbManager;

public class OrderDao {

	private OrderDao() {
	}

	private static OrderDao instance = new OrderDao();

	public static OrderDao getInstance() {
		return instance;
	}

	//장바구니 목록에서 주문처리시 카트 헤더 조회 => 주문 헤더에 저장하기 위해
	public CartHeaderViewVo selectCartByMemberId(String member_id) {
		CartHeaderViewVo cartHeader = null;
		
		String sql = "Select "
					+ " member_id,"     
					+ " member_name,"     
					+ " grand_total,"     
					+ " phone,"     
					+ " email,"     
					+ " zip_num,"     
					+ " address1,"     
					+ " address2,"     
					+ " regdate "
					+ " From view_cart_header"
					+ " Where member_id=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, member_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				cartHeader = new CartHeaderViewVo();
				cartHeader.setMember_id(rs.getString("member_id"));
				cartHeader.setMember_name(rs.getString("member_name"));
				cartHeader.setGrand_total(rs.getInt("grand_total"));
				cartHeader.setPhone(rs.getString("phone"));
				cartHeader.setEmail(rs.getString("email"));
				cartHeader.setZip_num(rs.getString("zip_num"));
				cartHeader.setAddress1(rs.getString("address1"));
				cartHeader.setAddress2(rs.getString("address2"));
				cartHeader.setRegdate(rs.getDate("regdate"));
				
				System.out.println(cartHeader.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return cartHeader;
	}		
	
	
	//장바구니 목록에서 주문시 선택된 cart_item조회(OrderInsertAction)
	public CartItemViewVo selectCartItemByMemberId(String cart_id) {
		CartItemViewVo cartItemVo = null; 
		String sql = "Select "
					+ " cart_id,"    
					+ " member_id,"    
					+ " product_code,"     
					+ " product_name,"  
					+ " list_price,"     
					+ " quantity,"     
					+ " regdate "
					+ " From view_cart_item"
					+ " Where"
					+ " cart_id=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, Integer.parseInt(cart_id));
			rs = pstmt.executeQuery();
			while (rs.next()) {
				cartItemVo = new CartItemViewVo();
				cartItemVo.setCart_id(rs.getInt("cart_id"));
				cartItemVo.setMember_id(rs.getString("member_id"));
				cartItemVo.setProduct_code(rs.getInt("product_code"));
				cartItemVo.setProduct_name(rs.getString("product_name"));
				cartItemVo.setList_price(rs.getInt("list_price"));
				cartItemVo.setQuantity(rs.getInt("quantity"));
				cartItemVo.setRegdate(rs.getDate("regdate"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return cartItemVo;
	}	
	

	
	
	// 사용자 주문 저장(순서 : 주문헤더 저장 -> 주문Item 저장 -> 장바구니 삭제)
	// 파라미터 : ArrayList<CartVo> : 장바구니 리스트
	public int insertOrder(ArrayList<CartItemViewVo> cartList, CartHeaderViewVo cartHeader) {
		int max_order_id = 0;
		int result = 0; //저장 작업 처리 결과(0 - 실패 / 1 - 성공)
		String sql = null;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs;

		try {
			conn = DbManager.getConnection();

			sql = "Select max(order_id) max_order_id"
				+ " From orders";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				max_order_id = rs.getInt("max_order_id");
				System.out.println("주문 헤더 테이블의 Max order id : " + max_order_id);
			}
			pstmt.close();
			sql = "";
			
			System.out.println("휴대폰 번호 : " + cartHeader.getPhone());
			
			//주문 헤더(orders) 저장
			sql = "Insert Into orders("
				+ " order_id,"		
				+ " mem_id,"		
				+ " status,"
				+ " grand_total,"
				+ " name,"
				+ " phone,"
				+ " email,"
				+ " zip_num,"
				+ " address1,"
				+ " address2)"
				+ " Values("
				+ " seq_order_id.nextval,"
				+ " ?, '0', ?, ?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, cartHeader.getMember_id());
			pstmt.setInt(2, cartHeader.getGrand_total());
			pstmt.setString(3, cartHeader.getMember_name());
			pstmt.setString(4, cartHeader.getPhone());
			pstmt.setString(5, cartHeader.getEmail());
			pstmt.setString(6, cartHeader.getZip_num());
			pstmt.setString(7, cartHeader.getAddress1());
			pstmt.setString(8, cartHeader.getAddress2());
			pstmt.executeUpdate();

			//주문Detail(item)에 저장
			for (CartItemViewVo cartVO : cartList) {
				insertOrderItem(cartVO, max_order_id);
			}
			result = 0;
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
		return max_order_id + 1;
	}

	/**
	 * 주문 Detail(Item)테이블에 저장
	 * 파라미터 cartVo : 화면에서 전달받은 장바구니 상품
	 * 		  max_order_id + 1 : 주문 헤더와 연결
	 * **/
	
	public void insertOrderItem(CartItemViewVo cartVo, int max_order_id) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DbManager.getConnection();

			System.out.println(cartVo.toString());
			
			System.out.println("insertOrderItem orders order_id : " + (max_order_id + 1));
			
			String sql = "Insert Into order_item("
					+ " order_item_id, "
					+ " order_id, "
					+ " code,"
					+ " name,"
					+ " quantity,"
					+ " unit_price,"
					+ " status,"
					+ " regdate)"
					+ " Values("
					+ "seq_order_item_id.nextval, "
					+ "?, ?, ?, ?, ?, '0', sysdate)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, (max_order_id + 1));	//orders 테이블과 갖게하도록
			pstmt.setInt(2, cartVo.getProduct_code());
			pstmt.setString(3, cartVo.getProduct_name());
			pstmt.setInt(4, cartVo.getQuantity());
			pstmt.setInt(5, cartVo.getList_price());
			pstmt.executeUpdate();
			pstmt.close();

			//주문완료된 장바구니 삭제 처리
			sql = "";
			sql = "Delete From cart "
					+ "Where cart_id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, cartVo.getCart_id());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
	}

	
	/**
	 * 사용자가 관리자페이지 클릭시 나오는 화면(바로 자신의 주문내역이 보여짐)
	 * 파라미터 : mem_id(사용자id) status(주문 상태 0-입금대기,1-결제완료,2-배송준비중,3-배송중,4-배송완료)
	 * **/
	public ArrayList<OrderItemMypageViewVo> selectOrderItemByMemberId(String mem_id) {
		ArrayList<OrderItemMypageViewVo> orderItemList = new ArrayList<OrderItemMypageViewVo>();
		String sql = "Select *"
					+ " From view_order_item_mypage"
					+ " Where mem_id=?"
					+ " Order by order_id desc, order_item_id asc";	
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, mem_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				OrderItemMypageViewVo orderItem = new OrderItemMypageViewVo();
				orderItem.setOrder_id(rs.getInt("order_id"));
				orderItem.setOrder_item_id(rs.getInt("order_item_id"));
				orderItem.setStatus(rs.getString("status"));				
				orderItem.setMem_id(rs.getString("mem_id"));
				orderItem.setMember_name(rs.getString("member_name"));
				orderItem.setGrand_total(rs.getInt("grand_total"));
				orderItem.setProduct_code(rs.getInt("product_code"));
				orderItem.setProduct_name(rs.getString("product_name"));
				orderItem.setQuantity(rs.getInt("quantity"));
				orderItem.setUnit_price(rs.getInt("unit_price"));
				orderItem.setAmt_per_product(rs.getInt("amt_per_product"));
				orderItem.setZip_num(rs.getString("zip_num"));
				orderItem.setAddress(rs.getString("address"));
				orderItem.setPhone(rs.getString("phone"));
				orderItem.setRegdate(rs.getDate("regdate"));
				orderItemList.add(orderItem);
				
				System.out.println("주문목록조회 selectOrderById 메소드 : " + orderItem.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return orderItemList;
	}	
	
	
	
	/**
	 * 사용자가 주문후 보여주는 주문 내역
	 * 파라미터 : mem_id(사용자id) order_id(주문번호)
	 * **/
	public ArrayList<OrderItemViewVo> selectOrderById(String mem_id, int order_id) {
		ArrayList<OrderItemViewVo> orderList = new ArrayList<OrderItemViewVo>();
		String sql = "Select *"
					+ " From view_order_item"
					+ " Where order_id=?"
					+ " And mem_id=?"
					+ " Order by order_id desc, order_item_id asc";	

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, order_id);
			pstmt.setString(2, mem_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				OrderItemViewVo orderView = new OrderItemViewVo();
				orderView.setOrder_item_id(rs.getInt("order_item_id"));
				orderView.setOrder_id(rs.getInt("order_id"));
				orderView.setStatus(rs.getString("status"));				
				orderView.setMem_id(rs.getString("mem_id"));
				orderView.setMember_name(rs.getString("member_name"));
				orderView.setProduct_code(rs.getInt("product_code"));
				orderView.setProduct_name(rs.getString("product_name"));
				orderView.setQuantity(rs.getInt("quantity"));
				orderView.setUnit_price(rs.getInt("unit_price"));
				orderView.setZip_num(rs.getString("zip_num"));
				orderView.setAddress(rs.getString("address"));
				orderView.setPhone(rs.getString("phone"));
				orderView.setRegdate(rs.getDate("regdate"));
				orderList.add(orderView);
				
				System.out.println("주문목록조회 selectOrderById 메소드 : " + orderView.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return orderList;
	}

	// 주문 내역 조회(주문 상태 0-입금대기,1-결제완료,2-배송준비중,3-배송중,4-배송완료)
	public ArrayList<Integer> selectSeqOrderIng(String id) {
		ArrayList<Integer> oseqList = new ArrayList<Integer>();
		String sql = "Select distinct oseq "
					+ " From order_view" 
					+ " Where id=? And result='1' "
					+ " Order by oseq desc";
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = DbManager.getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				oseqList.add(rs.getInt(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(con, pstmt, rs);
		}
		return oseqList;
	}

	/**************** 관리자 모드에서 사용되는 메소드 *************************/
	
	// 관리자 주문 조회 Order & Order Item을 Join해서 만든 뷰를 통해서 select
	// 파라미터 : searchText - 주문자명
	public ArrayList<OrderItemViewVo> selectOrderItemList(String searchText) {
		ArrayList<OrderItemViewVo> orderList = new ArrayList<OrderItemViewVo>();
		String sql = "select *"
				+ " from view_order_item"
				+ " where member_name like '%'||?||'%'"
				+ " order by status asc, order_id desc, order_item_id asc";	
				//정렬순서 : 미처리 item 우선, 최근 주문 우선

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			if (searchText == "") {
				pstmt.setString(1, "%");
			} else {
				pstmt.setString(1, searchText);
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				OrderItemViewVo orderView = new OrderItemViewVo();
				orderView.setOrder_item_id(rs.getInt("order_item_id"));
				orderView.setOrder_id(rs.getInt("order_id"));
				orderView.setStatus(rs.getString("status"));				
				orderView.setMem_id(rs.getString("mem_id"));
				orderView.setMember_name(rs.getString("member_name"));
				orderView.setProduct_code(rs.getInt("product_code"));
				orderView.setProduct_name(rs.getString("product_name"));
				orderView.setQuantity(rs.getInt("quantity"));
				orderView.setUnit_price(rs.getInt("unit_price"));
				orderView.setZip_num(rs.getString("zip_num"));
				orderView.setAddress(rs.getString("address"));
				orderView.setPhone(rs.getString("phone"));
				orderView.setRegdate(rs.getDate("regdate"));
				orderList.add(orderView);
				System.out.println(orderView.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt, rs);
		}
		return orderList;
	}
	
	//주문테이블의 처리상태 업데이트(0-입금대기 / 1-결제완료)
	public void updateOrderStatus(String order_item_id) {
		String sql = "Update order_item"
					+ " Set"
					+ " status='1'"
					+ " Where order_item_id=?";

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, Integer.parseInt(order_item_id));
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbManager.close(conn, pstmt);
		}
	}
}
