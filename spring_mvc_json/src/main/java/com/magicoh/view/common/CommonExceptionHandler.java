package com.magicoh.view.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

//"com.magicoh.view"로 시작하는 컨트롤러에서 예외가 발생하면 내가 처리하겠다.
@ControllerAdvice("com.magicoh.view")
public class CommonExceptionHandler {

	public CommonExceptionHandler() {
	}
	
	//ArithmeticException 예외 발생시 처리하는 메소드
	@ExceptionHandler(ArithmeticException.class)
	public ModelAndView handleArithmeticException(Exception e){
		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", e);
		mav.setViewName("/common/arithmeticError.jsp");
		return mav;
	}

	//NullPointerException 예외 발생시 처리하는 메소드
	@ExceptionHandler(NullPointerException.class)
	public ModelAndView handleNullPointerException(Exception e){
		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", e);
		mav.setViewName("/common/arithmeticError.jsp");
		return mav;
	}
	
	//Exception 예외 발생시 처리하는 메소드
	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception e){
		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", e);
		mav.setViewName("/common/arithmeticError.jsp");
		return mav;
	}
	
}
