package com.magicoh.view.board;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.magicoh.biz.board.BoardService;
import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.impl.BoardDAO;
import com.magicoh.biz.user.UserVo;

@Controller
@SessionAttributes("board")
@RequestMapping(value="/bbs")
public class BoardController {

	@Autowired
	private BoardService boardService;
	

	
	@RequestMapping("/getBoardList.do")
	public String getBoardList(BoardVO vo, Model model){
		
		if(vo.getSearchCondition() == null){
			vo.setSearchCondition("TITLE");
		}
		if(vo.getSearchKeyword() ==  null){
			vo.setSearchKeyword("");
		}
		List<BoardVO> boardList = boardService.getBoardList(vo);
		model.addAttribute("boardList", boardList);	//boardList가 2차로  Model 객체에 실려서 JSP로 전달된다.
		
		return "/board/getBoardList" ;
	}
	
	@RequestMapping("/getBoard.do")
	public String getBoard(BoardVO vo, Model model){
		BoardVO board = boardService.getBoard(vo);
		model.addAttribute("board", board);
		return "/board/getBoard";
	}
	
	//insertForm
	@RequestMapping(value="/insertBoard.do", method=RequestMethod.GET)
	public String insertBoardForm(HttpSession session, Model model){
		
		UserVo loginUser = (UserVo)session.getAttribute("loginUser");	
		if (loginUser == null) {
			return "/user/login";
		} else{
			return "/board/insertBoardForm";
		}
	}
	
	@RequestMapping("/insertBoard.do")
	public String insertBoard(BoardVO vo) throws IOException{
		//File upload to server(copy)
		MultipartFile uploadFile = vo.getUploadFile();
		if(!uploadFile.isEmpty()){
			String fileName = uploadFile.getOriginalFilename();
			uploadFile.transferTo(new File("c:/fileUpload/" + fileName));
			System.out.println("file upload succefully completed!!");
		}
		boardService.insetBoard(vo);
		
		return "redirect:getBoardList.do";	//"redirect:/sales/getBoardList.do"
	}
	
	//updateForm
	@RequestMapping(value="/updateBoard.do", method=RequestMethod.GET)
	public String updateBoardForm(BoardVO vo, Model model){
		BoardVO board = boardService.getBoard(vo);
		model.addAttribute("board", board);
		return "/board/updateBoardForm";
	}
	
	//update
	@RequestMapping(value="/updateBoard.do", method=RequestMethod.POST)
	public String updateBoard(BoardVO vo){
		boardService.updateBoard(vo);
		return "redirect:getBoardList.do";
	}
	
	@RequestMapping("/deleteBoard.do")
	public String deleteBoard(BoardVO vo, BoardDAO boardDAO){
		boardDAO.deleteBoard(vo);
		return "redirect:getBoardList.do";

	}
	
	/**
	 * 조회 화면의 검색 조건을 세팅해주는 메소드로 화면에서 만들지 않고
	 * DB에서 불러서 세팅해줄 때 유용함. 이 메소드의 실행이 끝나면
	 * 아래에 있는 getBoardList()가 실행된다.
	 * **/
	@ModelAttribute("/conditionMap")
	public Map<String, String> searchConditionMap(){
		Map<String, String> conditionMap = new HashMap<String, String>();
		conditionMap.put("제목", "TITLE");
		conditionMap.put("내용", "CONTENT");
		//보낸 내용(conditionMap)이 Model 객체에 실린다. 그 다음에  getBoardList()메소드 실행하러 간다.
		return conditionMap;	
	}

	@RequestMapping("/dataTransform.do")
	@ResponseBody
	public List<BoardVO> dataTransform(BoardVO vo){
		
		if(vo.getSearchCondition() == null){
			vo.setSearchCondition("TITLE");
		}
		if(vo.getSearchKeyword() ==  null){
			vo.setSearchKeyword("");
		}
		List<BoardVO> boardList = boardService.getBoardList(vo);
		
		return boardList ;
	}	
}
