package com.magicoh.view.user;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.magicoh.biz.user.UserVo;
import com.magicoh.biz.user.impl.UserDao;


@Controller
@RequestMapping(value="/bbs")
public class LoginController  {
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String loginView(UserVo vo, Model model) {	//로그인 정보 입력 화면
		System.out.println("로그인폼 화면");
		
		vo.setId("magic");
		vo.setPassword("1234");
		
		return "/user/login";		
	}

	@RequestMapping(value="login.do", method=RequestMethod.POST)
	public String login(UserVo vo, UserDao userDAO, HttpSession session) {	
		System.out.println("로그인 인증 처리");
		
		if (vo.getId().equals("") || vo.getId() == null){
			throw new IllegalArgumentException("아이디를 입력하세요.");
		}
		
		UserVo user = userDAO.getUser(vo); 
		if(user != null){	
			session.setAttribute("loginUser", user);
			return "redirect:getBoardList.do";	
		}else{
			return "/user/login";		
		}
	}

	@RequestMapping(value="logout.do")
	public String logout(HttpSession session) {
		System.out.println("로그아웃 처리");
		session.invalidate();
		return "/user/login";
	}
}
