package com.magicoh.biz.board.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magicoh.biz.board.BoardService;
import com.magicoh.biz.board.BoardVO;

@Service("boardService")
public class BoardServiceImpl implements BoardService {
	
	@Autowired
	private BoardDAOMybatis boardDAO;
	//private BoardDAOSpring boardDAO;
	
	public static int countNo = 0;
	
	public BoardServiceImpl() {
	}
	
	@Override	//포인트컷(트랜잭션 걸린 상태)
	public void insetBoard(BoardVO vo) {
		//공통관심사 코드가 와야될 위치
		this.boardDAO.insertBoard(vo);
		//this.boardDAO.insertBoard(vo);
	}
	@Override
	public List<BoardVO> getBoardList(BoardVO vo) {
		return boardDAO.getBoardList(vo);
	}
	@Override
	public void updateBoard(BoardVO vo) {
		System.out.println("==> public void updateBoard(BoardVO vo) {");
		boardDAO.updateBoard(vo);
		countNo++;
		System.out.println("=======> "+countNo);
	}

	@Override
	public void deleteBoard(BoardVO vo) {
		boardDAO.deleteBoard(vo);
		
	}

	@Override
	public BoardVO getBoard(BoardVO vo) {
		return boardDAO.getBoard(vo);
	}



	
}
