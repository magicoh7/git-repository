package com.magicoh.biz.board.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.magicoh.biz.board.BoardVO;

//@Repository
public class BoardDAOSpring{

	private final String BOARD_INSERT = "insert into board_spring (seq, title, writer, content) values ((select nvl(max(seq),0)+1 from board_spring),?,?,?)";
	private final String BOARD_UPDATE = "update board_spring set title=?, content=? where seq=?";
	private final String BOARD_DELETE = "delete from board_spring where seq=?";
	private final String BOARD_GET = "select * from board_spring where seq=?";
	private final String BOARD_LIST = "select * from board_spring order by seq desc";

	private final String BOARD_LIST_T = "select * from board_spring where title like '%'||?||'%' order by seq desc";
	private final String BOARD_LIST_C = "select * from board_spring where content like '%'||?||'%' order by seq desc";

	private final String BOARD_INSERT_TRANSACTION = "insert into board_spring (seq, title, writer, content) values (?,?,?,?)";

	@Autowired
	private JdbcTemplate jdbcTemplate; 	//applicationContext.xml에 jdbcTemplate Bean 설정
	
	public void insertBoard(BoardVO vo) {
		System.out.println("===>Spring JDBC로 insertBoard() 기능처리");
		jdbcTemplate.update(BOARD_INSERT, vo.getTitle(), vo.getContent(), vo.getSeq());
		//jdbcTemplate.update(BOARD_INSERT_TRANSACTION, vo.getSeq(), vo.getTitle(), vo.getWriter(), vo.getContent() );
	}// insertBoard() END

	// 글목록 조회
	public List<BoardVO> getBoardList(BoardVO vo) {

		System.out.println("===>Spring JDBC로  getBoardList() 기능 처리");
		return jdbcTemplate.query(BOARD_LIST, new BoardRowMapper());
	}// getBoardList() END	
	// 글 수정
	public void updateBoard(BoardVO vo) {
		System.out.println("===>Spring JDBC로 updateBoard() 기능 처리");
		jdbcTemplate.update(BOARD_UPDATE, vo.getTitle(), vo.getContent(), vo.getSeq());
	}// updateBoard() END

	// 글 삭제
	public void deleteBoard(BoardVO vo) {
		System.out.println("===>Spring JDBC로 deleteBoard() 기능 처리");
		jdbcTemplate.update(BOARD_DELETE, vo.getSeq());
	}// deleteBoard() END
	
	// 글 상세 조회
	public BoardVO getBoard(BoardVO vo) {
		System.out.println("===>Spring JDBC로 getBoard() 기능 처리");
		Object[] args = {vo.getSeq()};
		return jdbcTemplate.queryForObject(BOARD_GET, args, new BoardRowMapper());
	}// getBoard() END
	

}
