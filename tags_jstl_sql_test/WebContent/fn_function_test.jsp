<%@ page language="java" contentType="text/html; charset=UTF-8"     pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>fn function 사용 예제</title>
</head>
<body>
	<c:set var="greeting" value="C'est va bien"></c:set>
	Original String : ${greeting }
	
	<br /> All Capitial Letter : ${fn:toUpperCase(greeting) }

	<br /> All Lower case : ${fn:toLowerCase(greeting) }

	<br /> location of 'va' : ${fn:indexOf(greeting, "va") }

	<br /> length of String : ${fn:length(greeting) }
	
	<br />
	
	
	
</body>
</html>