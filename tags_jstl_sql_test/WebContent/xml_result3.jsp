<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%--단순히 xml문서를 text 형태로 읽어들이는 역할  --%>
	<c:import charEncoding="UTF-8" url="namecard.xml" var="xmldata" />
	<%--위에서 읽어들인 xm을 실제로 파싱해주는 역할  --%>
	<x:parse xml="${xmldata }" var="xdata" />
	
	<x:out select="$xdata//person[1]/name" />
	<x:out select="$xdata//person[last()]/name"
	 />
	<hr>
	
	<table border="1">
		<x:forEach select="$xdata//person">
			<tr>
				<td> <x:out select="email" /></td>
				<td> <x:out select="phone" /></td>
			</tr>
		</x:forEach>
	</table>
</body>
</html>