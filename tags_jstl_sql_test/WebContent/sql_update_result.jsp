<%@ page language="java" contentType="text/html; charset=UTF-8"     pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSTL_SQL</title>
</head>
<body>
	<sql:setDataSource  url="jdbc:oracle:thin:@localhost:1521:XE"
						driver="oracle.jdbc.driver.OracleDriver"
						user="boardsearch"
						password="1234"
						scope="application" 
						var="dataSource" />
						
	<fmt:setLocale value="ko_kr" />
	
	<!-- 조회 -->
	<sql:query var="emp" dataSource="${dataSource}">
	
		SELECT EMPNO AS 사원번호, ENAME AS 이름,
		SAL AS 월급여, HIREDATE AS 입사일
		FROM employee_tbl
								
	</sql:query>

	<!-- 수정 -->
	<sql:update var="result" dataSource="${dataSource}">
	
		UPDATE EMPLOYEE_TBL SET ENAME=? WHERE EMPNO=?
		
		<sql:param value="test" />
		<sql:param value="1" />
		
	</sql:update>

	<!-- 저장 -->
	<sql:update var="result" dataSource="${dataSource}">
	
		INSERT INTO EMPLOYEE_TBL(EMPNO, ENAME, SAL, HIREDATE ) VALUES(employee_seq.nextval, ?, ?, SYSDATE)
		
		<sql:param value="홍길동" />
		<sql:param value="5000" />
		
	</sql:update>

	<table border="1">
		<tr>
			<c:forEach var="columnName"	items="${emp.columnNames }">
				<th><c:out value="${columnName }" /></th>
			</c:forEach>
			
			<c:forEach var="row" items="${emp.rowsByIndex }">	<!-- 한개의 row씩 추출 0번째 부터 -->
				<tr>
					<c:forEach var="column" items="${row }" varStatus="i">	<!-- i = 0부터 -->
					<c:choose>
						<c:when test="${i.index==3 }">	<!-- 3번째 컬럼(HireDate) -->
							<td><fmt:formatDate value="${column }" pattern="yyyy/MM/dd" /></td>
						</c:when>	
						<c:otherwise>
							<td><c:out value="${column }" /></td>
						</c:otherwise>
					</c:choose>
					</c:forEach>
				</tr>
			</c:forEach>
		</tr>
	</table>

	<table border="1">
		<c:forEach var="row" items="${emp.rows }">	<!-- rows 메소드는 컬럼명 - Map(key name / 데이터 - value) 형태로 반환 -->
			<tr>
				<td>번호 : <c:out value="${row['사원번호'] }"/></td>
				<td>이름 : <c:out value="${row['이름'] }"/></td>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>