<%@ page language="java" contentType="text/html; charset=UTF-8"     pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSTL_SQL</title>
</head>
<body>
	<sql:setDataSource  url="jdbc:oracle:thin:@localhost:1521:XE"
						driver="oracle.jdbc.driver.OracleDriver"
						user="boardsearch"
						password="1234"
						scope="application" 
						var="dataSource" />
	<fmt:setLocale value="ko_kr" />
	<sql:query var="emp" dataSource="${dataSource}">
	
		SELECT EMPNO AS 사원번호, ENAME AS 이름,
		SAL AS 월급여, HIREDATE AS 입사일
		FROM employee_tbl
								
	</sql:query>

<table border="1">
	<tr>
		<c:forEach var="columnName"	items="${emp.columnNames }">
			<th><c:out value="${columnName }" /></th>
		</c:forEach>
		
		<c:forEach var="row" items="${emp.rowsByIndex }">	<!-- 한개의 로우 추출 0부터 -->
			<tr>
				<c:forEach var="column" items="${row }" varStatus="i">	<!-- i = 0부터 -->
				<c:choose>
					<c:when test="${i.index==3 }">	<!-- 3번째 컬럼(HireDate) -->
						<td><fmt:formatDate value="${column }" pattern="yyyy/MM/dd" /></td>
					</c:when>	
					<c:otherwise>
						<td><c:out value="${column }" /></td>
					</c:otherwise>
				</c:choose>
				</c:forEach>
			</tr>
		</c:forEach>
	</tr>
</table>
	
</body>
</html>