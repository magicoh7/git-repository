<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<%
	response.setContentType("text/html");
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:if test="${!empty param.name }">	<!-- param.name EL 부분 -->
		param : <x:out select="$param:name" />	<!-- select="$param:name" XML 부분 -->
	</c:if>
	
	<form>
		name : <input type="text" name="name" required="required" />
		<input type="submit" value="자신에게 전송" />
	</form>
</body>
</html>