package com.magicoh.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.magicoh.dto.Product;
import com.magicoh.dto.Worker;
import com.magicoh.util.DbManager;

public class WorkerDao {
	
	private Connection conn = null;
	private Statement stmt = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	
	private final String driver = "oracle.jdbc.driver.OracleDriver";
	private final String url = "jdbc:oracle:thin:@localhost:1521:XE";
	
	private String user = "product";
	private String password = "1234";
	
	private String sql = null;
	private int r = 0;
	
	
	public WorkerDao() {
	}
	
	public void connect() {
		try {
			Class.forName(driver);
			System.out.println("드라이버 로딩 성공");
			conn = DriverManager.getConnection(url, user, password);
			System.out.println("디비 접속 성공");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//한 개 조회
	public Boolean selectLoginCheck(Worker model) {
		
		Boolean flag = false;
		sql = "";
		
		
		sql = "Select id";
		sql += " From worker ";
		sql += " Where id = ? And pwd = ?";
		
		System.out.println("worker sql : " + sql);
		
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
		
		try {
//			conn = DbManager.getConnection();
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, model.getId());
			pstmt.setString(2, model.getPwd());
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				flag = true;
			}
			
			return flag;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
//		catch (NamingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		finally {
//			//DbManager.close(conn, pstmt, rs);
//			this.close();
//		}
		
		return flag;
	}
	
//	//게시물 등록
//	public void insertProduct(Product product) {
//		
//		sql = "";
//		int r = 0;
//		
//		sql = "Insert into product ";
//		sql += " Values(product_code_seq.nextval, ?, ?, ?, ?)";
//		
////		Connection conn = null;
////		PreparedStatement pstmt = null;
//		
//		try {
////			conn = DbManager.getConnection();
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setString(1, product.getName());
//			pstmt.setInt(2, product.getPrice());
//			pstmt.setString(3, product.getPicture());
//			pstmt.setString(4, product.getDescription());
//			
//			r = pstmt.executeUpdate();
//			if(r == 1) {
//				System.out.println("데이터 저장 : " + r + " 건 성공");
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} 
////		catch (NamingException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} 
////		finally {
////			//DbManager.close(conn, pstmt);
////			this.close();
////		}
//	}
//	
//	//상품  업데이트
//	public void UpdateProduct(Product product) {
//		
//		sql = "";
//		int r = 0;
//		
//		sql = "Update product Set ";
//		sql += " 	name = ?, ";
//		sql += " 	price = ?, ";
//		sql += " 	picture = ?, ";
//		sql += " 	description = ? ";
//		sql += " Where code  = ? ";
//		
//		//System.out.println("sql : " + sql);
//		//System.out.println("product.getCode() : " + product.getCode());
//		
////		Connection conn = null;
////		PreparedStatement pstmt = null;
//
//		try {
////			conn = DbManager.getConnection();
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setString(1, product.getName());
//			pstmt.setInt(2, product.getPrice());
//			pstmt.setString(3, product.getPicture());
//			pstmt.setString(4, product.getDescription());
//			pstmt.setInt(5, product.getCode());
//			
//			r = pstmt.executeUpdate();
//			if(r == 1) {
//				System.out.println("업데이트 성공");
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} 
////		catch (NamingException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} 
////		finally {
////			//DbManager.close(conn, pstmt);
////			this.close();
////		}
//	}
//
//	//게시물 삭제
//	public void deleteProduct(int code) {
//		sql = "";
//		int r = 0;
//		
//		sql = "Delete from product ";
//		sql += " Where code = ?";
//		
////		Connection conn = null;
////		PreparedStatement pstmt = null;
//		
//		try {
////			conn = DbManager.getConnection();
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setInt(1, code);
//			r = pstmt.executeUpdate();
//			
//			if(r > 0) {
//				System.out.println("삭제 성공");
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} 
////		catch (NamingException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} 
////		finally {
////			this.close();
////		}
//	}
	
	
	public void close() {
		try {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(pstmt != null) pstmt.close();
			if(conn != null) conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
}
	
}
