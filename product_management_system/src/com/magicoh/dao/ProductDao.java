package com.magicoh.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.magicoh.dto.Product;
import com.magicoh.util.DbManager;

//SingleTone 패턴 : 하나의 객체만 만들어짐
public class ProductDao {
	
	private Connection conn = null;
	private Statement stmt = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	
	private final String driver = "oracle.jdbc.driver.OracleDriver";
	private final String url = "jdbc:oracle:thin:@localhost:1521:XE";
	
	private String user = "product";
	private String password = "1234";
	
	private String sql = null;
	private int r = 0;
	
	
	public ProductDao() {
	}
	
	public void connect() {
		try {
			Class.forName(driver);
			System.out.println("드라이버 로딩 성공");
			conn = DriverManager.getConnection(url, user, password);
			System.out.println("디비 접속 성공");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//전체 상품 조회(
	public List<Product> selectAll(Product model){
		List<Product> products = new ArrayList<Product>();
		
		String searchText = ""; 
		sql = "";
				
		if(model.getSearchText() != null) {
			searchText = model.getSearchText();
		}
				
		int start = 0;
		int end = 0;
		
		start = (Integer.parseInt(model.getPageNum()) - 1) * model.getListCount() + 1;
		end = start + model.getListCount() - 1;
		
		System.out.println("시작게시물번호 : " + start + " / 끝 게시물번호 : " + end);
		
		sql = "Select c.seq, c.code, c.name, c.kind, c.cost_price, c.list_price, c.sales_margin, c.image, c.useyn, c.regdate";
		sql += " From( ";
		sql += "	Select rownum as seq, b.code, b.name, b.kind, b.cost_price, b.list_price, b.sales_margin, b.image, b.useyn, b.regdate";
		sql += "    From ( ";
		sql += "		Select rownum, a.code, a.name, a.kind, a.cost_price, a.list_price, a.sales_margin, a.image, a.useyn, to_char(regdate, 'yyyy-MM-dd') as regdate";
		sql += "    	From product a ";
		if(!searchText.equals("")) {
			sql += " 	Where a.name like ?";
		}
		sql += "		Order By a.code Desc ";
		sql += "    ) B";   
		sql += " ) C";
		sql += " Where c.seq Between ? And ?";
		
		System.out.println("searchText:" + searchText);
		System.out.println("sql : " + sql );
		
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
		
		try {
//			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			if(!searchText.equals("")) {	
					pstmt.setString(1, "%" + searchText + "%");		//상품명 
					pstmt.setInt(2, start);							//start num
					pstmt.setInt(3, end);							//end num
			}else {	//상품 검색어가 없으면 
					pstmt.setInt(1, start);							//start num
					pstmt.setInt(2, end);							//end num
			}

			rs = pstmt.executeQuery();
			while(rs.next()) {
				Product product = new Product();
				product.setCode(rs.getInt("code"));
				product.setName(rs.getString("name"));
				product.setKind(rs.getString("kind"));				
				product.setCost_price(rs.getInt("cost_price"));
				product.setList_price(rs.getInt("list_price"));
				product.setSales_margin(rs.getInt("sales_margin"));
				product.setImage(rs.getString("image"));
				product.setUseyn(rs.getString("useyn"));
				product.setRegdate(rs.getDate("regdate"));
				products.add(product);
				
				System.out.println(product.toString());
				product = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
//		finally {
////			DbManager.close(conn, pstmt, rs);
//			this.close();
//		}
		return products;
	}
	
	//테이블의 전체 레코드 수 조회
	public int selectCount(Product product) {
		int totalCount = 0;
		sql = "";
		
		String searchText = product.getSearchText();
		
		sql = "Select Count(code) as totalCount";
		sql += " From product";
		if(!searchText.equals("")) {
			sql += " Where name Like ? ";
		}
		
		//System.out.println(sql);
		
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;

		
		try {
//			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			if(!searchText.equals("")) {
					pstmt.setString(1, "%" + searchText + "%");
			}
			rs = pstmt.executeQuery();
			if(rs.next()) {
				totalCount = rs.getInt("totalCount");
				//System.out.println("totalCount : " + totalCount);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
			//System.out.println("select count ERR" + e.getMessage());
		} 
//		catch (NamingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		finally {
//			//DbManager.close(conn, pstmt, rs);
//			this.close();
//		}
		return totalCount;
	}
	
	
	//한 개 조회
	public Product selectProductByCode(int code) {
		
		sql = "";
		sql = "Select code, name, kind, cost_price, list_price, sales_margin, content, image, useyn, bestyn, to_char(regdate, 'yyyy-MM-dd') as regdate";
		sql += " From product ";
		sql += " Where code = ?";
		
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
		
		try {
//			conn = DbManager.getConnection();
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, code);
			rs = pstmt.executeQuery();
			rs.next();
			
			Product product = new Product();
			product.setCode(rs.getInt("code"));
			product.setName(rs.getString("name"));
			product.setKind(rs.getString("kind"));				
			product.setCost_price(rs.getInt("cost_price"));
			product.setList_price(rs.getInt("list_price"));
			product.setSales_margin(rs.getInt("sales_margin"));
			product.setContent(rs.getString("content"));
			product.setImage(rs.getString("image"));
			product.setUseyn(rs.getString("useyn"));
			product.setBestyn(rs.getString("bestyn"));
			product.setRegdate(rs.getDate("regdate"));
			
			return product;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
//		catch (NamingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		finally {
//			//DbManager.close(conn, pstmt, rs);
//			this.close();
//		}
		
		return null;
	}
	
	//게시물 등록
	public void insertProduct(Product product) {
		
		sql = "";
		int r = 0;
		int sales_margin = 0;

		if(product.getList_price() != null && product.getCost_price() != null) {
			sales_margin = product.getList_price() - product.getCost_price();
		}
		
		sql = "Insert Into product(code, name, cost_price, list_price, sales_margin, content, image, useyn, bestyn, regdate)";
		sql += " Values(product_code_seq.nextval, ?, ?, ?, ?, ?, ?, ?, ?, sysdate)";
		
//		Connection conn = null;
//		PreparedStatement pstmt = null;
		
		try {
//			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, product.getName());
			pstmt.setInt(2, product.getCost_price());
			pstmt.setInt(3, product.getList_price());
			pstmt.setInt(4, sales_margin);
			pstmt.setString(5, product.getContent());
			pstmt.setString(6, product.getImage());
			pstmt.setString(7, product.getUseyn());
			pstmt.setString(8, product.getBestyn());
			
			r = pstmt.executeUpdate();
			if(r == 1) {
				System.out.println("데이터 저장 : " + r + " 건 성공");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
//		catch (NamingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		finally {
//			//DbManager.close(conn, pstmt);
//			this.close();
//		}
	}
	
	//상품  업데이트
	public void UpdateProduct(Product product) {
		
		sql = "";
		int r = 0;
		int sales_margin = 0;

		if(product.getList_price() != null && product.getCost_price() != null) {
			sales_margin = product.getList_price() - product.getCost_price();
		}

		sql = "Update product Set ";
		sql += " 	name = ?, ";
		sql += " 	cost_price = ?, ";
		sql += " 	list_price = ?, ";
		sql += " 	sales_margin = ?, ";
		sql += " 	content = ?, ";
		sql += " 	image = ?, ";
		sql += " 	useyn = ?, ";
		sql += " 	bestyn = ? ";
		sql += " Where code  = ? ";
		
		System.out.println("Update sql : " + sql);
		//System.out.println("product.getCode() : " + product.getCode());
		
//		Connection conn = null;
//		PreparedStatement pstmt = null;

		try {
//			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, product.getName());
			pstmt.setInt(2, product.getCost_price());
			pstmt.setInt(3, product.getList_price());
			pstmt.setInt(4, sales_margin);
			pstmt.setString(5, product.getContent());
			pstmt.setString(6, product.getImage());
			pstmt.setString(7, product.getUseyn());
			pstmt.setString(8, product.getBestyn());
			
			r = pstmt.executeUpdate();
			if(r == 1) {
				System.out.println("업데이트 성공");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
//		catch (NamingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		finally {
//			//DbManager.close(conn, pstmt);
//			this.close();
//		}
	}

	//게시물 삭제
	public void deleteProduct(int code) {
		sql = "";
		int r = 0;
		
		sql = "Delete from product ";
		sql += " Where code = ?";
		
//		Connection conn = null;
//		PreparedStatement pstmt = null;
		
		try {
//			conn = DbManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, code);
			r = pstmt.executeUpdate();
			
			if(r > 0) {
				System.out.println("삭제 성공");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
//		catch (NamingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		finally {
//			this.close();
//		}
	}
	
	
	public void close() {
		try {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(pstmt != null) pstmt.close();
			if(conn != null) conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
}
	
}
