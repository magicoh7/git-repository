package com.magicoh.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.ProductDao;
import com.magicoh.dto.Product;
import com.magicoh.util.PageNavigator;
/**
 * Servlet implementation class ProductListServlet
 */
@WebServlet("/productList.do")
public class ProductListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductListServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//ProductDao dao = ProductDao.getInstance();
		//ProductDao dao = new ProductDao();
		
		List<Product> products = new ArrayList<Product>();

		String pageNum = request.getParameter("pageNum");	//페이지 번호를 눌렀을 때 전달
		String searchText = request.getParameter("searchText");
		
		//처음 화면이 열릴 때는 기본적으로 모든 상품리스트
		if(pageNum == null) {
			pageNum = "1";
		}
		if(searchText == null) {	
			searchText = "";
		}
		
		//Dao 쪽으로 보낼 파라미터를 Dto BoardModel 객체로 만들어서 보냄.
		Product product = new Product();
		product.setPageNum(pageNum);
		product.setSearchText(searchText);

		ProductDao dao = new ProductDao();
		dao.connect();
		int totalCount = dao.selectCount(product);
		products = dao.selectAll(product);
		dao.close();
		
		request.setAttribute("totalCount", totalCount);
		request.setAttribute("pageNum", pageNum);
		PageNavigator pageNavigator = new PageNavigator();
		String pageNums = pageNavigator.getPageNavigator(totalCount, 	//총 게시물 수
										product.getListCount(), 			//한 페이지에 보여줄 게시물수
										product.getPagePerBlock(), 		//한 페이지에 보여줄 페이지 블럭
										Integer.parseInt(pageNum), 		//요청된 페이지 번호
										searchText);
		
		//System.out.println("반환받은 스트링 : " + pageNums);
		
		request.setAttribute("page_navigator", pageNums);
		request.setAttribute("productList", products);
		request.setAttribute("product", product);	//검색타입 검색어를 계속 유지키위해

		RequestDispatcher rd = request.getRequestDispatcher("productList.jsp");
		rd.forward(request, response);		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
