package com.magicoh.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.ProductDao;
import com.magicoh.dto.Product;

@WebServlet("/productDelete.do")
public class ProductDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductDeleteServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//파라미터 전달
		int code = Integer.parseInt(request.getParameter("code"));
		
		//ProductDao dao = ProductDao.getInstance();
		ProductDao dao = new ProductDao();
		dao.connect();
		Product product = dao.selectProductByCode(code);
		dao.close();
		
		request.setAttribute("product", product);
		RequestDispatcher rd = request.getRequestDispatcher("productDelete.jsp");
		rd.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int code = Integer.parseInt(request.getParameter("code"));
		
		//ProductDao dao = ProductDao.getInstance();
		ProductDao dao = new ProductDao();
		dao.connect();
		dao.deleteProduct(code);
		dao.close();
		
		response.sendRedirect("productList.do");

	}

}
