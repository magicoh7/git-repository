package com.magicoh.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.dao.ProductDao;
import com.magicoh.dao.WorkerDao;
import com.magicoh.dto.Worker;

/**
 * Servlet implementation class WorkerLoginServlet
 */
@WebServlet("/workerLogin.do")
public class WorkerLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public WorkerLoginServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		boolean flag = false;
		String msg = "";
		
		//저장할 값 넘겨받음
		String id = request.getParameter("id");
		String pwd = request.getParameter("pwd");
		
		//넘어온 값을 BoardModel 객체에 세팅
		Worker worker = new Worker();
		worker.setId(id);
		worker.setPwd(pwd);
		
		//ProductDao dao = ProductDao.getInstance();
		WorkerDao dao = new WorkerDao();
		dao.connect();
		flag = dao.selectLoginCheck(worker);
		dao.close();
		
		if(flag) {
			msg = worker.getName() + " 님 반갑습니다.";
			//세션에 기록
			HttpSession session = request.getSession();
			session.setAttribute("id", id);			
			
			response.sendRedirect("productList.do");
		}else {
			msg = "로그인에 실패했습니다. 아이디와 비밀번호를 확인하세요.";
			
			request.setAttribute("id", id);
			request.setAttribute("msg", msg);
			request.setAttribute("Worker", worker);
			
			RequestDispatcher rd = request.getRequestDispatcher("workerLoginAction.jsp");
			rd.forward(request, response);	
		}
	}

}
