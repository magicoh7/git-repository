package com.magicoh.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.ProductDao;
import com.magicoh.dto.Product;

@WebServlet("/productView.do")
public class ProductViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductViewServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int code = Integer.parseInt(request.getParameter("code"));
		
		//ProductDao dao = ProductDao.getInstance();
		ProductDao dao = new ProductDao();
		dao.connect();
		Product product = dao.selectProductByCode(code);
		dao.close();
		
		request.setAttribute("product", product);
		RequestDispatcher rd = request.getRequestDispatcher("productView.jsp");
		rd.forward(request, response);	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
