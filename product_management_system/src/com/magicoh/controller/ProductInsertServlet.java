package com.magicoh.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.magicoh.dao.ProductDao;
import com.magicoh.dto.Product;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

@WebServlet("/productInsert.do")
public class ProductInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductInsertServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("productInsert.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		ServletContext context = getServletContext();

		String path = context.getRealPath("upload");
		String encType = "UTF-8";
		int sizeLimit = 20 * 1024 * 1024;
		
		MultipartRequest multi = new MultipartRequest(request,
														path,
														sizeLimit,
														encType,
														new DefaultFileRenamePolicy());
		String name = multi.getParameter("name");
		int cost_price = Integer.parseInt(multi.getParameter("cost_price"));
		int list_price = Integer.parseInt(multi.getParameter("list_price"));
		String content = multi.getParameter("content");
		String image = multi.getFilesystemName("image");
		String useyn = multi.getFilesystemName("useyn");
		String bestyn = multi.getFilesystemName("bestyn");
		
		System.out.println("productInsert image : " + image);
		
		//넘어온 값을 Product 객체에 세팅
		Product product = new Product();
		product.setName(name);
		product.setCost_price(cost_price);
		product.setList_price(list_price);
		product.setContent(content);
		product.setImage(image);
		product.setUseyn(useyn);
		product.setBestyn(bestyn);
		
		//ProductDao dao = ProductDao.getInstance();
		ProductDao dao = new ProductDao();
		dao.connect();
		dao.insertProduct(product);
		dao.close();
		
		response.sendRedirect("productList.do");
	}

}
