package com.magicoh.util;

import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DbManager {
	
	public DbManager() {
	}
	
	public static Connection getConnection() throws NamingException {

		Connection conn = null;
		
		try {
			Context initContext = new InitialContext();
			Context envContext  = (Context)initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource)envContext.lookup("jdbc/myoracle");		//web.xml에 설정했던 값
			conn = ds.getConnection();
			
			System.out.println("conn : " + conn);
		} catch (NamingException e) {
			e.printStackTrace();
			//System.out.println("Connect ERR " + e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			//System.out.println("DataSource ERR " + e.getMessage());
		}
			
		return conn;
	}
	
	//PrepareStatment 는 Statement의 아들이기 때문에 별도로 close 안해도 자동으로 닫힘
	public static void close(Connection conn, Statement stmt, ResultSet rs) {
		try {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(conn != null) conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close(Connection conn, Statement stmt) {
		try {
			if(stmt != null) stmt.close();
			if(conn != null) conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
