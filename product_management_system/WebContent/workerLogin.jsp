<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*, com.magicoh.dto.*" %>

	<% 
		Worker worker = (Worker)session.getAttribute("worker");		
	
	if(session.getAttribute("user") == null) %>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>관리자 로그인 화면 : workerLogin.jsp</title>
	<link rel="stylesheet" type="text/css" href="css/shopping.css" />
	<script type="text/javascript" src="script/product.js" ></script>
</head>
<body>


	<div id="wrap" align="center">
		<h1>관리자 로그인</h1>
		<form method="post" action="workerLogin.do" name="login_form">
			<div>
				아이디 <input type="text"  placeholder="아이디" name="id" maxlength="20" required  />
	    	</div>
	   		<div>
				비밀번호 <input type="password"  placeholder="비밀번호" name="pwd" maxlength="20" required />
	    	</div>
	    	
			<br />
			<input type="button" value="로그인" onclick="workerLoginCheck()">
			<input type="reset" value="다시작성">
		</form>
	</div>
</body>
</html>