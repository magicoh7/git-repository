<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/views/shared/header.jsp" %>
	
	<div class="container">
		<div class="row">
			<!-- <form action="modify" method="post">  -->
				<!-- <input type="hidden" name="bId" value="${content_view.bId}">  -->
				<table class="table table-bordered table-hover table-striped" style="text-align: center; border: 1px solid #dddddd">
						<thead>
							<tr>
								<th colspan="3" style="background-color: #eeeeee; text-align: center;">상품 상세 페이지</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width: 20%">상품 ID</td>
								<td colspan="2" style="text-align: left;">${product.product_id} </td>
							</tr>
							<tr>
								<td>상품명</td>
								<td colspan="2" style="text-align: left;">${product.product_name}</td>
							</tr>
							<tr>
								<td>브랜드</td>
								<td colspan="2" style="text-align: left;">${product.brand }</td>
							</tr>
							<tr>
								<td>상품 설명</td>
								<td colspan="2" style="text-align: left;">${product.description }</td>
							</tr>
							<tr>
								<td>단가</td>
								<td colspan="2" style="text-align: left;">${product.unit_price }</td>
							</tr>
							<tr>
								<td>상품 Category</td>
								<td colspan="2" style="text-align: left;">${product.category_id }</td>
							</tr>
							<tr>
								<td>사용여부</td>
								<td colspan="2" style="text-align: left;">${product.is_active }</td>
							</tr>
							<tr>
								<td>입고일</td>
								<td colspan="2" style="text-align: left;">${product.receipt_date }</td>
							</tr>
						</tbody>
				</table>
			<!--  </form> -->	
				<div align="center">			
					<a href="product_list" class="btn btn-primary">목록</a>
					<a href="product_updateform?product_id=${product.product_id}" class="btn btn-info">수정</a>
					<a onclick="return confirm('정말로 삭제하시겠습니까?');" href="product_delete?product_id=${product.product_id}" class="btn btn-danger">삭제</a>
				</div>
		</div>
	</div>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	
</body>
</html>