package com.magicoh.springmvc.service.sales;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magicoh.springmvc.dao.IBbsMapper;
import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.InvoiceCommonResult;

@Service
//@Service("IProductService")
public class InvoiceServiceImpl implements IInvoiceService
{
	@Autowired
	private IBbsMapper bbsMappger;

	@Override
	public List<InvoiceCommonResult> getInvoiceList() {
		List<InvoiceCommonResult> invoiceListView = bbsMappger.getInvoiceList();
		
		//System.out.println("InvoiceServiceImpl invoiceListView.size() ====> " + invoiceListView.size());
		
		return invoiceListView;
	}

	@Override
	public List<InvoiceCommonResult> getInvoiceDetail(int invoice_id)
	{
		List<InvoiceCommonResult> invoiceDetailList = bbsMappger.getInvoiceDetail(invoice_id);
		return invoiceDetailList;
	}

	@Override
	public int getMaxInvoiceId() {
		return bbsMappger.getMaxInvoiceId();
	}

	@Override
	public void insertInvoiceDetail(InvoiceDetail invoiceDetail)
	{
		bbsMappger.insertInvoiceDetail(invoiceDetail);
	}

	
	@Override
	public void insertInvoiceHeader(InvoiceHeader invoiceHeader) {
		bbsMappger.insertInvoiceHeader(invoiceHeader);
	}

	@Override
	public InvoiceCommonResult getInvoiceHeader(int invoice_no)
	{
		InvoiceCommonResult invoiceHeader = bbsMappger.selectInvoiceHeader(invoice_no);
		return invoiceHeader;
	}

	/**

	@Override
	public void updateProduct(Product product)
	{
		bbsMappger.updateProduct(product);
	}

	@Override
	public void deleteProduct(int product_id)
	{
		bbsMappger.deleteProduct(product_id);
	}
	**/
}
