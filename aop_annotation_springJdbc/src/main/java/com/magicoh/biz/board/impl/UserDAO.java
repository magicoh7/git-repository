package com.magicoh.biz.board.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.UserVo;

/** DAO : Data Access Object **/

@Repository("userDAO")
public class UserDAO {

	// JDBC 관련 멤버 변수
	private Connection conn = null;
	private PreparedStatement stmt = null;
	private ResultSet rs = null;

	// SQL 명령어 상수 선언
	private final String USER_GET = "select * from users where id=? and password=?";

	public UserDAO() {
	}

	/** CRUD 기능 메서드 구현 **/
	// 회원 로그 체크
	public UserVo getUser(UserVo vo) {
		System.out.println("===> JDBC로 getUser() 기능 처리");
		UserVo user = null;

		try {
			this.conn = JDBCUtil.getConnection();
			this.stmt = this.conn.prepareStatement(USER_GET);
			this.stmt.setString(1, vo.getId());
			this.stmt.setString(2, vo.getPassword());
			this.rs = this.stmt.executeQuery();

			if (this.rs.next()) {
				user = new UserVo();
				user.setId(this.rs.getString("id"));
				user.setPassword(this.rs.getString("password"));
				user.setName(this.rs.getString("name"));
				user.setRole(this.rs.getString("role"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(rs, stmt, conn);
		}
		return user;
	} // getBoard(BoardVO vo) END

}


/**
 * @Repository

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.freeflux.biz.board.BoardVO;
import com.freeflux.biz.board.UserVo;

@Repository("userDAO")
public class UserDAO {

	// JDBC 관련 멤버 변수
	private Connection conn = null;
	private PreparedStatement stmt = null;
	private ResultSet rs = null;

	// SQL 명령어 상수 선언
	private final String USER_GET = "select * from users where id=? and password=?";

	public UserDAO() {
	}


	// 회원 로그 체크
	public UserVo getUser(UserVo vo) {
		System.out.println("===> JDBC로 getUser() 기능 처리");
		UserVo user = null;

		try {
			this.conn = JDBCUtil.getConnection();
			this.stmt = this.conn.prepareStatement(USER_GET);
			this.stmt.setString(1, vo.getId());
			this.stmt.setString(2, vo.getPassword());
			this.rs = this.stmt.executeQuery();

			if (this.rs.next()) {
				user = new UserVo();
				user.setId(this.rs.getString("id"));
				user.setPassword(this.rs.getString("password"));
				user.setName(this.rs.getString("name"));
				user.setRole(this.rs.getString("role"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(rs, stmt, conn);
		}
		return user;
	} // getBoard(BoardVO vo) END

}
**/



/**
 * 
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.freeflux.biz.board.BoardVO;
import com.freeflux.biz.board.UserVo;


public class UserDAO {

	// JDBC 관련 멤버 변수
	private Connection conn = null;
	private PreparedStatement stmt = null;
	private ResultSet rs = null;

	// SQL 명령어 상수 선언
	private final String USER_GET = "select * from users where id=? and password=?";

	public UserDAO() {
	}


	// 회원 로그 체크
	public UserVo getUser(UserVo vo) {
		System.out.println("===> JDBC로 getUser() 기능 처리");
		UserVo user = null;

		try {
			this.conn = JDBCUtil.getConnection();
			this.stmt = this.conn.prepareStatement(USER_GET);
			this.stmt.setString(1, vo.getId());
			this.stmt.setString(2, vo.getPassword());
			this.rs = this.stmt.executeQuery();

			if (this.rs.next()) {
				user = new UserVo();
				user.setId(this.rs.getString("id"));
				user.setPassword(this.rs.getString("password"));
				user.setName(this.rs.getString("name"));
				user.setRole(this.rs.getString("role"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(rs, stmt, conn);
		}
		return user;
	} // getBoard(BoardVO vo) END

}

**/