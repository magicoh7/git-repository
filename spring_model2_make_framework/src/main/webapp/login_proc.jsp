<%@ page import="com.magicoh.biz.board.impl.UserDAO"%>
<%@ page import="com.magicoh.biz.board.UserVo"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%
	String id = request.getParameter("id");
	String password = request.getParameter("password");
	
	out.println("id : " + id);
	
	UserVo vo = new UserVo();
	vo.setId(id);
	vo.setPassword(password);

	UserDAO userDAO = new UserDAO();
	UserVo user = userDAO.getUser(vo);
	
	if(user != null){
		response.sendRedirect("getBoardList.jsp");
	}else{
		response.sendRedirect("login.jsp");
	}

%>
