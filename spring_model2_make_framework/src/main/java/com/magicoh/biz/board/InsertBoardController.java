package com.magicoh.biz.board;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.biz.board.impl.BoardDAO;
import com.magicoh.biz.controller.Controller;

public class InsertBoardController implements Controller {

	public InsertBoardController() {
	}

	@Override
	public String handlerRequest(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("저장 작업 처리");

		String title = request.getParameter("title");
		String writer = request.getParameter("writer");
		String content = request.getParameter("content");
		
		BoardVO vo = new BoardVO();
		vo.setTitle(title);
		vo.setWriter(writer);
		vo.setContent(content);
		
		BoardDAO boardDAO = new BoardDAO();
		boardDAO.insertBoard(vo);
		
		return "getBoardList.do";
	}

}
