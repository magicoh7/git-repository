package com.magicoh.biz.board;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.magicoh.biz.board.impl.BoardDAO;
import com.magicoh.biz.controller.Controller;

public class LogoutController implements Controller {

	public LogoutController() {
	}

	@Override
	public String handlerRequest(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		session.invalidate();
		
		return "login";
	}

}
