package com.magicoh.biz.board.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.magicoh.biz.board.BoardVO;


@Repository("boardDAO")
public class BoardDAO {

	public BoardDAO() {
	}

	// JDBC 관련 변수
	private Connection conn = null;
	private PreparedStatement stmt = null;
	private ResultSet rs = null;

	// SQL 명령어들
	private final String Board_INSERT = "insert into board_spring (seq, title, writer, content) values ((select nvl(max(seq),0)+1 from board_spring),?,?,?)";
	private final String Board_UPDATE = "update board_spring set title=?, content=? where seq=?";
	private final String Board_DELETE = "delete from board_spring where seq=?";
	private final String Board_GET = "select * from board_spring where seq=?";
	private final String Board_LIST = "select * from board_spring order by seq desc";

	// CRUD 기능의 메소드 구현
	// 글등록
	public void insertBoard(BoardVO vo) {
		System.out.println("===>BoardDAO에서 JDBC로 insertBoard() 기능 처리");
		try {
			conn = JDBCUtil.getConnection();
			System.out.println("conn : " + conn);
			stmt = conn.prepareStatement(Board_INSERT);
			stmt.setString(1, vo.getTitle());
			stmt.setString(2, vo.getWriter());
			stmt.setString(3, vo.getContent());
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("insertBoard(BoardVO vo)ERR:" + e.getMessage());
		} finally {
			//JDBCUtil.close(stmt, conn);
		}

	}// insertBoard() END

	// 글 수정
	public void updateBoard(BoardVO vo) {
		System.out.println("===>JDBC로 updateBoard() 기능 처리 Board Obj : " + vo.toString());
		try {
			conn = JDBCUtil.getConnection();
			stmt = conn.prepareStatement(Board_UPDATE);
			stmt.setString(1, vo.getTitle());
			stmt.setString(2, vo.getContent());
			stmt.setInt(3, vo.getSeq());
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("updateBoard(BoardVO vo)ERR:" + e.getMessage());
		} finally {
			//JDBCUtil.close(stmt, conn);
		}

	}// updateBoard() END

	// 글 삭제
	public void deleteBoard(BoardVO vo) {
		System.out.println("===>JDBC로 deleteBoard() 기능 처리");

		try {
			conn = JDBCUtil.getConnection();
			stmt = conn.prepareStatement(Board_DELETE);
			stmt.setInt(1, vo.getSeq());
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("deleteBoard(BoardVO vo)ERR:" + e.getMessage());
		} finally {
			//JDBCUtil.close(stmt, conn);
		}

	}// deleteBoard() END

	// 글 상세 조회
	public BoardVO getBoard(BoardVO vo) {
		System.out.println("===>JDBC로 getBoard() 기능 처리 전달받은 seq : " + vo.getSeq());
		BoardVO board = null;

		try {
			conn = JDBCUtil.getConnection();
			stmt = conn.prepareStatement(Board_GET);
			stmt.setInt(1, vo.getSeq());
			rs = stmt.executeQuery();
			if (rs.next()) {
				board = new BoardVO();
				board.setSeq(rs.getInt("seq"));
				board.setTitle(rs.getString("title"));
				board.setWriter(rs.getString("writer"));
				board.setContent(rs.getString("content"));
				board.setRegdate(rs.getDate("regdate"));
				board.setCnt(rs.getInt("cnt"));
			}
		} catch (SQLException e) {
			System.out.println("getBoard(BoardVO vo)ERR:" + e.getMessage());
		} finally {
			//JDBCUtil.close(stmt, conn);
		}

		return board;
	}// getBoard() END

	// 글목록 조회
	public List<BoardVO> getBoardList(BoardVO vo) {

		System.out.println("===>JDBC로 getBoardList() 기능 처리");
		List<BoardVO> boardList = new ArrayList<BoardVO>();

		try {
			conn = JDBCUtil.getConnection();
			stmt = conn.prepareStatement(Board_LIST);
			rs = stmt.executeQuery();
			while (rs.next()) {
				BoardVO board = new BoardVO();
				board.setSeq(rs.getInt("seq"));
				board.setTitle(rs.getString("title"));
				board.setWriter(rs.getString("writer"));
				board.setContent(rs.getString("content"));
				board.setRegdate(rs.getDate("regdate"));
				board.setCnt(rs.getInt("cnt"));
				boardList.add(board);
			}
		} catch (SQLException e) {
			System.out.println("getBoardList(BoardVO vo)ERR:" + e.getMessage());
		} finally {
			//JDBCUtil.close(rs, stmt, conn);
		}
		return boardList;
	}// getBoardList() END
}
