package com.magicoh.biz.controller;

import java.util.HashMap;
import java.util.Map;

import com.magicoh.biz.board.DeleteBoardController;
import com.magicoh.biz.board.GetBoardController;
import com.magicoh.biz.board.GetBoardListController;
import com.magicoh.biz.board.InsertBoardController;
import com.magicoh.biz.board.LogoutController;
import com.magicoh.biz.board.UpdateBoardController;
import com.magicoh.biz.user.LoginController;

public class HandlerMapping {
	private Map<String, Controller> mappings;

	public HandlerMapping() {
		mappings = new HashMap<String, Controller>();
		mappings.put("/login.do", new LoginController());
		mappings.put("/getBoardList.do", new GetBoardListController());
		mappings.put("/getBoard.do", new GetBoardController());
		mappings.put("/insertBoard.do", new InsertBoardController());
		mappings.put("/updateBoard.do", new UpdateBoardController());
		mappings.put("/deleteBoard.do", new DeleteBoardController());
		mappings.put("/logout.do", new LogoutController());
	}
	
	public Controller getController(String path){
		return mappings.get(path);
	}
	
	

}
