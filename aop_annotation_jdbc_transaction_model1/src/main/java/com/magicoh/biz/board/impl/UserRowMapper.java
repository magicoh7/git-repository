package com.magicoh.biz.board.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.UserVo;

public class UserRowMapper implements RowMapper<UserVo> {

	public UserRowMapper() {
	}

	@Override
	public UserVo mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserVo user = new UserVo();

		user.setId(rs.getString("id"));
		user.setPassword(rs.getString("password"));
		user.setName(rs.getString("name"));
		user.setRole(rs.getString("role"));
				
		return user;
	}

}
