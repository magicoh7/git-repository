<%@ page import="java.util.List"%>
<%@ page import="com.magicoh.biz.board.impl.BoardDAO"%>
<%@ page import="com.magicoh.biz.board.BoardVO"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<%

	String seq = request.getParameter("seq");

	BoardVO vo = new BoardVO();
	vo.setSeq(Integer.parseInt(seq));
	
	BoardDAO boardDAO = new BoardDAO();
	BoardVO board = boardDAO.getBoard(vo);
			
%>

<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>글상세</title>
</head>
<body align="center">
	<h1>글상세</h1>
	<a href="logout_proc.jsp">Log-out</a>
	<hr>
	<div align="center">
		<form action="updateBoard_proc.jsp" method="post">
			<input type="hidden" name="seq" value="<%= board.getSeq() %>" />
			<table border="1" cellpadding="0" cellspacing="0" width="700">
				<tr>
					<td bgcolor="orange" width="70">제목</td>
					<td align="left"><input type="text" name="title" value="<%= board.getTitle()%>"/></td>
				</tr>
				<tr>
					<td bgcolor="orange">작성자</td>
					<td align="left"><%= board.getWriter() %></td>
				</tr>
				<tr>
					<td bgcolor="orange">내용</td>
					<td align="left"><%= board.getContent() %></td>
				</tr>
				<tr>
					<td bgcolor="orange">등록일</td>
					<td align="left"><%= board.getRegdate() %></td>
				</tr>
				<tr>
					<td bgcolor="orange">조회수</td>
					<td align="left"><%= board.getCnt() %></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="저장"/>
					</td>
				</tr>
			</table>
		</form>
		<hr>
		<a href="insertBoard.jsp">글등록</a>&nbsp;&nbsp;&nbsp;
		<a href="deleteBoard_proc.jsp?seq=<%= board.getSeq() %>">글삭제</a>&nbsp;&nbsp;&nbsp;
		<a href="getBoardList.jsp">글목록</a>
	</div>
</body>
</html>
