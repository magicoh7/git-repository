package com.magicoh.biz.board.common;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;

import com.magicoh.biz.user.UserVo;

@Service
@Aspect
public class AfterReturningAdvice {

	public AfterReturningAdvice() {
	}
	
	@Pointcut("execution(* com.magicoh.biz..*Impl.get*(..))")
	public void getPointcut(){}
	
	@AfterReturning(pointcut="getPointcut()", returning="returnObj")
	public void afterLog(JoinPoint jp, Object returnObj){
		String method = jp.getSignature().getName();
		
		if(returnObj instanceof UserVo){
			UserVo user = (UserVo) returnObj;
			if(user.getRole().equals("Admin")){
				System.out.println(user.getName() + " 로그인(Admin)");
			}
		}
		System.out.println("사후처리 " + method + "() 메소드 return 정보 : " + returnObj.toString());
	}

	
	public void afterLog(){
		System.out.println("사후처리 - 비즈니스 로직 수행 후 동작");
	}

}
