package com.magicoh.view.user;

//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.magicoh.biz.user.UserVo;
import com.magicoh.biz.user.impl.UserDAO;

@Controller
public class LoginController  {

	@RequestMapping("/login.do")
	public String login(UserVo vo, UserDAO userDAO) {
		System.out.println("로그인 처리");
		
		if(userDAO.getUser(vo) != null){	
			return "getBoardList.do";	//해당 로직 처리후 뭔가 보여줄 정보가 있는 경우
		}else{
			return "login.jsp";		//해당 로직 처리후 보여줄 정보가 없는 경우는 그냥 login.jsp(폼)를 보여주도록
		}
	}

}
