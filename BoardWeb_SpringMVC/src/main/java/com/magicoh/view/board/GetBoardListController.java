package com.magicoh.view.board;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.impl.BoardDAO;

@Controller
public class GetBoardListController{

	@RequestMapping("/getBoardList.do")
	public ModelAndView getBoardList(BoardVO vo, BoardDAO boardDAO, ModelAndView mav){
		
		System.out.println("목록 보기 처리");
		
		List<BoardVO> boardList = boardDAO.getBoardList(vo);
		System.out.println("boardList.size() : " + boardList.size());
		mav.addObject("boardList", boardList);
		mav.setViewName("getBoardList.jsp");
		
		return mav;
	}


}
