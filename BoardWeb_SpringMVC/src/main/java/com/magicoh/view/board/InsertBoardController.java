package com.magicoh.view.board;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.magicoh.biz.board.BoardVO;
import com.magicoh.biz.board.impl.BoardDAO;

@Controller
public class InsertBoardController {

	@RequestMapping("/insertBoard.do")
	public String insertBoard(BoardVO vo, BoardDAO boardDAO) {
		System.out.println("저장 작업 처리");
		
		boardDAO.insertBoard(vo);
		
		return "redirect:getBoardList.do";
	}
}
