package com.magicoh.springmaven.service;


import java.util.List;

import com.magicoh.springmaven.dto.BDto;

public interface IBbsService
{
	public List<BDto> getBbsList();
	public BDto getBbs(String bId);
	public void insertBbs(BDto bdto);
	public void updateBbs(BDto bdto);
	public void deleteBbs(String bId);
	//public BDto getReplyBbs(String bId);
	public void insertReplyBbs(BDto bdto);
}
