package com.magicoh.springmaven.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magicoh.springmaven.dao.IBbsDao;
import com.magicoh.springmaven.dao.IBbsMapper;
import com.magicoh.springmaven.dto.BDto;

@Service("IBbsService")
public class BbsServiceImpl implements IBbsService
{
	@Autowired
	//private IBbsDao bbsDao;
	private IBbsMapper bbsMapper;

	@Override
	public List<BDto> getBbsList()
	{
		//List<BDto> bbsList = bbsDao.getBbsList();
		List<BDto> bbsList = bbsMapper.selectBbsList();
		return bbsList;
	}

	@Override
	public BDto getBbs(String bId)
	{
		bbsMapper.updateHitNum(bId);	//조회수 + 1
		
		//BDto dto = bbsDao.getBbs(bId);
		BDto dto = bbsMapper.selectBbs(bId);
		return dto;
	}

	@Override
	public void insertBbs(BDto bdto)
	{
		//bbsDao.insertBbs(bdto);
		bbsMapper.insertBbs(bdto);
	}

	@Override
	public void updateBbs(BDto bdto)
	{
		//bbsDao.updateBbs(bdto);
		bbsMapper.updateBbs(bdto);
	}

	@Override
	public void deleteBbs(String bId)
	{
		//bbsDao.deleteBbs(bId);
		bbsMapper.deleteBbs(bId);
	}

	@Override
	public void insertReplyBbs(BDto bdto)
	{
		bbsMapper.updateReplyShape(bdto.getbGroup(), bdto.getbStep()); //댓글을 달기 전에 댓글이 한 개라도 있으면 STEP + 1 해서 새 댓글이 그자리에 들어갈 자리 마련한다.
		bdto.setbStep(bdto.getbStep() + 1);	//댓글은 원글 대비 step + 1, indent + 1(원본글 보다 한칸 뒤로 밀려서 나와야 하니까)
		bdto.setbIndent(bdto.getbIndent() + 1);

		//bbsDao.insertReplyBbs(bdto);
		bbsMapper.insertReplyBbs(bdto);		
	}
}
