package com.magicoh.springmaven.dao;

import java.util.List;

import com.magicoh.springmaven.dto.BDto;

@MyMapper
public interface IBbsDao
{
	public List<BDto> getBbsList();
	public BDto getBbs(String bId);
	public void insertBbs(BDto bdto);
	public void updateBbs(BDto bdto);
	public void deleteBbs(String bId);
	//public BDto getReplyBbs(String bId);
	public void insertReplyBbs(BDto bdto);

//	public void replyShapeDao(String bGroup, int bStep);
//	public void upHitDao(String bId);
	
}
