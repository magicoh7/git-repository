package com.magicoh.springmaven.dao;

import java.util.List;

import com.magicoh.springmaven.dto.BDto;

@MyMapper	//내가 바로 매퍼 인터페이스다. 
public interface IBbsMapper
{
	public List<BDto> selectBbsList();
	public BDto selectBbs(String bId);
	public void insertBbs(BDto dto);
	public void updateBbs(BDto dto);
	public void deleteBbs(String bId);
	//public void selectReplyBbs(String bId);
	public void insertReplyBbs(BDto dto);
	public void updateReplyShape(int bGroup, int bStep);
	public void updateHitNum(String bId);
	
}
