package com.magicoh.springmaven.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.magicoh.springmaven.dto.BDto;

@Repository("IBbsDao")
public class BbsDaoImpl implements IBbsDao
{
	@Autowired
	//private SqlSession sqlSession ;
	private IBbsMapper bbsMapper; 
	
	@Override
	public List<BDto> getBbsList()
	{
		List<BDto> bbsList = bbsMapper.selectBbsList();	//sqlSession.selectList("com.magicoh.springmaven.dao.IDao.bbsList");
		return bbsList;
	}

	@Override
	public BDto getBbs(String bId)
	{
		bbsMapper.updateHitNum(bId);	//조회수 + 1
		BDto dto = bbsMapper.selectBbs(bId);
		return dto;
	}

	@Override
	public void insertBbs(BDto dto)
	{
		bbsMapper.insertBbs(dto);
	}

	@Override
	public void updateBbs(BDto dto)
	{
		bbsMapper.updateBbs(dto);

	}

	@Override
	public void deleteBbs(String bId)
	{
		bbsMapper.deleteBbs(bId);

	}

//	@Override
//	public BDto getReplyBbs(String bId)
//	{
//		BDto dto = bbsMapper.selectBbs(bId);
//		return dto;
//	}

	@Override
	public void insertReplyBbs(BDto dto)
	{
		bbsMapper.updateReplyShape(dto.getbGroup(), dto.getbStep()); //댓글을 달기 전에 댓글이 한 개라도 있으면 STEP + 1 해서 새 댓글이 그자리에 들어갈 자리 마련한다.
		dto.setbStep(dto.getbStep() + 1);	//댓글은 원글 대비 step + 1, indent + 1(원본글 보다 한칸 뒤로 밀려서 나와야 하니까)
		dto.setbIndent(dto.getbIndent() + 1);
		bbsMapper.insertReplyBbs(dto);
	}

}
