package com.magicoh.springmaven.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;

import com.magicoh.springmaven.dbutil.Constant;
import com.magicoh.springmaven.dbutil.IDao;
import com.magicoh.springmaven.dto.BDto;

public class DaoOriginImpl implements IDao
{
	//private DataSource dataSource;
	JdbcTemplate template;
	
	
	@Autowired	//servlet-context.xml에서 컴포넌트를 스캔해서 자동으로 넣어준다. 
	public void setTemplate(JdbcTemplate template)
	{
		this.template = template;
		//Constant.template = this.template;	 
	}
	
//	@Autowired
//	public void setDao(BDao dao)
//	{
//		//this.dao = dao;
//	}
	
	public DaoOriginImpl() 
	{
		//template = Constant.template;
	}
	
	
	
	//게시물 리스트
	public ArrayList<BDto> listDao()
	{
		ArrayList<BDto> dtos = null; 
		String sql = "SELECT BID, BNAME, BTITLE, BCONTENT, BDATE, BHIT, BGROUP, BSTEP, BINDENT FROM MVC_BOARD ORDER BY BGROUP DESC, BSTEP ASC" ;
		dtos = (ArrayList<BDto>)template.query(sql, new BeanPropertyRowMapper<BDto>(BDto.class));	//sql 실행후에 그 결과를 갖고올 커멘트 객체를 BeanPropertyRowMapper에 할당
		return dtos;
	}

	//한 개의 게시물 조회
	public BDto contentViewDao(String bId)
	{
		upHitDao(bId);
		
		String sql = "SELECT * FROM MVC_BOARD WHERE BID = " + bId ;
		BDto dto = template.queryForObject(sql, new BeanPropertyRowMapper<BDto>(BDto.class));
		return dto;
	}
		
	//글쓰기
	public void writeDao(final String bName, final String bTitle, final String bContent)
	{
		template.update(new PreparedStatementCreator()
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException
			{
				// TODO Auto-generated method stub
				String sql = "INSERT INTO MVC_BOARD(BID, BNAME, BTITLE, BCONTENT, BHIT, BGROUP, BSTEP, BINDENT) VALUES(MVC_BOARD_SEQ.NEXTVAL, ?, ?, ?, 0, MVC_BOARD_SEQ.CURRVAL, 0, 0)";
				PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1, bName);
				pst.setString(2, bTitle);
				pst.setString(3, bContent);
				
				return pst;
			}
		});
	}
	
	//글수정
	public void updateDao(final String bId, final String bName, final String bTitle, final String bContent)
	{
		String sql = "UPDATE MVC_BOARD SET BNAME = ?, BTITLE = ?, BCONTENT = ? WHERE BID= ?";

		template.update(sql, new PreparedStatementSetter()
		{
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException
			{
				ps.setString(1, bName);
				ps.setString(2, bTitle);
				ps.setString(3, bContent);
				ps.setInt(4, Integer.parseInt(bId));
			}
		});
	}	
	
	//글 삭제
	public void deleteDao(final String bId)
	{
		
		String sql = "DELETE FROM MVC_BOARD WHERE BID = ?";
		
		template.update(sql, new PreparedStatementSetter()
		{
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException
			{
				ps.setInt(1, Integer.parseInt(bId));
			}
		});
	}
	
	//reply 폼 화면
	public BDto replyViewDao(String bId)
	{
		String sql = "SELECT * FROM MVC_BOARD WHERE BID = " + bId;
		BDto bdto = template.queryForObject(sql, new BeanPropertyRowMapper<BDto>(BDto.class));
		return bdto;
	}
	
	
	//답변하기
	public void replyDao(final String bId, final String bName, final String bTitle, final String bContent, final String bGroup, final int bStep, final int bIndent)
	{
		String sql = "INSERT INTO MVC_BOARD(BID, BNAME, BTITLE, BCONTENT, BGROUP, BSTEP, BINDENT) VALUES(MVC_BOARD_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?)";
		
		replyShapeDao(bGroup, bStep);
		
		template.update(sql, new PreparedStatementSetter()
		{
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException
			{
				ps.setString(1, bName);
				ps.setString(2, bTitle);
				ps.setString(3, bContent);
				ps.setInt(4, Integer.parseInt(bGroup));
				ps.setInt(5, bStep + 1);
				ps.setInt(6, bIndent + 1);
			}
		});	
	}	
	
	//들여쓰기 모양
	public void replyShapeDao(final String bGroup, final int bStep)
	{
		String sql = "UPDATE MVC_BOARD SET BSTEP = BSTEP + 1 WHERE BGROUP = ? AND BSTEP > ?";
		
		template.update(sql, new PreparedStatementSetter()
		{
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException
			{
				ps.setInt(1, Integer.parseInt(bGroup));
				ps.setInt(2, bStep);
				
			}
		});
	}
	
	//조회수 증가
		public void upHitDao(final String bId)
		{
			String sql = "UPDATE MVC_BOARD SET BHIT = BHIT + 1 WHERE BID = ?" ;
			template.update(sql, new PreparedStatementSetter()
			{
				
				@Override
				public void setValues(PreparedStatement ps) throws SQLException
				{
					ps.setInt(1, Integer.parseInt(bId));
					
				}
			});
		}

		@Override
		public void updateDao(String bId, String bTitle, String bContent)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void replyDao(String bName, String bTitle, String bContent, int bGroup, int bStep, int bIndent)
		{
			// TODO Auto-generated method stub
			
		}	
	
}
