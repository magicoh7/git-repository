package com.magicoh.springmaven.dbutil;

import java.util.ArrayList;
import com.magicoh.springmaven.dto.BDto;

public interface IDao
{
	public ArrayList<BDto> listDao();
	public BDto contentViewDao(String bId);
	public void writeDao(String bName, String bTitle, String bContent);
	public void updateDao(String bId, String bTitle, String bContent);
	public void deleteDao(String bId);
	public BDto replyViewDao(String bId);
	public void replyDao(String bName, String bTitle, String bContent, int bGroup, int bStep, int bIndent);
	public void replyShapeDao(String bGroup, int bStep);
	public void upHitDao(String bId);
	
}
