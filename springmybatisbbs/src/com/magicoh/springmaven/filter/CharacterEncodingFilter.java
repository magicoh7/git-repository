package com.magicoh.springmaven.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


//import com.magicoh.springmaven.filter.*;

//@WebFilter("/*") 
public class CharacterEncodingFilter implements Filter
{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException
	{
		request.setCharacterEncoding("UTF-8");
		
		System.out.println("Before filter");
		chain.doFilter(request, response);
		System.out.println("After filter");
	}

	@Override
	public void destroy()
	{
		// TODO Auto-generated method stub

	}

}
