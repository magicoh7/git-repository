package com.magicoh.springmaven.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.magicoh.springmaven.dto.BDto;
import com.magicoh.springmaven.service.IBbsService;

@Controller
public class BBSController
{
	@Autowired
	private IBbsService bbsService;
	
	@RequestMapping(value = {"/", "/home", "/index"}, method = RequestMethod.GET)
	public String home() 
	{
		
		return "home";
	}
	
	@RequestMapping(value = {"/sidebar"}, method = RequestMethod.GET)
	public String sidebar() 
	{
		
		return "sidebar";
	}
	
	@RequestMapping(value = "/bbsList", method = RequestMethod.GET)
	public String getBbsList(Model model) throws Exception
	{
		List<BDto> bbsList = bbsService.getBbsList();
		
		model.addAttribute("bbsList", bbsList);
		
		return "bbsList";
	}
	
	//글 내용  보기 화면
	@RequestMapping(value = "/bbs_view", method = RequestMethod.GET)
	public ModelAndView getBbs(@RequestParam String bId) 
	{
		BDto dto = bbsService.getBbs(bId);
		return new ModelAndView("bbs_view", "bbs", dto);
	}
	
	//글 작성 폼 화면
	@RequestMapping(value = "/insertBbs_form", method = RequestMethod.GET)
	public ModelAndView insertBbsForm(Model model)
	{
		List<String> genderList = new ArrayList<>();
		genderList.add("남자");
		genderList.add("여자");
		List<String> cityList = new ArrayList<>();
		cityList.add("서울");
		cityList.add("부산");
		cityList.add("대구");
		cityList.add("대전");
		cityList.add("광주");
		
		Map<String, List<String>> map = new HashMap<>();
		map.put("genderList", genderList);
		map.put("cityList", cityList);
		
		return new ModelAndView("insertBbs_form", "map", map);
	}

	@RequestMapping(value = "/insertBbs", method = RequestMethod.POST)
	public String insertBbs(@ModelAttribute BDto dto)
	{
		if(dto != null)
		{
			bbsService.insertBbs(dto);
		}
		
		return "redirect:bbsList";
	}
	
	@RequestMapping(value = "/updateBbs_form", method = RequestMethod.GET)
	public ModelAndView updateBbsForm(@RequestParam String bId)
	{
		BDto dto = bbsService.getBbs(bId);
		
		List<String> genderList = new ArrayList<>();
		genderList.add("남자");
		genderList.add("여자");
		List<String> cityList = new ArrayList<>();
		cityList.add("서울");
		cityList.add("부산");
		cityList.add("대구");
		cityList.add("대전");
		cityList.add("광주");
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bbs", dto);
		map.put("genderList", genderList);
		map.put("cityList", cityList);
		
		return new ModelAndView("updateBbs_form", "map", map);
	}
	
	@RequestMapping(value = "/updateBbs", method = RequestMethod.POST)
	public String updateBbs(@ModelAttribute BDto dto)
	{
		bbsService.updateBbs(dto);
		
		return "redirect:bbsList";
	}
	
	@RequestMapping(value = "/deleteBbs", method = RequestMethod.GET)
	public String deleteBbs(@RequestParam String bId)
	{
		bbsService.deleteBbs(bId);
		
		return "redirect:bbsList";
	}
	
	//댓글 작성 폼 화면
	@RequestMapping(value = "/replyBbs_form", method = RequestMethod.GET)
	public ModelAndView replyBbsForm(@RequestParam String bId)
	{
		BDto dto = bbsService.getBbs(bId);
		
		List<String> genderList = new ArrayList<>();
		genderList.add("남자");
		genderList.add("여자");
		List<String> cityList = new ArrayList<>();
		cityList.add("서울");
		cityList.add("부산");
		cityList.add("대구");
		cityList.add("대전");
		cityList.add("광주");
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bbs", dto);
		map.put("genderList", genderList);
		map.put("cityList", cityList);
		
		return new ModelAndView("replyBbs_form", "map", map);
	}
	
	@RequestMapping(value = "/insertReply", method = RequestMethod.POST)
	public String insertReply(@ModelAttribute BDto dto)
	{
		if(dto != null)
		{
			bbsService.insertReplyBbs(dto);
		}
		
		return "redirect:bbsList";
	}	
	
	public String handleExString(Exception e)
	{
		return "viewError";
	}
}
