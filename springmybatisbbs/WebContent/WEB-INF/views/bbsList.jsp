<%@page import="java.io.FileInputStream"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>

<%@ page import="java.util.ArrayList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/custom.css">
<title>Spring MyBatis Board</title>
<style type="text/css">
	a, a:hovor {
		color: #000000;
		text-decoration: none;		
	}
</style>
</head>
<body>
	<%
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
		}
		int pageNumber = 1;	//기본 페이지
		if(request.getParameter("pageNumber") != null){
			pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
		}
	%>
	<nav class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Spring MyBatis Board</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="home">메인</a></li>
				<li class="active"><a href="bbsList">게시판</a></li>
			</ul>
			<%
				if(userID == null) {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">접속하기<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="login.jsp">로그인</a></li>
						<li><a href="join.jsp">회원가입</a></li>
					</ul>
				</li>
			</ul>
			
			<%
				} else {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">회원관리<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="logout.jsp">로그아웃</a></li>
					</ul>
				</li>
			</ul>
			
			<%
				}
			%>
		</div>
	</nav>
	
	<div class="container">
		<div class="content-container">
            <section>
            </section>
        </div>
		<div class="row">
			<div>
			    <nav class="navbar navbar-default" >
			        <div class="container-fluid" >
			            <form action="list.do" method="Get">
				            <div class="collapse navbar-collapse" style="text-align:right">
				                <div class="form-group">
				                    <input type="text" class="form-group" name="search_text" id="search_text"  />
				                    <button type="submit" class="btn btn-success" name="btnSearch">search</button>
				                </div>
				            </div>
			            </form>
			        </div>
			    </nav>	
			</div>
		
			<div>
				<table class="table table-striped" style="text-align: center; border: 1px solid #dddddd">
					<thead>
						<tr>
							<th style="background-color: #eeeeee; text-align: left;">번  호</th>
							<th style="background-color: #eeeeee; text-align: left;">작성자</th>
							<th style="background-color: #eeeeee; text-align: left;">제  목</th>
							<th style="background-color: #eeeeee; text-align: center;">작성일</th>
							<th style="background-color: #eeeeee; text-align: center;">조회수</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="dto" items="${bbsList}">
						<tr>
							<td align=left>${dto.bId }</td>
							<td align=left>${dto.bName }</td>
							<td align=left>
								<c:forEach begin="1" end="${dto.bIndent}">&nbsp;&nbsp;</c:forEach>
								<a href="bbs_view?bId=${dto.bId}">${dto.bTitle}</a>
							</td>
							<td>${dto.bDate}</td>
							<td>${dto.bHit}</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				<a href="insertBbs_form" class="btn btn-primary pull-right">글쓰기</a>
			</div>

		</div>
	</div>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	
</body>
</html>