<%@ page language="java" contentType="text/html; charset=UTF-8"     pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<!-- Custom styles for this template -->
	<link href="css/toggle-sidebar.css" rel="stylesheet">
	
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script>
		$(document).ready(function () {
		  var trigger = $('.link2me'),
		      overlay = $('.overlay'),
		     isClosed = false;
		
		    trigger.click(function () {
		      link2me_cross();      
		    });
		
		    function link2me_cross() {
		
		      if (isClosed == true) {          
		        overlay.hide();
		        trigger.removeClass('is-open');
		        trigger.addClass('is-closed');
		        isClosed = false;
		      } else {   
		        overlay.show();
		        trigger.removeClass('is-closed');
		        trigger.addClass('is-open');
		        isClosed = true;
		      }
		  }
		 
		  $('[data-toggle="offcanvas"]').click(function () {
		        $('#wrapper').toggleClass('toggled');
		  });  
		});
	</script>
</body>
<div id="wrapper">
	<div class="overlay"></div>

	<!-- Sidebar 숨김으로 보였다 보이지 않았다 하는 부분 -->
	<nav class="navbar navbar-inverse navbar-fixed-top"
		id="sidebar-wrapper" role="navigation">
		<ul class="nav sidebar-nav">
			<li class="sidebar-brand"><a href="#"> Brand </a></li>
			<li><a href="#">Home</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">메뉴1</a></li>
			<li><a href="#">메뉴2</a></li>
			<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown">메뉴3<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li class="dropdown-header">Dropdown heading</li>
					<li><a href="#">서브메뉴3.1</a></li>
					<li><a href="#">서브메뉴3.2</a></li>
					<li><a href="#">서브메뉴3.3</a></li>
					<li><a href="#">서브메뉴3.4</a></li>
					<li><a href="#">서브메뉴3.5</a></li>
				</ul></li>
			<li><a href="#">메뉴4</a></li>
			<li><a href="#">메뉴5</a></li>
			<li><a href="https://twitter.com/maridlcrmn">메뉴6</a></li>
		</ul>
	</nav>
	<!-- /#sidebar-wrapper -->

	<!-- Page Content -->
	<div id="page-content-wrapper">
		<button type="button" class="link2me is-closed"
			data-toggle="offcanvas">
			<span class="hamb-top"></span> <span class="hamb-middle"></span> <span
				class="hamb-bottom"></span>
		</button>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<h1>Toggle Sidebar Navigation</h1>
					<p>토글 사이드바 네비게이션에 대한 본문 내용입니다.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
</body>
</html>