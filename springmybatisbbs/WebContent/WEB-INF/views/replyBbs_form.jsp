<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width", initial-scale="1">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/custom.css">
<title>Spring MyBatis Board</title>
</head>
<body>
	<%
		//Get UserID
		String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String) session.getAttribute("userID");
		}
		
		
// 		int bbsID = 0;
// 		if(request.getParameter("bbsID") != null){
// 			bbsID = Integer.parseInt(request.getParameter("bbsID"));
// 		}
// 		if(bbsID == 0){
// 			PrintWriter script = response.getWriter();
// 			script.println("<script>");
// 			script.println("alert('유효하지 않은 글입니다.')");
// 			script.println("location.href = 'Bbs.jsp'");
// 			script.println("</script>");	
// 		}
		
		//Bbs bbs = new BbsDAO().getBbs(bbsID);
	%>
	<nav class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Spring MyBatis Board</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="home.jsp">메인</a></li>
				<li class="active"><a href="bbsList">게시판</a></li>
			</ul>
			<%
				if(userID == null) {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">접속하기<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="login.jsp">로그인</a></li>
						<li><a href="join.jsp">회원가입</a></li>
					</ul>
				</li>
			</ul>
			
			<%
				} else {
			%>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">회원관리<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="logout.jsp">로그아웃</a></li>
					</ul>
				</li>
			</ul>
			
			<%
				}
			%>
		</div>
	</nav>
	
	<div class="container">
		<div class="row">
			<form action="insertReply" method="post">
				<!-- 원글에 대한 데이터(원글의 그룹 -> 답변 글의 bGroup 값으로 저장)-->
				<input type="hidden" name="bId" value="${map.bbs.bId }">
				<input type="hidden" name="bGroup" value="${map.bbs.bGroup }">
				<input type="hidden" name="bStep" value="${map.bbs.bStep }">
				<input type="hidden" name="bIndent" value="${map.bbs.bIndent }">
				<table class="table table-striped" style="text-align: center; border: 1px solid #dddddd">
					<thead>
						<tr>
							<th colspan="3" style="background-color: #eeeeee; text-align: center;">답변하기 화면</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width: 20%">원글 제목</td>
							<td colspan="2" style="text-align: left;">${map.bbs.bTitle} </td>
						</tr>
						<tr>
							<td>작 성 자</td>
							<td colspan="2" style="text-align: left;">${map.bbs.bName}</td>
						</tr>
						<tr>
							<td>작성일자</td>
							<td colspan="2" style="text-align: left;"><fmt:formatDate  pattern="yyyy-MM-dd hh:mm:dd" value="${map.bbs.bDate }"/></td>
						</tr>
						<tr>
							<td>조 회 수</td>
							<td colspan="2" style="text-align: left;"><fmt:formatNumber type="number" pattern="#,###" value="${map.bbs.bHit}"></fmt:formatNumber></td>
						</tr>
						<tr>
							<td style="width: 20%">답글 작성자</td>
							<td colspan="2" style="text-align: left;"><input type="text" class="form-control" placeholder="답글 작성자" name="bName" maxlength="50"></td>
						</tr>

						<tr>
							<td style="width: 20%">답글 제목</td>
							<td colspan="2" style="text-align: left;"><input type="text" class="form-control" placeholder="글 제목" name="bTitle" maxlength="50" value="(re)${map.bTitle}"></td>
						</tr>
						<tr>
							<td>답글 내용</td>
							<td colspan="2" style="min-height: 200px; text-align: left"><textarea class="form-control" placeholder="글 내용" name="bContent" style="height:350px" value="${map.bContent}"></textarea></td>
						</tr>
					</tbody>
				</table>
				<input type="submit" class="btn btn-primary" value="저장">
				<a href="bbsList" class="btn btn-info">목록</a>
				
			</form>				
		</div>
	</div>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	
</body>
</html>