package com.magicoh.springmvc.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Product {

	private int product_id = 0;			
	private String product_name = ""; 	
	private String brand = "";        	
	private String description = "";  	
	private double unit_price = 0.0;   	
	private int category_id = 0; 		
	private int is_active = 1;	   		
	
	public Product() {
	}

	public Product(int product_id, String product_name, String brand, String description, double unit_price,
			int category_id, int is_active) {
		this.product_id = product_id;
		this.product_name = product_name;
		this.brand = brand;
		this.description = description;
		this.unit_price = unit_price;
		this.category_id = category_id;
		this.is_active = is_active;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@JsonIgnore
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(double unit_price) {
		this.unit_price = unit_price;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	
	@JsonIgnore
	public int getIs_active() {
		return is_active;
	}

	public void setIs_active(int is_active) {
		this.is_active = is_active;
	}

	@Override
	public String toString() {
		return "Product [product_id=" + product_id + ", product_name=" + product_name + ", brand=" + brand
				+ ", description=" + description + ", unit_price=" + unit_price + ", category_id=" + category_id
				+ ", is_active=" + is_active + "]";
	}
	
	
	
}
