package com.magicoh.springmvc.dao;

import java.util.List;

import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.Client;
import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.InvoiceCommonResult;
import com.magicoh.springmvc.dto.Product;

/**
 * Use mappter interFace setting in application-contex.xml
 * **/
@MyMapper 
public interface IBbsMapper
{
	//Category
	public List<Category> selectCategoryList();
	public Category selectCategory(int category_id);
	public void insertCategory(Category category);
	public void updateCategory(Category category);
	public void deleteCategory(int category_id);

	//Product
	public List<Product> selectProductList();
	public List<Product> selectProductsByName(String product_name);
	public Product selectProduct(int product_id);
	public void insertProduct(Product product);
	public void updateProduct(Product product);
	public void deleteProduct(int product_id);
	
	//Client
	public List<Client> selectClientList();
	public List<Client> selectClientsByName(String client_name);
	public Client selectClient(int client_id);	
	
	//Invoice
	public List<InvoiceCommonResult> getInvoiceList();
	public List<InvoiceCommonResult> getInvoiceDetail(int invoice_id);
	public InvoiceCommonResult selectInvoiceHeader(int invoice_no);
	public int getMaxInvoiceId();
	public void insertInvoiceDetail(InvoiceDetail invoiceDetail);
	public void insertInvoiceHeader(InvoiceHeader invoiceHeader);
//	public void updateProduct(Product product);
//	public void deleteProduct(int product_id);
	
}
