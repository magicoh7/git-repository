package com.magicoh.springmvc.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.magicoh.springmvc.dto.Category;
import com.magicoh.springmvc.dto.Client;
import com.magicoh.springmvc.dto.InvoiceDetail;
import com.magicoh.springmvc.dto.InvoiceHeader;
import com.magicoh.springmvc.dto.InvoiceCommonResult;
import com.magicoh.springmvc.dto.Product;
import com.magicoh.springmvc.dto.User;
import com.magicoh.springmvc.service.product.ICategoryService;
import com.magicoh.springmvc.service.product.IClientService;
import com.magicoh.springmvc.service.product.IProductService;
import com.magicoh.springmvc.service.sales.IInvoiceService;

@Controller//("/sales")
public class SalesController {
	@Autowired
	private IInvoiceService invoiceService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private IProductService productService;

	
	/***************************************
	 *카테고리 등록/조회/수정/삭제
	 **************************************/
	
	//전체 인보이스 목록 조회
	@RequestMapping(value = "/invoice_list", method = RequestMethod.GET)
	public String getInvoiceList(Model model) throws Exception
	{
		List<InvoiceCommonResult> invoiceListView = invoiceService.getInvoiceList();
		
		model.addAttribute("invoiceListView", invoiceListView);
		
		//System.out.println("SalesController invoiceListView.size() getClient_name ====> " + invoiceListView.get(0).getClient_name());
		
		return "/sales/invoice_list";
	}
	
	//Invoice insert form
	@RequestMapping(value = "/invoice_insertform", method = RequestMethod.GET)
	public ModelAndView invoiceInsertForm(Model model)
	{
		//initialize insert from
		List<Category> categoryList = categoryService.getCategoryList(); //Category
		List<Client> clientList = clientService.getClientList(); //Client
		List<Product> productList = productService.getProductList(); //product

		//InvoiceHeader invoiceHeader = invoiceService.getInvoiceHeader(invoice_id);
		
		//setting invoice date today 
		SimpleDateFormat format1 = new SimpleDateFormat ("yyyy-MM-dd");
		Date today = new Date();
		String strToday = format1.format(today);
		
		int invoice_id = 0; //dummy invoice id 
		List<InvoiceCommonResult> invoiceDetailList = invoiceService.getInvoiceDetail(invoice_id);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("categoryList", categoryList);
		map.put("clientList", clientList);
		map.put("productList", productList);
		map.put("invoice_date", strToday);
		//map.put("invoiceHeader", new InvoiceHeader());
		map.put("invoiceDetailList", invoiceDetailList);

		return new ModelAndView("/sales/invoice_insertform", "map", map);
	}
	
	//Save Invoice
	//@Transactional
	@RequestMapping(value = "/invoice_insert", method = RequestMethod.POST)
	public String insertInvoice(@RequestBody InvoiceHeader invoiceHeader, HttpSession session)
	{
		User loginUser = (User) session.getAttribute("loginUser");

		if(invoiceHeader != null)
		{
			//1. get max invoice_id(next invoice_id) 
			int maxInvoiceId = invoiceService.getMaxInvoiceId();
			int invoice_id = maxInvoiceId + 1;	//seq.nextval
			
			//2. save invoice header
			invoiceHeader.setInvoice_id(invoice_id);
			System.out.println(invoiceHeader.toString());
			invoiceService.insertInvoiceHeader(invoiceHeader);
			
			//3. get invoice detail from param 
			List<InvoiceDetail> invoiceDetails = invoiceHeader.getInvoiceDetails();
			
			//4. save invoice detail with looping 
			for(InvoiceDetail detail : invoiceDetails){
				detail.setInvoice_id(invoice_id); 			//invoice_id
				System.out.println("invoiceDetail : " + detail.toString());
				invoiceService.insertInvoiceDetail(detail);	//save invoice
			}
		}
		return "redirect:invoice_list";
	}

	//Invoice update form
	@RequestMapping(value = "/invoice_updateform", method = RequestMethod.GET)
	public ModelAndView invoiceUpdateForm(@RequestParam(value="invoice_id", defaultValue="0") int invoice_id, Model model)
	{
		List<Client> clientList = clientService.getClientList(); //Client

		//setting invoice date today 
		SimpleDateFormat format1 = new SimpleDateFormat ("yyyy-MM-dd");
		Date today = new Date();
		String strToday = format1.format(today);

		//get invoice header
		InvoiceCommonResult invoiceHeader = invoiceService.getInvoiceHeader(invoice_id);
		
		//get invoice detail list
		List<InvoiceCommonResult> invoiceDetailList = invoiceService.getInvoiceDetail(invoice_id);

		Map<String, Object> map = new HashMap<String, Object>();
		//map.put("categoryList", categoryList);
		//map.put("productList", productList);
		map.put("clientList", clientList);
		map.put("invoice_date", strToday);
		map.put("invoiceHeader", invoiceHeader);
		map.put("invoiceDetailList", invoiceDetailList);

		return new ModelAndView("/sales/invoice_updateform", "map", map);
	}	
	
	
	
	//@ResponseBody
	@RequestMapping(value = "/product_modal", method = RequestMethod.GET)
	//public List<Product> getProductList(ModelMap model, @RequestParam("product_name") String product_name, @RequestParam("parentsLowNo") String parentsLowNo) throws Exception
	public String getProductList(ModelMap model, @RequestParam("product_name") String product_name, @RequestParam("parentsLowNo") String parentsLowNo) throws Exception
	{
		//System.out.println("product_name : [" + product_name + "] , parentLowNo :[" + parentsLowNo +"]");
		
		List<Product> productList = productService.getProductsByName(product_name);
		System.out.println("게시물  건수 : " + productList.size());
		model.addAttribute("productList", productList);
		model.addAttribute("parentsLowNo", parentsLowNo);
		
		//next sentence works fine(1. no @ResponseBody / 2.return type is public String getProductList / 3. modal page return type"/sales/product_modal" ) 
		return "/shared/product_modal";
		//next sentence works fine
		//return productList;
	}
	
	//modal called from parent screen(invoiceInsert) 
	//@ResponseBody
	@RequestMapping(value = "/client_modal", method = RequestMethod.GET)
	public String getClientList(ModelMap model, @RequestParam("client_name") String client_name) throws Exception
	{
		List<Client> clientList = clientService.getClientsByName(client_name);
		System.out.println("모달 띄우면서 검색된  건수(GET) : " + clientList.size());
		model.addAttribute("clientList", clientList);
		
		return "/shared/client_modal";
	}
	
	//search in a client model
	@ResponseBody
	@RequestMapping(value = "/client_modal_search", method = RequestMethod.GET)
	public List<Client> getClientListInModal(ModelMap model, @RequestParam("client_name") String client_name) throws Exception
	{
		List<Client> clientList = clientService.getClientsByName(client_name);
		System.out.println("모달 내 검색된 결과수(POST) : " + clientList.size());
		model.addAttribute("clientList", clientList);
	
		return clientList;
		//return "/shared/client_modal";
	}
}
