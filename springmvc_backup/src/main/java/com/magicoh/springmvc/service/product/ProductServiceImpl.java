package com.magicoh.springmvc.service.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magicoh.springmvc.dao.IBbsMapper;
import com.magicoh.springmvc.dto.Product;

@Service
//@Service("IProductService")
public class ProductServiceImpl implements IProductService
{
	@Autowired
	private IBbsMapper bbsMapper;

	@Override
	public List<Product> getProductList()
	{
		List<Product> productList = bbsMapper.selectProductList();
		return productList;
	}

	@Override
	public List<Product> getProductsByName(String product_name)
	{
		List<Product> productList = bbsMapper.selectProductsByName(product_name);
		return productList;
	}
	
	
	@Override
	public Product getProduct(int product_id)
	{
		Product product = bbsMapper.selectProduct(product_id);
		return product;
	}

	@Override
	public void insertProduct(Product product)
	{
		bbsMapper.insertProduct(product);
	}

	@Override
	public void updateProduct(Product product)
	{
		bbsMapper.updateProduct(product);
	}

	@Override
	public void deleteProduct(int product_id)
	{
		bbsMapper.deleteProduct(product_id);
	}

}
