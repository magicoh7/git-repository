<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<div class="container" style="width:400px;">
	    <nav class="navbar navbar-default">
			<form action="client_modal" name="clientSearchForm" id="clientSearchForm" class="navbar-form pull-right" method="POST">
				<input type="text" id="client_name" name="client_name" class="form-group search-query" placeholder="검색" value="${client_name}" />
				<button type="submit" id="btnSearch" class="btn btn-success">조회</button>
			</form>
	    </nav>
		<div class="row">
			<div>
				<table id="datatable" class="table table-bordered table-hover table-striped"  style="width:100%">
					<thead>
						<tr>
							<th style="background-color: #eeeeee">client_id</th>
							<th style="background-color: #eeeeee">client_name</th>
							<th style="background-color: #eeeeee">address</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="client" items="${clientList}">
						<tr>
							<td align=left>${client.client_id }</td>
							<td align=left><a href="#">${client.client_name}</a></td>
							<td align=left>${client.address }</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<script>
		$(document).ready(function(){
			$("#datatable").DataTable({
				"paging": true,
				"bFilter":false,
				columnDefs: [
                    { "targets": 0, 'width': '50px'},
                    { "targets": 1, 'width': '100px'},
                    { "targets": 2, 'width': '150px'},
               ],
				"columns": [
                    { "data": "client_id"},
                    { "data": "client_name"},
                    { "data": "address"},
                ]
            });
			
            //clicked search button
            $('#btnSearch').on('click', function () {
            	event.preventDefault(); //모달 창에서 조회했을 때 팝업이 닫히는 문제를 막아줌.	
            	var client_name = $('#client_name').val();
                Fn_Search_Client(client_name);
            });

			
		});	//end of ready()
		
		//Check duplicate productId from parents table
		function Fn_IsDuplicate_Items(productId) {
	        var dup = false;
	        $('#invoiceDetails tbody tr').each(function (index, ele) {
	            var pid = table.cell(this, 2).nodes().to$().find('input').val();       //productId
	            if (productId.trim() == pid.trim()) dup = true;
	        });
	        return dup;
	    }
		
		//search client
		function Fn_Search_Client(client_name) {
		
			//var client_name = $('#client_name').val();
			//alert('Fn_Search_Client client_name : ' + client_name);
			
			var url = "client_modal_search";
			var data = {client_name: client_name};
			//alert($("form[name=clientSearchForm]").serialize());
		
			
			
		      $.ajax({
		          type: "GET",
		          url: url,
		          data: data,
		          contentType:"application/json; charset=utf-8",
		          dataType: "json",
		          success: function (response) {
		          	alert('55555');
					//$('#datatable').html(response);

		          	//$('#datatable tr:not()').remove();
		          	
		          	var text = "";
		          	for(var i=0; i<response.length; i++){
		          		text += "<tr>\n";
		          		text += "<td>" + response[i].client_id + "</td>\n";
		          		text += "<td>" + response[i].client_name + "</td>\n";
		          		text += "<td>" + response[i].address + "</td>\n";
		          		text += "</tr>\n";
		          	}
		          	
 		          	alert(text);
		          	
// 		          	$('#datatable').append(text);
		          	
		          }, failure: function (data) {
		        	  alert('11');
	              	//alert(response.responseText);
	              }, error: function (data) {
	            	  alert('22');
	                //alert(response.responseText);
	              }
		      });
		}
	</script>	
</body>
</html>